<?php // $Id: xslfilter.php 3150 2012-10-12 14:27:58Z sdalu $

/* Stephane D'Alu (c) 2007-2012
 * http://www.sdalu.com/
 */

/* 
 * Perform XSL transformation
 *
 *  __xslfilter_disabled
 *  __xslfilter_debug
 *  __xslfilter_phpfunc
 *  __xslfilter_pi
 *  __xslfilter_pi_disabled
 *  __xslfilter_prehook
 *  __xslfilter_posthook (not implemented)
 *  __xslfilter_start_utime
 *  __xslfilter_profiling
 *  __xslfilter_i18n
 * 
 * ENV:
 *  expect-lang
 *  handheld-client
 *  WEB_LAYOUT
 *  XSL_DIR
 *  XSL_STYLESHEET
 *
 * Param
 *   lastmod
 *   now %Y%m%d%H%M%SZ
 *   http-referer
 *   request-uri
 *   server-name
 *   secure
 *   protocol
 *
 * TODO:
 *  - need to handle 'media' and 'alternate' attribute in
 *     xml-stylesheet processing instruction
 */


global $__xslfilter_start_utime;
if (!$__xslfilter_start_utime)
    $__xslfilter_start_utime = microtime();


function __xslfilter_build_xmlerrors($errors) {
    if (empty($errors))
	return "No errors!";

    $r = array();

    foreach ($errors as $error) {
	switch ($error->level) {
	case LIBXML_ERR_WARNING: $lvl = "Warning"; break;
	case LIBXML_ERR_ERROR:   $lvl = "Error";   break;
	case LIBXML_ERR_FATAL:   $lvl = "Fatal";   break;
	default:                 $lvl = "????";    break;
	}
    
	$err = $lvl . ": " . trim($error->message) . "\n";
	if ($error->file) 
	    $err .= "<br/> (File: $error->file)\n";
	
	$r[] = array('msg' => $err, 'line' => $error->line);
    }
    
    return $r;
}

function __xslfilter_get_stylesheet($doc, $type=null) {
    $pi = array();
    foreach ($doc->childNodes as $n) 
	if (($n->nodeType == XML_PI_NODE) && 
	    ($n->nodeName == 'xml-stylesheet')) {
	    for ($i = preg_match_all('/(\w+)="([^"]*)"/', $n->data, $m) ;
		 $i ; ) {
		$i--;
		$kv[trim($m[1][$i])]= trim($m[2][$i]);
            }
	    if (is_null($type) || $kv['type'] == $type)
		$pi[] = $kv;
	}
    return empty($pi) ? null : $pi;
}

function __xslfilter_process_pi($doc) {
    global $__xslfilter_pi;
    global $__xslfilter_pi_disabled;

    if (is_null($__xslfilter_pi) || $__xslfilter_pi_disabled)
        return null;

    if (! $doc->hasChildNodes())
        return null;

    foreach ($doc->childNodes as $n)
        if ($n->nodeType == XML_PI_NODE) {
	    $inst = $n->nodeName;
	    $func = $__xslfilter_pi[$inst];
	    if ($inst != 'xml-stylesheet' && !is_null($func)) {
	        $res = $func($n->data);
		$n->parentNode->replaceChild(new DOMText($res), $n);
	    }
        } else {
	    if ($r = __xslfilter_process_pi($n))
	        return $r;
	}
    return null;
}


function __xslfilter_do_error($headline, $messages=null) {
    header('HTTP/1.0 500 Internal error');
  $r = <<<EOT
<html>
  <head>
    <title>Internal Error</title>
    <style>
      table     { border: 		5px double red;
		  border-collapse:	collapse;		      }
      th        { text-align: 		left;
	          font-weight: 		bold;
	          border-bottom: 	2px solid red;		      }
      td        { vertical-align: 	top;			      }
      tr.msg td { border-top: 		1px solid red;		      }
    </style>
  </head>
  <body>
    <table>
      <tr><th colspan="2">$headline</th></tr>
EOT;
      if (is_string($messages))
	$messages = array(array('msg' => $messages));
      if (!empty($messages)) {
	foreach ($messages as $m) {
	  $r .= "      <tr class=\"msg\"><td>Message:</td><td>" .
	    $m['msg'] . "</td></tr>\n";
	  if ($m['line'])
	    $r .= "      <tr><td>At line:</td><td>" . 
	      htmlspecialchars($m['line']) . "</td></tr>\n";
	}
      }      
$r .= <<<EOT
    </table>
  </body>
</html>
EOT;
    return $r;
}


function xslfilter($buffer) {
    global $__xslfilter_debug;
    global $__xslfilter_phpfunc;
    global $__xslfilter_prehook;
    global $__xslfilter_posthook;
    global $__xslfilter_start_utime;
    global $__xslfilter_i18n;

    if (empty($buffer))
	return false;

    if ($__xslfilter_debug)
	return false;
    

    libxml_use_internal_errors(true);
  
    if (!array_key_exists('WEB_LAYOUT',     $_SERVER) &&
        !array_key_exists('XSL_DIR',        $_SERVER) &&
	!array_key_exists('XSL_STYLESHEET', $_SERVER))
	return __xslfilter_do_error("Missing stylesheet information",
		"None of the environment variables <code>WEB_LAYOUT</code>, "
	      . "<code>XSL_DIR</code> or <code>XSL_STYLESHEET</code> "
	      . "have been defined");
  
    libxml_clear_errors();

    // Load document
    $doc = new DOMDocument;
    if (! $doc->loadXML($buffer, LIBXML_NONET))
	return __xslfilter_do_error("Error in current document",
			    __xslfilter_build_xmlerrors(libxml_get_errors()));

    // Processing instruction
    if ($r = __xslfilter_process_pi($doc))
        return $r;

    // XSLT Processor
    $xslt = new xsltProcessor;  

    // Basic information 
    $xslt->setParameter('', 'lastmod', 
    	gmstrftime("%Y%m%d%H%M%SZ", getlastmod()));
    $xslt->setParameter('', 'now', 
    	gmstrftime("%Y%m%d%H%M%SZ", time()));

    // Information about http request
    $xslt->setParameter('', 'http-referer', 
        array_key_exists('HTTP_REFERER', $_SERVER) ? $_SERVER['HTTP_REFERER']
					 	   : null);
    $xslt->setParameter('', 'request-uri', 
        array_key_exists('REQUEST_URI', $_SERVER)  ? $_SERVER['REQUEST_URI']
					 	   : null);
    $xslt->setParameter('', 'base-uri', 
        array_key_exists('SCRIPT_FILENAME',$_SERVER)
            ? dirname($_SERVER['SCRIPT_FILENAME'])
 	    : null);

    $xslt->setParameter('', 'server-name', 
        array_key_exists('SERVER_NAME', $_SERVER)  ? $_SERVER['SERVER_NAME']
					 	   : null);
    $xslt->setParameter('', 'secure', 
        array_key_exists('HTTPS', $_SERVER)        ? 'true' : null);
    $xslt->setParameter('', 'protocol', 
        array_key_exists('HTTPS', $_SERVER)        ? 'https' : 'http');

    if (($script_uri = parse_url($_SERVER['SCRIPT_URI'])) !== false) {
        $xslt->setParameter('', 'uri-scheme',   @$script_uri['scheme'  ]);
        $xslt->setParameter('', 'uri-host',     @$script_uri['host'    ]);
        $xslt->setParameter('', 'uri-port',     @$script_uri['port'    ]);
        $xslt->setParameter('', 'uri-path',     @$script_uri['path'    ]);
        $xslt->setParameter('', 'uri-query',    @$script_uri['query'   ]);
        $xslt->setParameter('', 'uri-fragment', @$script_uri['fragment']);
    } else {
        $xslt->setParameter('', 'uri-scheme',   null);
        $xslt->setParameter('', 'uri-host',     null);
        $xslt->setParameter('', 'uri-port',     null);
        $xslt->setParameter('', 'uri-path',     null);
        $xslt->setParameter('', 'uri-query',    null);
        $xslt->setParameter('', 'uri-fragment', null);
    }

    
    // Parameters set from apache server
    $xslt->setParameter('', 'pagedesc-default-xml',
        array_key_exists('WEB_LAYOUT', $_SERVER) 
	    ? $_SERVER['WEB_LAYOUT'] . '/defaults.xml' : null);
    $xslt->setParameter('', 'expect-lang', 
        array_key_exists('expect-lang', $_SERVER)  ? $_SERVER['expect-lang']
					 	   : null);
    $xslt->setParameter('', 'handheld-client', 
        array_key_exists('handheld-client', $_SERVER) ?
	                                $_SERVER['handheld-client'] : null);
    
    // Pre hook
    if ((! is_null($__xslfilter_prehook)) &&
	($r = $__xslfilter_prehook($doc, $xslt)) !== true)
	return $r;

    // PHP function enabling
    if ($__xslfilter_phpfunc)
        $xslt->registerPHPFunctions(is_array($__xslfilter_phpfunc) 
	                            ? $__xslfilter_phpfunc : null);

    // Lang to import localization templates
    $lang = '';
    if ($__xslfilter_i18n === null || $__xslfilter_i18n)
	$lang = $doc->documentElement->getAttribute('lang');
    if (empty($lang) && // Fallback to default if provided
	!is_null($__xslfilter_i18n) && is_string($__xslfilter_i18n))
	$lang = $__xslfilter_i18n;

    // Listing stylesheet
    $stylesheet = array();
    if (array_key_exists('WEB_LAYOUT',     $_SERVER) ||
        array_key_exists('XSL_DIR',        $_SERVER)) {
	$xsl_dir = array_key_exists('XSL_DIR', $_SERVER) 
	         ? $_SERVER['XSL_DIR']
                 : $_SERVER['WEB_LAYOUT'];

	$style = __xslfilter_get_stylesheet($doc, 'text/xsl');
	foreach($style as $s) {
	    if (! ($href = $s['href']))
		continue;
	    
	    if (($href[0] == '/') ||
		(preg_match('|^\w+://|', $href)))
		return __xslfilter_do_error("Error in current document",
			    "Only local and relative stylesheet are allowed");
	
            $stylesheet[] =  $xsl_dir . '/' . $href;
        }
        if (empty($stylesheet) &&
            array_key_exists('XSL_STYLESHEET', $_SERVER)) {
	    $href = $_SERVER['XSL_STYLESHEET'];
	    if ($href[0] != '/')
 	        array_push($stylesheet, $xsl_dir . '/' . $href);
        }

	if (!empty($lang)) {
	    $href = 'l10n/' . $lang . '.xsl';
	    if (file_exists($file = $xsl_dir . '/' . $href))
		$stylesheet[] = $file;
	}
    } else if (array_key_exists('XSL_STYLESHEET', $_SERVER)) {
        array_push($stylesheet, $_SERVER['XSL_STYLESHEET']);
	if (!empty($lang)) {
	    $file = dirname($_SERVER['XSL_STYLESHEET']) . '/' 
		  . 'l10n/' . $lang . '.xsl';
	    if (file_exists($file))
		$stylesheet[] = $file;
	}
    } else {
	return __xslfilter_do_error("No usable stylesheet",
	    "Missing stylesheet at server and document level");
    }

    // Importing stylesheet
    if (count($stylesheet) <= 1) {
	// Correct code for importing stylesheet
	// Unfortunately PHP only take into account the last import
	foreach($stylesheet as $file) {
	    libxml_clear_errors();
	    $xsl  = new DomDocument();
	    $xsl->substituteEntities = true; // LIBXML_NOENT
	    if (! $xsl->load($file, LIBXML_NONET))
		return __xslfilter_do_error("Error in stylesheet: $href",
		     __xslfilter_build_xmlerrors(libxml_get_errors()));

	    $xslt->importStyleSheet($xsl);
	}
    } else {
	// Fix code to import several stysheets
	// By faking a top level stylesheet with import statements
	libxml_clear_errors();
	$xsl  = new DomDocument('1.0');
	$xsl->substituteEntities = true; // LIBXML_NOENT
	$root = $xsl->createElementNS('http://www.w3.org/1999/XSL/Transform',
				      'xsl:stylesheet');
	$v = $xsl->createAttribute('version');
	$v->value="1.0";
	$root->appendChild($v);
	
	$xsl->appendChild($root);
	foreach($stylesheet as $file) {
	    $s = $xsl->createElementNS('http://www.w3.org/1999/XSL/Transform',
				       'xsl:import');
	    $h = $xsl->createAttribute('href');
	    $h->value = $file;
	    $s->appendChild($h);
	    $root->appendChild($s);
	}
	$xslt->importStyleSheet($xsl);
    }


    // Applying transformation
    $r = $xslt->transformToXML($doc);
    if ($errors = libxml_get_errors())
	return __xslfilter_do_error("Error while rendering document",
				__xslfilter_build_xmlerrors($errors));

    // Something wrong, but we have no clue
    if ($r == false)
	return __xslfilter_do_error("Error while rendering document",
			    "Check document and associated stylesheets");
  
    // Job's done
    return $r;
}


function xslfilter_remove() {
    $ob_list = ob_list_handlers();

    if ($ob_list[count($ob_list)-1] == 'xslfilter') {
	ob_end_clean();
    } else {
	$msg  = 'The list of output buffer was';
	$msg .= empty($ob_list) ? ' empty' : ':';
	foreach($ob_list as $ob) {
	    $msg .= ' "' . $ob . '"';
	    ob_end_clean();
	}
	echo __xslfilter_do_error('xslfilter not current output buffer', $msg);
	exit;
    }
}


function xslfilter_sanitized($time_limit = null) {
    session_write_close();
    while (ob_end_clean())
	;
    ignore_user_abort(false);
    if ($time_limit !== null)
	set_time_limit($time_limit);
}


function xslfilter_send_file($file, $time_limit=null) {
    if (($fp = fopen($file, 'rb')) === false)
      return false;

    $fi = new finfo(FILEINFO_PRESERVE_ATIME|FILEINFO_MIME);
    if (($content_type = $fi->file($file)) === false)
	$content_type = 'application/octet-stream';
    $file_size = sprintf("%u", filesize($file));

    xslfilter_sanitized($time_limit);
    header('Content-Type: '   . $content_type);
    header('Content-Length: ' . $file_size);
    fpassthru($fp);
    exit;
}

function xslfilter_send_output($process, 
 	 	               $content_type='application/octet-stream',
		               $time_limit=null) {
    xslfilter_sanitized($time_limit); 
    if (! empty($content_type))
	header('Content-Type: '   . $content_type); 
    passthru($process, $errcode);
    exit;
}





global $__xslfilter_pi_disabled;
if (!isset ($__xslfilter_pi_disabled))
    $__xslfilter_pi_disabled = false;

global $__xslfilter_disabled;
if (! $__xslfilter_disabled)
    ob_start("xslfilter");

// Local Variables:
// c-indent-level: 4
// c-basic-offset: 4
// End:
?>
