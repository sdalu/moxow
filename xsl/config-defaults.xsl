<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl ="http://www.w3.org/1999/XSL/Transform"
  xmlns:m   ="http://www.moxow.org/main"
  xmlns:o   ="urn:moxow:3rd-party"
  exclude-result-prefixes="xsl m o"
>

  <!-- Piwik -->
  <xsl:variable name="o:piwik-default-href"
		select="'//piwik.sdalu.com'"/>

</xsl:stylesheet>
