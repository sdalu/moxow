<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:m  ="http://www.moxow.org/main"
  xmlns:str ="http://exslt.org/strings"
  extension-element-prefixes="str"
  exclude-result-prefixes="xsl m"
>

  <!-- ================================================================ -->
  <!-- === Variables                                                === -->
  <!-- ================================================================ -->

  <!-- CDN -->
  <xsl:variable name="m:cdn-googleapis" 
		select="'//ajax.googleapis.com/ajax/libs'"/>
  <xsl:variable name="m:cdn-bootstrap"
		select="'//netdna.bootstrapcdn.com'"/>
  <xsl:variable name="m:cdn-cloudflare"
		select="'//cdnjs.cloudflare.com/ajax/libs'"/>

  <!-- Framework -->
  <xsl:variable name="m:framework"
		select="($m:pagedesc | $m:defaults)/m:framework"/>


  
  <!-- ================================================================ -->
  <!-- === Overriding                                               === -->
  <!-- ================================================================ -->

  <xsl:template name="m:framework-commons">
    <xsl:call-template name="m:framework-moxow"            />
    <xsl:call-template name="m:framework-angularjs"        />
    <xsl:call-template name="m:framework-d3"               />
    <xsl:call-template name="m:framework-jquery"           />
    <xsl:call-template name="m:framework-jquery-ui"        />
    <xsl:call-template name="m:framework-jquery-flot"      />
    <xsl:call-template name="m:framework-popper-js"        />
    <xsl:call-template name="m:framework-bootstrap"        />
    <xsl:call-template name="m:framework-font-awesome"     />
    <xsl:call-template name="m:framework-fancybox"         />
    <xsl:call-template name="m:framework-openlayers"       />
    <xsl:call-template name="m:framework-uri-js"           />
  </xsl:template>

  

  <!-- ================================================================ -->
  <!-- === Frameworks                                               === -->
  <!-- ================================================================ -->
  
  <!-- MoXoW -->
  <xsl:template name="m:framework-moxow">
    <xsl:if test="$m:framework/@moxow">
      <script type="text/javascript" src="/js/common/combo/moxow-base.min.js"/>
      <script type="text/javascript" src="/js/layout/moxow.js"/>

      <script type="text/javascript">
	<xsl:if test="$m:cookie">
	  MoXoW.setOptions({ cookie : { 
	     domain : '<xsl:value-of select="$m:cookie/@domain"/>' } });
	</xsl:if>
	<xsl:if test="$m:lang">
	  MoXoW.setOptions({ lang : '<xsl:value-of select="$m:lang"/>' });
	</xsl:if>
      </script>

      <link rel="stylesheet" type="text/css"
	    href="{$m:cdn-googleapis}/jqueryui/1.10.3/themes/smoothness/jquery-ui.css"/>
    </xsl:if>
  </xsl:template>


  <!-- URI.js -->
  <xsl:template name="m:framework-uri-js">
    <xsl:param name="options"
	       select="str:split($m:framework/@uri-js, '|')"/>
    <xsl:param name="version" select="$options[1]"/>
    <xsl:param name="modules" select="$options[2]"/>
    
    <xsl:if test="$version">
      <script type="text/javascript"
	      src="{$m:cdn-cloudflare}/URI.js/{$version}/URI.min.js"/>

      <xsl:for-each select="str:split($modules, ',')">
	<script type="text/javascript"
		src="{$m:cdn-cloudflare}/URI.js/{$version}/{.}.min.js"/>
      </xsl:for-each>
    </xsl:if>
  </xsl:template>


  <!-- Poppper.js -->
  <xsl:template name="m:framework-popper-js">
    <xsl:param name="version" select="$m:framework/@popper-js"/>

    <xsl:if test="$version">
      <script type="text/javascript"
	      src="{$m:cdn-cloudflare}/popper.js/{$version}/umd/popper.min.js"/>
    </xsl:if>
  </xsl:template>


  <!-- Angular JS -->
  <xsl:template name="m:framework-angularjs">
    <xsl:param name="version" select="$m:framework/@angularjs"/>
    
    <xsl:if test="$version">
      <script type="text/javascript" 
	      src="{$m:cdn-googleapis}/angularjs/{$version}/angular.min.js"/>
    </xsl:if>
  </xsl:template>

  
  <!-- D3 -->
  <xsl:template name="m:framework-d3">
    <xsl:param name="version" select="$m:framework/@d3"/>
    
    <xsl:if test="$version">
      <script type="text/javascript"
	      src="{$m:cdn-cloudflare}/d3/{$version}/d3.min.js"/>
    </xsl:if>
  </xsl:template>

  
  <!-- jQuery -->
  <xsl:template name="m:framework-jquery">
    <xsl:param name="version" select="$m:framework/@jquery"/>
    
    <xsl:if test="$version">
      <script type="text/javascript" 
	      src="{$m:cdn-googleapis}/jquery/{$version}/jquery.min.js"/>
    </xsl:if>
  </xsl:template>

  
  <!-- jQuery Flot -->
  <xsl:template name="m:framework-jquery-flot">
    <xsl:param name="options"
	       select="str:split($m:framework/@jquery-flot, '|')"/>
    <xsl:param name="version" select="$options[1]"/>
    <xsl:param name="modules" select="$options[2]"/>
    
    <xsl:if test="$version">
      <script type="text/javascript"
	      src="{$m:cdn-cloudflare}/flot/{$version}/jquery.flot.min.js"/>

      <xsl:for-each select="str:split($modules, ',')">
	<script type="text/javascript"
		src="{$m:cdn-cloudflare}/flot/{$version}/jquery.flot.{.}.min.js"/>
      </xsl:for-each>
    </xsl:if>
  </xsl:template>

  
  <!-- jQuery UI -->
  <xsl:template name="m:framework-jquery-ui">
    <xsl:param name="version" select="$m:framework/@jquery-ui"/>
    
    <xsl:if test="$version">
      <link   rel="stylesheet" type="text/css"
	      href="{$m:cdn-googleapis}/jqueryui/{$version}/themes/smoothness/jquery-ui.css"/>
      <script type="text/javascript" 
	      src="{$m:cdn-googleapis}/jqueryui/{$version}/jquery-ui.min.js"/>
    </xsl:if>
  </xsl:template>


  <!-- Bootstrap -->
  <xsl:template name="m:framework-bootstrap">
    <xsl:param name="version" select="$m:framework/@bootstrap"/>
    
    <xsl:if test="$version">
      <link rel="stylesheet"
	    href="{$m:cdn-bootstrap}/bootstrap/{$version}/css/bootstrap.min.css"/>
      <script src="{$m:cdn-bootstrap}/bootstrap/{$version}/js/bootstrap.min.js"/>
    </xsl:if>
  </xsl:template>


  <!-- Font Awesome -->
  <xsl:template name="m:framework-font-awesome">
    <xsl:param name="version" select="$m:framework/@font-awesome"/>
    
    <xsl:if test="$version">
      <link rel="stylesheet"
	    href="{$m:cdn-bootstrap}/font-awesome/{$version}/css/font-awesome.min.css"/>
    </xsl:if>
  </xsl:template>

  
  <!-- Fancybox -->
  <xsl:template name="m:framework-fancybox">
    <xsl:param name="version" select="$m:framework/@fancybox"/>    

    <xsl:if test="$version">
      <link rel="stylesheet"
	    href="{$m:cdn-cloudflare}/fancybox/{$version}/jquery.fancybox.css"/>
      <script type="text/javascript"
	      src="{$m:cdn-cloudflare}/fancybox/{$version}/jquery.fancybox.min.js"/>
    </xsl:if>
  </xsl:template>

    
  <!-- OpenLayers -->
  <xsl:template name="m:framework-openlayers">
    <xsl:param name="version" select="$m:framework/@openlayers"/>
    <xsl:if test="$version">
      <link rel="stylesheet"
	    href="{$m:cdn-cloudflare}/ol3/{$version}/ol.min.css"/>
      <script type="text/javascript"
	      src="{$m:cdn-cloudflare}/ol3/{$version}/ol.min.js"/>
    </xsl:if>
  </xsl:template>


</xsl:stylesheet>
