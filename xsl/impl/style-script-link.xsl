<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:m  ="http://www.moxow.org/main"
  exclude-result-prefixes="xsl m"
>

  <!-- ================================================================ -->
  <!-- === Stylesheets                                              === -->
  <!-- ================================================================ -->

  <!-- Defaults -->
  <xsl:template name="m:stylesheets-webpage">
    <xsl:apply-templates select="$m:pagedesc/m:style"/>
  </xsl:template>

  <!-- Webpage -->
  <xsl:template name="m:stylesheets-defaults">
    <xsl:apply-templates select="$m:defaults/m:style"/>
  </xsl:template>

  <!-- Element -->
  <xsl:template match="m:style">
    <xsl:choose>
      <xsl:when test="@href"> <!-- officiel -->
	<link type="text/css" rel="stylesheet" href="{@href}"/>
      </xsl:when>
      <xsl:when test="@src">
	<link type="text/css" rel="stylesheet" href="{@src}"/>
      </xsl:when>
      <xsl:otherwise>
	<style type="text/css">
	  <xsl:value-of select="." disable-output-escaping="yes"/>
	</style>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>


  <!-- ================================================================ -->
  <!-- === Scripts                                                  === -->
  <!-- ================================================================ -->

  <!-- Defaults -->
  <xsl:template name="m:scripts-defaults">
    <xsl:apply-templates select="$m:defaults/m:script"/>
  </xsl:template>

  <!-- Webpage -->
  <xsl:template name="m:scripts-webpage">
    <xsl:apply-templates select="$m:pagedesc/m:script"/>
  </xsl:template>

  <!-- Element -->
  <xsl:template match="m:script">
    <script type="text/javascript">
      <xsl:if test="@async">
	<xsl:attribute name="async">
	  <xsl:text>async</xsl:text>
	</xsl:attribute>
      </xsl:if>
      <xsl:choose>
	<xsl:when test="@src"> <!-- officiel -->
	  <xsl:copy-of select="@src"/>
	  <xsl:text> </xsl:text>
	</xsl:when>
	<xsl:when test="@href">
	  <xsl:copy-of select="@href"/>
	  <xsl:text> </xsl:text>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:value-of select="." disable-output-escaping="yes"/>
	</xsl:otherwise>
      </xsl:choose>
    </script>
  </xsl:template>


  <!-- ================================================================ -->
  <!-- === Links                                                    === -->
  <!-- ================================================================ -->

  <!-- Links (with: m:link) -->
  <xsl:template name="m:head-links">
    <xsl:for-each select="$m:pagedesc/m:link">
      <link>
	<xsl:copy-of select="@*"/>
      </link>
    </xsl:for-each>	
  </xsl:template>

</xsl:stylesheet>
