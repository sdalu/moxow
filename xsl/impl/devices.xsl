<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:m  ="http://www.moxow.org/main"
  exclude-result-prefixes="xsl m"
>
  <!-- ================================================================ -->
  <!-- === Devices                                                  === -->
  <!-- ================================================================ -->

  <!-- Viewport -->
  <xsl:template name="m:device-viewport">
    <!-- See: http://www.quirksmode.org/mobile/viewports2.html -->
    <xsl:param name="mode" select="$m:defaults/m:viewport/@mode"/>
    <xsl:if test="$mode">
      <meta name="viewport">
	<xsl:attribute name="content">
	  <xsl:choose>
	    <xsl:when test="$mode = 'no-scale'">
	      <xsl:text>width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no</xsl:text>
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:message terminate="yes">
		unsupported viewport mode <xsl:value-of select="$mode"/>
	      </xsl:message>
	    </xsl:otherwise>
	  </xsl:choose>
	</xsl:attribute>
      </meta>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
