<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:m  ="http://www.moxow.org/main"
  exclude-result-prefixes="xsl m"
>

  <!-- ================================================================ -->
  <!-- === Navigation                                               === -->
  <!-- ================================================================ -->

  <!-- Robots rules -->
  <xsl:template name="m:navigation-robots">
    <xsl:choose>
      <xsl:when test="$m:pagedesc/m:robots/@index  = 'no' and
		      $m:pagedesc/m:robots/@follow = 'no'">
	<meta name="robots" content="noindex,nofollow"/>
      </xsl:when>
      <xsl:when test="$m:pagedesc/m:robots/@index  = 'no'">
	<meta name="robots" content="noindex"/>
      </xsl:when>
      <xsl:when test="$m:pagedesc/m:robots/@follow = 'no'">
	<meta name="robots" content="nofollow"/>
      </xsl:when>
    </xsl:choose>
    <xsl:if test="$m:pagedesc/m:robots/@rules">
      <meta name="robots"
	    content="{$m:pagedesc/m:robots/@rules}"/>
    </xsl:if>
  </xsl:template>

  <!-- Alternate language -->
  <xsl:template name="m:navigation-alternate-lang">
    <xsl:for-each select="$m:pagedesc/m:lang">
      <xsl:variable name="href">
	<xsl:call-template name="uri-self-i18n-lang">
	  <xsl:with-param name="m-lang"  select="."/>
	  <xsl:with-param name="current" select="$m:lang"/>
	</xsl:call-template>
      </xsl:variable>
      <link rel="alternate" hreflang="{@isocode}" href="{$href}"/>
    </xsl:for-each>
  </xsl:template>
	
  <!-- News feed -->
  <xsl:template name="m:navigation-feeds">
    <xsl:for-each select="$m:pagedesc/m:feed">
      <link rel="alternate" type="application/atom+xml" href="{@href}">
	<xsl:copy-of select="@type|@title"/>
      </link>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>
