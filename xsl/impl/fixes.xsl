<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:m  ="http://www.moxow.org/main"
  exclude-result-prefixes="xsl m"
>
  
  <!-- ================================================================ -->
  <!-- === Fixes                                                    === -->
  <!-- ================================================================ -->

  <!-- HTML5 compat -->
  <xsl:template name="m:fix-html5-compat">
    <xsl:if test="$m:defaults/m:feature/@html5-compat">
      <xsl:call-template name="m:_fix-ie-html5"/>
    </xsl:if>
  </xsl:template>

  <!-- Javascript compat -->
  <xsl:template name="m:fix-javascript-compat">
    <xsl:if test="$m:defaults/m:feature/@js-compat">
      <xsl:call-template name="m:_fix-javascript-compat"/>
    </xsl:if>
  </xsl:template>

  
  <!-- ================================================================ -->
  <!-- === Fix for HTML5 on IE < 9                                  === -->
  <!-- ================================================================ -->
  <!-- FIX FROM: http://html5shiv.googlecode.com/svn/trunk/html5.js -->

  <xsl:template name="m:_fix-ie-html5">
    <!-- [IE] -->
    <xsl:text disable-output-escaping="yes">
      <![CDATA[<!--[if lt IE 9]>]]>
    </xsl:text>
    <script><![CDATA[
(function(l,f){function m(){var a=e.elements;return"string"==typeof a?a.split(" "):a}function i(a){var b=n[a[o]];b||(b={},h++,a[o]=h,n[h]=b);return b}function p(a,b,c){b||(b=f);if(g)return b.createElement(a);c||(c=i(b));b=c.cache[a]?c.cache[a].cloneNode():r.test(a)?(c.cache[a]=c.createElem(a)).cloneNode():c.createElem(a);return b.canHaveChildren&&!s.test(a)?c.frag.appendChild(b):b}function t(a,b){if(!b.cache)b.cache={},b.createElem=a.createElement,b.createFrag=a.createDocumentFragment,b.frag=b.createFrag();
a.createElement=function(c){return!e.shivMethods?b.createElem(c):p(c,a,b)};a.createDocumentFragment=Function("h,f","return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&("+m().join().replace(/[\w\-]+/g,function(a){b.createElem(a);b.frag.createElement(a);return'c("'+a+'")'})+");return n}")(e,b.frag)}function q(a){a||(a=f);var b=i(a);if(e.shivCSS&&!j&&!b.hasCSS){var c,d=a;c=d.createElement("p");d=d.getElementsByTagName("head")[0]||d.documentElement;c.innerHTML="x<style>article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}</style>";
c=d.insertBefore(c.lastChild,d.firstChild);b.hasCSS=!!c}g||t(a,b);return a}var k=l.html5||{},s=/^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,r=/^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,j,o="_html5shiv",h=0,n={},g;(function(){try{var a=f.createElement("a");a.innerHTML="<xyz></xyz>";j="hidden"in a;var b;if(!(b=1==a.childNodes.length)){f.createElement("a");var c=f.createDocumentFragment();b="undefined"==typeof c.cloneNode||
"undefined"==typeof c.createDocumentFragment||"undefined"==typeof c.createElement}g=b}catch(d){g=j=!0}})();var e={elements:k.elements||"abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",version:"3.7.0",shivCSS:!1!==k.shivCSS,supportsUnknownElements:g,shivMethods:!1!==k.shivMethods,type:"default",shivDocument:q,createElement:p,createDocumentFragment:function(a,b){a||(a=f);
if(g)return a.createDocumentFragment();for(var b=b||i(a),c=b.frag.cloneNode(),d=0,e=m(),h=e.length;d<h;d++)c.createElement(e[d]);return c}};l.html5=e;q(f)})(this,document);
    ]]></script>
    <!-- [/IE] -->
    <xsl:text disable-output-escaping="yes">
      <![CDATA[<![endif]-->]]>
    </xsl:text>
  </xsl:template>


  <xsl:template name="m:_fix-javascript-compat">
    <!-- [IE] -->
    <xsl:text disable-output-escaping="yes">
      <![CDATA[<!--[if lt IE 9]>]]>
    </xsl:text>
    <script><![CDATA[
(function(prototype){var key,value,tmp;if(!Object.prototype.hasOwnProperty){Object.prototype.hasOwnProperty=function(key){var result=false,value;key=String(key);if(/constructor|toString|valueOf/.test(key)){result=Object.prototype[key]!==this[key]}else{for(value in this){if(value===key){result=this===Object.prototype?false:Object.prototype[key]!==this[key];break}}}return result}}for(key in prototype){if(prototype.hasOwnProperty(key)){switch(typeof prototype[key]){case"function":if(!this[key]){this[key]=prototype[key]}break;case"object":for(value in prototype[key]){if(prototype[key].hasOwnProperty(value)){if(!this[key].prototype[value]||(prototype[key][value].test&&prototype[key][value].test())){this[key].prototype[value]=prototype[key][value].callback}}}break}}}try{prototype=key=value=undefined}catch(e){this.undefined=prototype=key=value=tmp}
/*@cc_on
	(function(callback){
		this.setTimeout = callback(this.setTimeout);
		this.setInterval = callback(this.setInterval);
	})(function(value){
		return	function(callback, i){
			var	tmp = Array.prototype.slice.call(arguments, 2);
			if(typeof callback != "function")
				callback = new Function(callback);
			return	value(function(){callback.apply(this, tmp);}, i);
		};
	});
	XMLHttpRequest = (function(XMLHttpRequest){
		function init(XMLHttpRequest){
			if(!XMLHttpRequest){
				var	tmp = ["MSXML2.XMLHTTP.3.0", "MSXML2.XMLHTTP", "Microsoft.XMLHTTP"];
				while(!XMLHttpRequest && tmp.length){
					try{XMLHttpRequest = new ActiveXObject(tmp.shift())}
					catch(e){}
				};
			}
			else
				XMLHttpRequest = new XMLHttpRequest;
			return	XMLHttpRequest;
		};
		return	function(){
			var	Object = this;
			(this["class"] = init(XMLHttpRequest)).onreadystatechange = function(){
				var	XMLHttpRequest = Object["class"];
				if(Object["synchronized"]()){
					Object.responseText = XMLHttpRequest.responseText;
					Object.responseXML = XMLHttpRequest.responseXML;
					Object.status = XMLHttpRequest.status;
					Object.statusText = XMLHttpRequest.statusText;
				};
				if(Object.onreadystatechange)
					Object.onreadystatechange.call(Object.onreadystatechange);
			};
		};
	})(this.XMLHttpRequest);
	XMLHttpRequest.prototype = {
		abort:function(){
			this["class"].abort();
		},
		getAllResponseHeaders:function(){
			return	this["synchronized"]() ? this["class"].getAllResponseHeaders() : [];
		},
		getResponseHeader:function(key){
			return	this["synchronized"]() ? this["class"].getResponseHeader(key) : null;
		},
		open:function(){
			this["class"].open(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
		},
		send:function(value){
			this["class"].send(value || null);
		},
		setRequestHeader:function(key, value){
			return	this["class"].setRequestHeader(key, value);
		},
		"synchronized":function(){
			return	(this.readyState = this["class"].readyState) == 4;
		},
		readyState:0,
		status:0,
		responseText:"",
		statusText:"",
		responseXML:null
	};
	@*/
})({Array:{every:{callback:function(e){for(var b=0,a=true,c=arguments[1]||null,d=this instanceof Array;b<this.length&&a;b++){if(d?this.hasOwnProperty(b):this[b]!==undefined){a=e.call(c,this[b],b,this)}}return a}},filter:{callback:function(e){for(var b=0,a=[],c=arguments[1]||null,d=this instanceof Array;b<this.length;b++){if((d?this.hasOwnProperty(b):this[b]!==undefined)&&e.call(c,this[b],b,this)){a[a.length]=this[b]}}return a}},forEach:{callback:function(d){for(var a=0,b=arguments[1]||null,c=this instanceof Array;a<this.length;a++){if(c?this.hasOwnProperty(a):this[a]!==undefined){d.call(b,this[a],a,this)}}}},indexOf:{callback:function(c,b){for(var a=this.length,b=b<0?b+a<0?0:b+a:b||0;b<a&&this[b]!==c;b++){}return a<=b?-1:b}},lastIndexOf:{callback:function(b,a){return Array.prototype.slice.call(this).reverse().indexOf(b,a)}},map:{callback:function(e){for(var b=0,a=new Array(this.length),c=arguments[1]||null,d=this instanceof Array;b<this.length;b++){if(d?this.hasOwnProperty(b):this[b]!==undefined){a[b]=e.call(c,this[b],b,this)}}return a}},pop:{callback:function(c){var b=this.length,a;if(b){a=this[--b]}this.length=b;return a}},push:{callback:function(){for(var a=0;a<arguments.length;a++){this[this.length]=arguments[a]}return this.length}},shift:{callback:function(){var a=Array.prototype.reverse.call(this).pop();Array.prototype.reverse.call(this);return a}},some:{callback:function(e){for(var b=0,a=false,c=arguments[1]||null,d=this instanceof Array;b<this.length&&!a;b++){if(d?this.hasOwnProperty(b):this[b]!==undefined){a=e.call(c,this[b],b,this)}}return a}},splice:{callback:function(){var a=[],e=[],d=0,c=this.length,b=0,f=arguments.length;switch(f){case 0:break;case 1:arguments[1]=!f++||this.length-arguments[0];default:b=arguments[0]+arguments[1];for(;d<c;d++){if(d<arguments[0]||d>=b){if(d===b&&f>2){for(d=2;d<f;d++){e[e.length]=arguments[d]}d=b}e[e.length]=this[d]}else{a[a.length]=this[d]}}for(d=0,c=e.length;d<c;d++){this[d]=e[d]}this.length=d;break}return a}},unshift:{test:function(){var a=[4,5];return !(a.unshift(1,2,3)===5&&a.join("-")==="1-2-3-4-5")},callback:function(){var a=arguments.length;Array.prototype.reverse.call(this);while(a--){this[this.length]=arguments[a]}return Array.prototype.reverse.call(this).length}}},Date:{getYear:{test:function(){return String((new Date).getYear()).length===4},callback:function(){return this.getFullYear()-1900}},setYear:{test:function(){return String((new Date).getYear()).length===4},callback:function(a){return this.setFullYear(a+1900)}}},Function:{apply:{callback:function(scope,arguments){var i=arguments?arguments.length:0,result=new Array(i),call;switch(typeof scope){case"boolean":scope=new Boolean(scope);break;case"number":scope=new Number(scope);break;case"string":scope=new String(scope);break;default:scope=scope||(function(){return this})();break}do{call="*"+Math.random()}while(scope[call]!==undefined);while(i--){result[i]="arguments["+i+"]"}scope[call]=this;result=eval("scope[call]("+result.join(",")+")");try{delete scope[call]}catch(e){scope[call]=undefined}return result}},call:{callback:function(c){for(var b=1,a=[];b<arguments.length;b++){a[a.length]=arguments[b]}return this.apply(c,a)}}},Number:{toFixed:{callback:function(b){var a=Math.pow(10,parseInt(b)),c=new Array(2);a=String(Math.round(this*a)/a);if(b>0){c=a.split(".");c[1]=(c[1]||"")+new Array(++b-(c[1]?c[1].length:0)).join("0");a=c.join(".")}return a}}},String:{lastIndexOf:{callback:function(c,b){var d=String(this),a=d.split("").reverse().join("").indexOf(String(c).split("").reverse().join(""),b||0);return a<0?a:d.length-a}},replace:{test:function(){return"aa".replace(/a/g,function(d,c){return c+" "})!="0 1 "},callback:(function(a){return function(f,h){var g=String(this),b=g;if(typeof h!="function"){b=a.apply(g,arguments)}else{var c=!/\/.*g.*$/.test(f),e=0,d=[];while(arguments=f.exec(b)){d[d.length]=b.substr(0,arguments.index);d[d.length]=h.apply(null,arguments.concat([g.indexOf(arguments[0],e),g]));b=g.substring(e+=(arguments.index+arguments[0].length));if(c){break}}b=d.join("")+b}return b}})(String.prototype.replace)}},decodeURIComponent:function(a){return String(a).replace(/(%F[0-9A-F]%E[0-9A-F]%[A-B][0-9A-F]%[8-9A-B][0-9A-F])|(%E[0-9A-F]%[A-B][0-9A-F]%[8-9A-B][0-9A-F])|(%[C-D][0-9A-F]%[8-9A-B][0-9A-F])|(%[0-9A-F]{2})/g,function(f,e,b,c,d){return String.fromCharCode(d?parseInt(d.substr(1,2),16):c?((parseInt(c.substr(1,2),16)-192)<<6)+(parseInt(c.substr(4,2),16)-128):b?((parseInt(b.substr(1,2),16)-224)<<12)+((parseInt(b.substr(4,2),16)-128)<<6)+(parseInt(b.substr(7,2),16)-128):((parseInt(e.substr(1,2),16)-240)<<18)+((parseInt(e.substr(4,2),16)-128)<<12)+((parseInt(e.substr(7,2),16)-128)<<6)+(parseInt(e.substr(10,2),16)-128))})},encodeURIComponent:function(b){function a(d,e){var c=d.toString(16).toUpperCase();e=(e||2)-c.length;return e==0?c:new Array(++e).join("0").concat(c)}return String(b).replace(/[\x00-\x20]|[\x25|\x3C|\x3E|\x5B|\x5D|\x5E|\x60|\x7F]|[\x7B-\x7D]|[\x80-\uFFFF]/g,function(e){var d=e.charCodeAt(0),c=[];if(d<128){c[c.length]=a(d)}else{if(d<2048){c[c.length]=a(192+(d>>6));c[c.length]=a(128+(d&63))}else{if(d<65536){c[c.length]=a(224+(d>>12));c[c.length]=a(128+(d>>6&63));c[c.length]=a(128+(d&63))}else{c[c.length]=a(240+(d>>18));c[c.length]=a(128+(d>>12&63));c[c.length]=a(128+(d>>6&63));c[c.length]=a(128+(d&63))}}}return"%"+c.join("%")}).replace(/[\x23|\x24|\x26|\x2B|\x2C|\x2F|\x3A|\x3B|\x3D|\x3F|\x40]/g,function(c){return"%"+a(c.charCodeAt(0))}).replace(/"/g,"%22").replace(/\\/g,"%5C")},execScript:function(value){var tmp=document.createElement("script");tmp.type="text/javascript";tmp.appendChild(document.createTextNode(value));with(document.documentElement){removeChild(appendChild(tmp))}}});
    ]]></script>
    <!-- [/IE] -->
    <xsl:text disable-output-escaping="yes">
      <![CDATA[<![endif]-->]]>
    </xsl:text>
  </xsl:template>

</xsl:stylesheet>
