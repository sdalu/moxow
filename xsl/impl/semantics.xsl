<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:m  ="http://www.moxow.org/main"
  exclude-result-prefixes="xsl m"
>

  <xsl:template name="m:title">
      <xsl:value-of select="$m:pagedesc/m:title"/>
  </xsl:template>


  
  <!-- ================================================================ -->
  <!-- === OpenGraph                                                === -->
  <!-- ================================================================ -->

  <xsl:template name="m:opengraph-locale">
    <meta property="og:locale" content="{$m:locale}"/>
  </xsl:template>
  
  <xsl:template name="m:opengraph-site-name">
    <xsl:if test="$m:website/@name">
      <meta property="og:site_name" content="{$m:website/@name}" />
    </xsl:if>
  </xsl:template>

  <xsl:template name="m:opengraph-title">
    <xsl:if test="$m:pagedesc/m:title">
      <meta property="og:title">
	<xsl:attribute name="content">
	  <xsl:call-template name="m:title"/>
	</xsl:attribute>
      </meta>
    </xsl:if>
  </xsl:template>

  <xsl:template name="m:opengraph-defaults">
    <xsl:call-template name="m:opengraph-locale"/>
    <xsl:call-template name="m:opengraph-site-name"/>
    <xsl:call-template name="m:opengraph-title"/>
  </xsl:template>

  <xsl:template name="m:opengraph-others"/>


  
  <!-- ================================================================ -->
  <!-- === SchemaOrg                                                === -->
  <!-- ================================================================ -->

  <xsl:template name="m:schema-org-defaults"/>
  <xsl:template name="m:schema-org-others"/>

  
  
  <!-- ================================================================ -->
  <!-- === Semantics                                                === -->
  <!-- ================================================================ -->

  <xsl:template name="m:semantic-authorship">
    <xsl:call-template name="m:authorship-copyright"/>
    <xsl:call-template name="m:authorship-author-profile"/>
  </xsl:template>

  <xsl:template name="m:semantic-description">
    <xsl:call-template name="m:description-browser-title"/>
    <xsl:call-template name="m:description-content"/>
    <xsl:call-template name="m:description-keywords"/>
    <xsl:call-template name="m:description-icon"/>
  </xsl:template>


  <xsl:template name="m:semantic-opengraph">
    <!-- See:   http://ogp.me/                                      -->
    <!-- See:   http://fbdevwiki.com/wiki/Open_Graph_protocol       -->
    <!-- Debug: https://developers.facebook.com/tools/debug         -->
    <xsl:call-template name="m:opengraph-defaults"/>
    <xsl:call-template name="m:opengraph-others"/>
    <xsl:for-each select="$m:pagedesc/m:og">
      <meta property="og:{@property}" content="{@content}"/>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="m:semantic-schema-org">
    <!-- See:   http://schema.org/                                  -->
    <!-- See:   https://developers.google.com/+/web/snippet/        -->
    <!-- Debug: http://www.google.com/webmasters/tools/richsnippets -->
    <xsl:call-template name="m:schema-org-defaults"/>
    <xsl:call-template name="m:schema-org-others"/>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- === Description                                              === -->
  <!-- ================================================================ -->

  <!-- Title -->
  <xsl:template name="m:description-browser-title">
    <title>
      <xsl:value-of select="$m:pagedesc/m:title"/>
    </title>
  </xsl:template>

  <!-- Icon -->
  <xsl:template name="m:description-icon">
    <xsl:if test="$m:website/@icon">
      <link rel="icon" href="{$m:website/@icon}"/>
    </xsl:if>
  </xsl:template>

  <!-- Content -->
  <xsl:template name="m:description-content">
    <xsl:if test="$m:pagedesc/m:description">
      <meta name="description" 
	    content="{$m:pagedesc/m:description}"/>
    </xsl:if>
  </xsl:template>


  <!-- Keywords -->
  <xsl:template name="m:description-keywords">
    <xsl:if test="$m:pagedesc/m:keyword">
      <meta name="keywords">
	<xsl:attribute name="content">          
	  <xsl:for-each select="$m:pagedesc/m:keyword">
	    <xsl:value-of select="."/>
	    <xsl:if test="position() != last()">,</xsl:if>              
	  </xsl:for-each>
	</xsl:attribute>          
      </meta>
    </xsl:if>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- === Authorship                                               === -->
  <!-- ================================================================ -->

  <xsl:template name="m:authorship-copyright">
    <xsl:if test="$m:author/@name">
      <meta name="author" content="{$m:author/@name}"/>
      <xsl:if test="$m:copyright">
	<meta name="copyright" content="© {$m:copyright/@date} {$m:author/@name}"/>
      </xsl:if>
    </xsl:if>
  </xsl:template>

  <xsl:template name="m:authorship-author-profile">
    <xsl:if test="$m:author/@href and 
		  not($m:pagedesc/m:link/@rel = 'author')">
      <link rel="author" href="{$m:author/@href}"/>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
