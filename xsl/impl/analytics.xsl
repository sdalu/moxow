<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:m  ="http://www.moxow.org/main"
  xmlns:o  ="urn:moxow:3rd-party"
  exclude-result-prefixes="xsl m o"
>

  <!-- ================================================================ -->
  <!-- === Variables                                                === -->
  <!-- ================================================================ -->

  <!-- User Analytics -->
  <xsl:variable name="m:analytics" 
		select="($m:pagedesc | $m:defaults)/m:analytics" />


  
  <!-- ================================================================ -->
  <!-- === Overriding                                               === -->
  <!-- ================================================================ -->

  <!-- Analytics -->
  <xsl:template name="m:body-analytics">
    <!-- Analytics code -->
    <xsl:for-each select="$m:analytics">
      <xsl:if test="normalize-space(@id)">
	<xsl:choose>
	  <!-- Google Analytics -->
	  <xsl:when test="@type = 'google-analytics'">
	    <xsl:call-template name="o:google-analytics">
	      <xsl:with-param name="id"     select="@id"    />
	      <xsl:with-param name="domain" select="@domain"/>
	    </xsl:call-template>
	  </xsl:when>
	  <!-- Piwik -->
	  <xsl:when test="@type = 'piwik' or not(@type)">
	    <xsl:call-template name="o:piwik-tracker">
	      <xsl:with-param name="id"     select="@id"    />
	      <xsl:with-param name="href"   select="@href"  />
	      <xsl:with-param name="domain" select="@domain"/>
	    </xsl:call-template>
	  </xsl:when> 
	</xsl:choose>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>
