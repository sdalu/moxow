<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:m  ="http://www.moxow.org/main"
  exclude-result-prefixes="xsl m"
>

  <!-- ================================================================ -->
  <!-- === Comments                                                 === -->
  <!-- ================================================================ -->

  <!-- Copyright informations -->
  <xsl:template name="m:comments-copyright">
    <xsl:if test="$m:author/@name and $m:copyright/@date">
      <xsl:comment>
	<xsl:text> Copyright (c) </xsl:text>
	<xsl:value-of select="$m:author/@name"/>
	<xsl:text>  </xsl:text>
	<xsl:value-of select="$m:copyright/@date"/>
	<xsl:text> </xsl:text>
      </xsl:comment>
      <xsl:text>&#10;</xsl:text>
    </xsl:if>
  </xsl:template>

  <!-- Website reference -->
  <xsl:template name="m:comments-website">
    <xsl:if test="$m:website/@href">
      <xsl:comment>
	<xsl:text> </xsl:text>
	<xsl:value-of select="$m:website/@href"/>
	<xsl:text> </xsl:text>
      </xsl:comment>
      <xsl:text>&#10;</xsl:text>
    </xsl:if>
  </xsl:template>

  <!-- Page served in ... time -->
  <xsl:template name="m:comments-served-in">
    <xsl:if test="$m:pagedesc/m:served/@in">
      <xsl:comment>
	<xsl:text> Served in </xsl:text>
	<xsl:value-of select="$m:pagedesc/m:served/@in"/>
	<xsl:text> sec </xsl:text>
      </xsl:comment>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
