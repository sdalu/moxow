<?xml version="1.0" encoding="utf-8"?>

<!DOCTYPE xsl:stylesheet [
<!ENTITY % HTMLsymbol PUBLIC
  "-//W3C//ENTITIES Symbols for XHTML//EN" "xhtml-symbol.ent">
%HTMLsymbol;

<!ENTITY % HTMLspecial PUBLIC
  "-//W3C//ENTITIES Special for XHTML//EN" "xhtml-special.ent">
%HTMLspecial;
]>


<xsl:stylesheet version="1.0"
  xmlns:xsl ="http://www.w3.org/1999/XSL/Transform"
  xmlns:func="http://exslt.org/functions"
  xmlns:m   ="http://www.moxow.org/main"
  xmlns:d   ="http://www.moxow.org/data"
  xmlns:f   ="http://www.moxow.org/function"
  extension-element-prefixes="func"
  exclude-result-prefixes="xsl m d f"
>


  <!-- == Import stylesheet =========================================== -->

  <xsl:import href="core/namespace-removal.xsl"   />
  <xsl:import href="core/functions.xsl"           />
  <xsl:import href="core/helpers.xsl"             />

  <xsl:import href="core/html.xsl"                />
  <xsl:import href="core/html-head-body.xsl"      />
  <xsl:import href="core/html-skeleton.xsl"       />
  <xsl:import href="core/html-layout.xsl"         />

  <xsl:import href="impl/style-script-link.xsl"   />
  <xsl:import href="impl/frameworks.xsl"          />
  <xsl:import href="impl/comments.xsl"            />
  <xsl:import href="impl/semantics.xsl"           />
  <xsl:import href="impl/navigations.xsl"         />
  <xsl:import href="impl/devices.xsl"             />


  <!-- Authorship -->
  <xsl:variable name="m:author"
		select="($m:pagedesc | $m:defaults)/m:author"   />
  <xsl:variable name="m:copyright" 
		select="$m:pagedesc/m:copyright"                /> 

  <!-- Website -->
  <xsl:variable name="m:website"   
		select="m:lang(($m:pagedesc | $m:defaults)/m:website)"  />
  <xsl:variable name="m:webmaster"
		select="($m:pagedesc | $m:defaults)/m:webmaster"/>
  <xsl:variable name="m:cookie"
		select="($m:pagedesc | $m:defaults)/m:cookie"   />
  <xsl:variable name="m:i18n"
		select="($m:pagedesc | $m:defaults)/m:i18n"     />
  <xsl:variable name="m:feature"
		select="($m:pagedesc | $m:defaults)/m:feature"  />
  
  <!-- ================================================================ -->
  <!-- == Parameters                                                 == -->
  <!-- ================================================================ -->

  <xsl:param name="pagedesc-default-xml"  />
  <xsl:param name="expect-lang"           />
  <xsl:param name="lastmod"               />
  <xsl:param name="now"                   />
  <xsl:param name="http-referer"          />
  <xsl:param name="request-uri"           />
  <xsl:param name="server-name"           />
  <xsl:param name="https"                 />
  <xsl:param name="protocol"              />
  <xsl:param name="uri-scheme"            />
  <xsl:param name="uri-host"              />
  <xsl:param name="uri-port"              />
  <xsl:param name="uri-path"              />
  <xsl:param name="uri-query"             />
  <xsl:param name="uri-fragment"          />

  <xsl:variable name="uri-scheme-host-port">
    <xsl:value-of select="$uri-scheme"/>
    <xsl:text>://</xsl:text>
    <xsl:value-of select="$uri-host"/>
    <xsl:if test="$uri-port">
      <xsl:value-of select="$uri-port"/>
    </xsl:if>
  </xsl:variable>
  
  <!-- ================================================================ -->
  <!-- == Constants helper                                           == -->
  <!-- ================================================================ -->

  <!-- Document root / content -->
  <xsl:variable name="m:moxow"   select="/m:moxow"          />
  <xsl:variable name="m:content" select="$m:moxow/m:content"/>
 
  <!-- Language of the page being served (default to english) -->
  <xsl:variable name="m:lang"
		select="f:if-absent($m:moxow/@lang, 'en')"/>

  <!-- Most probable locale infered from lang -->
  <xsl:variable name="m:locale">
    <xsl:choose>
      <xsl:when test="$m:lang = 'fr'">fr_FR</xsl:when>
      <xsl:otherwise                 >en_GB</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <!-- Page description -->
  <xsl:variable name="m:pagedesc" select="$m:moxow/m:pagedesc"/>

  <!-- Defaults -->
  <xsl:variable name="m:defaults-path">
    <xsl:choose>
      <!-- Auto      -->
      <xsl:when test="$m:pagedesc/@default = 'auto'">
	<xsl:value-of select="$pagedesc-default-xml"/>
      </xsl:when>
      <!-- None      -->
      <xsl:when test="$m:pagedesc/@default = 'none'"/>
      <!-- Specified -->
      <xsl:when test="$m:pagedesc/@default">
	<xsl:value-of select="$m:pagedesc/@default"/>
      </xsl:when>
      <!-- Auto      -->
      <xsl:otherwise>
	<xsl:value-of select="$pagedesc-default-xml"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="m:defaults" 
		select="document($m:defaults-path)/m:pagedesc|
			document($m:defaults-path)/m:defaults"/>

  
  <!-- Page description (and default) -->
  <xsl:variable name="m:pagedesc-dflt" select="$m:defaults"/>



  <!-- == Variables to be overwritten ================================= -->

  <xsl:variable name="m:body-class"/>

  <xsl:variable  name="m:resource-path" select="'/common/'"/>
  <func:function name="m:resource-path">
    <xsl:param name="file"/>
    <xsl:param name="path" select="$m:resource-path"/>
    <func:result select="concat($path, $file)"/>
  </func:function>








  <!-- ================================================================ -->
  <!-- === Output                                                   === -->
  <!-- ================================================================ -->

  <xsl:output method="html" indent="no" encoding="utf-8"
	      standalone="yes" omit-xml-declaration="yes"/>



  <!-- ================================================================ -->
  <!-- === MoXoW page                                               === -->
  <!-- ================================================================ -->
  <xsl:template match="/m:moxow">
    <!-- Sanity check -->
    <xsl:if test="$m:pagedesc/m:lang and not(@lang)">
      <xsl:message terminate="yes">
	m:moxow/@lang must be defined when using m:pagedesc/m:lang
      </xsl:message>
    </xsl:if>

    <!-- Fake doctype (as we have omited xml declaration) -->
    <xsl:text disable-output-escaping="yes"
	      >&lt;!DOCTYPE html&gt;&#10;</xsl:text>

    <!-- Render HTML page -->
    <xsl:call-template name="m:html"/>
  </xsl:template>

</xsl:stylesheet>

