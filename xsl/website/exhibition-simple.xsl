<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: exhibition-simple.xsl 3275 2013-04-06 18:36:25Z sdalu $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl ="http://www.w3.org/1999/XSL/Transform"
  xmlns:m   ="http://www.moxow.org/main"
  xmlns:f   ="http://www.moxow.org/function"
  xmlns:g   ="http://www.moxow.org/add-on/gallery"

  exclude-result-prefixes="xsl m f g"
>

  <xsl:import href="/web/MoXoW/xsl/main.xsl"                />
  <xsl:import href="/web/MoXoW/xsl/add-on/gallery.xsl"      />
  <xsl:import href="/web/MoXoW/xsl/add-on/exhibition.xsl"   />
  <xsl:import href="/web/MoXoW/xsl/add-on/shopping-cart.xsl"/>

  <!-- Performs add-on initialisation -->
  <xsl:template name="m:head-others">
    <xsl:apply-imports/>
    <xsl:call-template name= "g:init-do-head"/>
  </xsl:template>


  <!-- Default values -->
  <xsl:variable name="g:gallery-class"     select="'gallery imgbrowse'"/>
  <xsl:variable name="g:gallery-behaviour" select="'filmstrip'"        />

</xsl:stylesheet>
