<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: amazon.xsl 2788 2012-04-14 22:25:49Z sdalu $ -->

<xsl:stylesheet version="1.0"
  xmlns:h="http://www.w3.org/1999/xhtml" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:m="http://www.moxow.org/main"
  exclude-result-prefixes="h xsl m"
>

  <!-- For documentation see:
       https://partenaires.amazon.fr/gp/associates/network/tools/link-checker/main.html 
  -->

  <xsl:variable name="amazon-tag"/>
  <xsl:variable name="amazon-domain">
    <xsl:choose>
      <xsl:when test="$m:lang = 'fr'">fr</xsl:when>
      <xsl:otherwise                 >co.uk</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <!-- ================================================================ -->
  <!-- === Building Amazon picture URL                              === -->
  <!-- ================================================================ -->
  <xsl:template name="amazon-build-picture-url">
    <xsl:param name="asin"/>
    <xsl:param name="size"/>
    <xsl:text>http://images.amazon.com/images/P/</xsl:text>
    <xsl:value-of select="$asin"/>
    <xsl:text>.03.</xsl:text>
    <xsl:choose>
      <xsl:when test="$size = 'large'">LZZZZZZZ</xsl:when>
      <xsl:when test="$size = 'thumb'">THUMBZZZ</xsl:when>
      <xsl:otherwise                  >MZZZZZZZ</xsl:otherwise>
    </xsl:choose>
    <xsl:text>.jpg</xsl:text>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- === Picture                                                  === -->
  <!-- ================================================================ -->

  <xsl:template match="m:amazon-picture">
    <img alt="{@asin}">
      <xsl:copy-of select="@id|@title|@alt|@class|@style"/>
      <xsl:attribute name="src">
	<xsl:call-template name="amazon-build-picture-url">
	  <xsl:with-param name="asin" select="@asin"/>
	  <xsl:with-param name="size" select="@size"/>
	</xsl:call-template>
      </xsl:attribute>
    </img>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- === Link                                                     === -->
  <!-- ================================================================ -->

  <xsl:template match="m:amazon-link">
    <xsl:param name="size" select="@size"/>

    <xsl:variable name="query">
      <xsl:if test="normalize-space($amazon-tag)">
	<xsl:text>?tag=</xsl:text><xsl:value-of select="$amazon-tag"/>
      </xsl:if>
    </xsl:variable>

    <a href="http://www.amazon.{$amazon-domain}/dp/{@asin}/{$query}">
      <xsl:copy-of select="@id|@title|@class|@style"/>
      <xsl:choose>
	<xsl:when test="node()">
	  <xsl:apply-templates select="node()"/>
	</xsl:when>
	<xsl:otherwise>
	  <img alt="{@asin}">
	    <xsl:copy-of select="@alt"/>
	    <xsl:attribute name="src">
	      <xsl:call-template name="amazon-build-picture-url">
		<xsl:with-param name="asin" select="@asin"/>
		<xsl:with-param name="size" select="$size"/>
	      </xsl:call-template>
	    </xsl:attribute>
	  </img>
	</xsl:otherwise>
      </xsl:choose>
    </a>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- === Folder                                                   === -->
  <!-- ================================================================ -->
  <xsl:template match="m:amazon-folder">
    <div>
      <xsl:copy-of select="@id|@class|@style"/>
      <xsl:choose>
	<xsl:when test="@size">
	  <xsl:apply-templates select="node()">
	    <xsl:with-param name="size" select="@size"/>
	  </xsl:apply-templates>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:apply-templates select="node()"/>
	</xsl:otherwise>
      </xsl:choose>
    </div>
  </xsl:template>

</xsl:stylesheet>
