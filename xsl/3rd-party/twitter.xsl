<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: twitter.xsl 3213 2012-11-16 21:45:39Z sdalu $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl ="http://www.w3.org/1999/XSL/Transform"
  xmlns:func="http://exslt.org/functions"
  xmlns:m   ="http://www.moxow.org/main"
  extension-element-prefixes="func"
  exclude-result-prefixes="xsl m"
>

<!--

Templates:
- m:tweet

-->

  <!-- ================================================================ -->
  <!-- === Initialisation                                           === -->
  <!-- ================================================================ -->

  <!-- Detection -->
  <xsl:variable  name="m:twitter-init-content" select="$m:detection-nodes"/>

  <func:function name="m:twitter-init-required">
    <xsl:param name="nodes" select="$m:twitter-init-content"/>
    <func:result select="($nodes//m:tweet | $nodes//m:twitter-follow)[1]"/>
  </func:function>
  
  <xsl:template name="m:twitter-init-if-required">
    <xsl:if test="m:twitter-init-required()">
      <xsl:call-template name="m:twitter-init"/>
    </xsl:if>
  </xsl:template>

  <!-- Initialisation -->
  <xsl:template name="m:twitter-init">
    <script type="text/javascript" 
	    src="//platform.twitter.com/widgets.js"/>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- === Tweet                                                    === -->
  <!-- ================================================================ -->

  <xsl:template match="m:tweet">
    <a href="http://twitter.com/share" class="twitter-share-button">
      <xsl:attribute name="data-count">
	<xsl:text>horizontal</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="data-lang">
	<xsl:value-of select="$m:lang"/>
      </xsl:attribute>
      <xsl:if test="@via">
	<xsl:attribute name="data-via">
	  <xsl:value-of select="@via"/>
	</xsl:attribute>
      </xsl:if>
      <xsl:text>Tweet</xsl:text>
    </a>
  </xsl:template>


  <!-- ================================================================ -->
  <!-- === Follow us                                                === -->
  <!-- ================================================================ -->
  <xsl:template match="m:twitter-follow">
    <a href="http://www.twitter.com/{@us}">
      <img src="//twitter-badges.s3.amazonaws.com/twitter-b.png">
	<xsl:choose>
	  <xsl:when test="@src">
	    <xsl:attribute name="src">
	      <xsl:value-of select="@src"/>
	    </xsl:attribute>
	  </xsl:when>
	</xsl:choose>
	<xsl:copy-of select="@alt"/>
      </img>
    </a>
  </xsl:template>

</xsl:stylesheet>
