<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: disqus.xsl 3203 2012-11-08 15:51:09Z sdalu $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:m  ="http://www.moxow.org/main"
  exclude-result-prefixes="xsl m"
>

  <!-- ================================================================ -->
  <!-- === Init                                                     === -->
  <!-- ================================================================ -->

  <xsl:template name="disqus-init">
    <xsl:param name="shortname" />
    <xsl:param name="identifier"/>
    <xsl:param name="url"       />
    <xsl:param name="lang"      />
    <xsl:param name="category"  />

    <script type="text/javascript">
        var disqus_shortname  = '<xsl:value-of select="$shortname" />';
      <xsl:if test="normalize-space($identifier)">
        var disqus_identifier = '<xsl:value-of select="$identifier"/>';
      </xsl:if>
      <xsl:if test="normalize-space($url)">
        var disqus_url        = '<xsl:value-of select="$url"       />';
      </xsl:if>
      <xsl:if test="normalize-space($m:lang)">
	var disqus_config     = function () { 
	     this.language = "<xsl:value-of select="$lang"/>";
	};
      </xsl:if>

	(function() {
	    var dsq = document.createElement('script'); 
	    dsq.type  = 'text/javascript';
	    dsq.async = true;
	    dsq.src   = '//' + disqus_shortname + '.disqus.com/embed.js';
	    (document.getElementsByTagName('head')[0] ||
	     document.getElementsByTagName('body')[0]).appendChild(dsq);
	})();
    </script>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- === Comments renderer                                        === -->
  <!-- ================================================================ -->

  <xsl:template name="m:webpage-comments-renderer">
    <xsl:param name="site-id"   />
    <xsl:param name="lang"      />
    <xsl:param name="identifier"/>

    <div id="disqus_thread"
	 style="margin-top: 1em;
		border-top: 1px solid grey;
		padding-top: 1ex;
		clear: both;
		"/>
    
    <xsl:call-template name="disqus-init">
      <xsl:with-param name="shortname"  select="$site-id"    />
      <xsl:with-param name="lang"       select="$lang"       />
      <xsl:with-param name="identifier" select="$identifier" />
    </xsl:call-template>
  </xsl:template>

</xsl:stylesheet>
