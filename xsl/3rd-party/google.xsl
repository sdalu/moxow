<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: google.xsl 3213 2012-11-16 21:45:39Z sdalu $ -->

<!--

Templates:
- google-adsens

- google-analytics

-->


<xsl:stylesheet version="1.0"
  xmlns:xsl ="http://www.w3.org/1999/XSL/Transform"
  xmlns:func="http://exslt.org/functions"
  xmlns:m   ="http://www.moxow.org/main"
  xmlns:o   ="urn:moxow:3rd-party"
  extension-element-prefixes="func"
  exclude-result-prefixes="xsl m o"
>



  <!-- Adsense                                                      -->
  <!-- ============================================================ -->
  <xsl:template name="o:google-adsense">
    <xsl:param name="key"   />
    <xsl:param name="slot"  />
    <xsl:param name="width" />
    <xsl:param name="height"/>

    <script async="async" 
	    src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js">
    </script>
    <ins class="adsbygoogle">
      <xsl:attribute name="style">
	<xsl:choose>
	  <xsl:when test="$height or $width">
	    <xsl:text>display:inline-block;</xsl:text>
	    <xsl:text>width:</xsl:text><xsl:value-of select='$width'/><xsl:text>px;</xsl:text>
	    <xsl:text>height:</xsl:text><xsl:value-of select='$height'/><xsl:text>px;</xsl:text>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:text>display:block;</xsl:text>
	  </xsl:otherwise>
	</xsl:choose>
      </xsl:attribute>

      <xsl:attribute name="data-ad-client">
	<xsl:value-of select='$key'/>
      </xsl:attribute>
      <xsl:attribute name="data-ad-slot">
	<xsl:value-of select='$slot'/>
      </xsl:attribute>
      <xsl:if test="not(@width or @height)">
      <xsl:attribute name="data-ad-format">
	<xsl:text>auto</xsl:text>
      </xsl:attribute>
      </xsl:if>
    </ins>
    <script>
      (adsbygoogle = window.adsbygoogle || []).push({});
    </script>

    <!--
    <script type="text/javascript">
      google_ad_client = "<xsl:value-of select='$key'/>";
      google_ad_slot   = "<xsl:value-of select='$slot'/>";
      google_ad_width  = <xsl:value-of select='$width'/>;
      google_ad_height = <xsl:value-of select='$height'/>;
    </script>
    <script type="text/javascript"
	    src="http://pagead2.googlesyndication.com/pagead/show_ads.js"/>
    -->
  </xsl:template>



  <!-- Analytics                                                    -->
  <!-- ============================================================ -->
  <xsl:template name="o:google-analytics">
    <xsl:param name="id"/>
    <xsl:param name="domain"/>
    <script type="text/javascript">
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', '<xsl:value-of select="$id"/>']);
      <xsl:if test="$domain">
      _gaq.push(['_setDomainName', '<xsl:value-of select="$domain"/>']);
      </xsl:if>
      _gaq.push(['_trackPageview']);

      (function() {
        var ga   = document.createElement('script');
	ga.type  = 'text/javascript';
	ga.async = true;
	ga.src   = ('https:' == document.location.protocol ? 'https://ssl' 
	                                                   : 'http://www')
		   + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(ga, s);
      })();
    </script>
  </xsl:template>


  <!-- Adwords                                                      -->
  <!-- ============================================================ -->
  <xsl:template match="m:google-adwords-conversion">
    <xsl:choose>
      <xsl:when test="@type='webpage'">
	<script type="text/javascript">
	  var google_conversion_id = <xsl:value-of select="@id"/>;
	  var google_conversion_language = "en";
	  var google_conversion_format = "3";
	  var google_conversion_color = "ffffff";
	  var google_conversion_label = "<xsl:value-of select="@label"/>";
	  var google_conversion_value = 0;
	  var google_remarketing_only = false;
	</script>
	<script type="text/javascript" 
		src="//www.googleadservices.com/pagead/conversion.js"/>
	<noscript>
	  <div style="display:inline;">
	    <img height="1" width="1" style="border-style:none;" alt="" 
		 src="//www.googleadservices.com/pagead/conversion/{@id}/?value=0&amp;label={@label}&amp;guid=ON&amp;script=0"/>
	  </div>
	</noscript>
      </xsl:when>
      <xsl:when test="@type = 'callback'">
	<script type="text/javascript">
	  goog_snippet_vars = function() {
	    var w = window;
	    w.google_conversion_id = <xsl:value-of select="@id"/>;
	    w.google_conversion_label = "<xsl:value-of select="@label"/>";
	    w.google_conversion_value = 0;
	    w.google_remarketing_only = false;
	  }
	  // DO NOT CHANGE THE CODE BELOW.
	  goog_report_conversion = function(url) {
	    goog_snippet_vars();
	    window.google_conversion_format = "3";
	    window.google_is_call = true;
	    var opt = new Object();
	    opt.onload_callback = function() {
	      if (typeof(url) != 'undefined') {
	        window.location = url;
	      }
	    }
	    var conv_handler = window['google_trackConversion'];
	    if (typeof(conv_handler) == 'function') {
	      conv_handler(opt);
	    }
	  }
	</script>
	<script type="text/javascript"
		src="//www.googleadservices.com/pagead/conversion_async.js"/>
      </xsl:when>
    </xsl:choose>
</xsl:template>


  <!-- Custom Search Engine                                         -->
  <!-- ============================================================ -->

  <!-- https://cse.google.com/cse/ -->

  <xsl:template match="m:google-cse">
    <xsl:variable name="id">
      <xsl:choose>
        <xsl:when test="@id"><xsl:value-of select="@id"/></xsl:when>
	<xsl:otherwise><xsl:value-of select="generate-id()"/></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <script>
      (function() {
        var cx = '<xsl:value-of select="@cx"/>';
	var gcse = document.createElement('script');
	gcse.type = 'text/javascript';
	gcse.async = true;
	gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(gcse, s);
	})();
    </script>

    <div id="{$id}">
      <xsl:copy-of select="@class|@style"/>
      <xsl:apply-templates select="node()"/>

      <gcse:search xmlns:gcse="-">
      </gcse:search>
    </div>


  </xsl:template>
</xsl:stylesheet>
