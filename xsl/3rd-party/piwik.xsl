<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:o  ="urn:moxow:3rd-party"
  xmlns:f  ="http://www.moxow.org/function"
  exclude-result-prefixes="xsl o f"
>

  <xsl:variable name="o:piwik-default-href" select="'-undefined-'"/>


  <!-- Tracking                                                     -->
  <!-- ============================================================ -->

  <xsl:template name="o:piwik-tracker">
    <xsl:param name="id"/>
    <xsl:param name="href" select="$o:piwik-default-href"/>
    <xsl:param name="domain" />

    <!-- As template is called with all its parameter defined,
	 the default value is not applied on empty, so build
	 a new $href as variable -->
    <xsl:variable name="href"
		  select="f:if-absent($href, $o:piwik-default-href)"/>

    <!-- Sanity check -->
    <xsl:if test="not(normalize-space($id))">
      <xsl:message terminate="yes"> 
	$id is required by piwik tracker
      </xsl:message>
    </xsl:if>
    
    <!-- Use default href if none provided -->
    <xsl:if test="$href = '-undefined-'">
      <xsl:message terminate="yes">
	you should specify an $href or define $piwik-default-href
      </xsl:message>
    </xsl:if>
    
    <!-- Ensure that $href is '/' terminated -->
    <xsl:variable name="href-normalized">
      <xsl:value-of select="$href"/>
      <xsl:if test="substring($href, string-length($href)) != '/'">
	<xsl:text>/</xsl:text>
      </xsl:if>
    </xsl:variable>

    <!-- Full tracking with javascript -->
    <script type="text/javascript">
      var _paq = _paq || [];
      _paq.push(["trackPageView"]);
      _paq.push(["enableLinkTracking"]);
      <xsl:if test="$domain">
      _paq.push(["setCookieDomain", "*.<xsl:value-of select="$domain"/>" ]);
      _paq.push(["setDomains",     ["*.<xsl:value-of select="$domain"/>"]]);
      </xsl:if>

      (function() {
        var u = "<xsl:value-of select="$href-normalized"/>";
        _paq.push(["setTrackerUrl", u+"piwik.php"]);
        _paq.push(["setSiteId",     "<xsl:value-of select="$id"/>"]);
        var d=document, g=d.createElement("script");
        g.type="text/javascript"; g.defer=true; g.async=true; 
        g.src=u+"piwik.js"; 
        var s=d.getElementsByTagName("script")[0]; 
        s.parentNode.insertBefore(g,s);
      })();
    </script>

    <!-- Handle noscript -->
    <noscript>
      <p><img style="border:0" alt="">
	  <xsl:attribute name="src">
	    <xsl:value-of select="$href-normalized"/>
	    <xsl:text>piwik.php</xsl:text>
	    <xsl:text>?idsite=</xsl:text>
	    <xsl:value-of select="$id"/>
	  </xsl:attribute>
      </img></p>
    </noscript>
  </xsl:template>

</xsl:stylesheet>
