<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: addthis.xsl 3274 2013-04-06 15:26:09Z sdalu $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl  ="http://www.w3.org/1999/XSL/Transform"
  xmlns:str  ="http://exslt.org/strings"
  xmlns:func ="http://exslt.org/functions"
  xmlns:m    ="http://www.moxow.org/main"
  extension-element-prefixes="str func"
  exclude-result-prefixes="xsl m"
>


  <!-- ================================================================ -->
  <!-- === Defaults                                                 === -->
  <!-- ================================================================ -->

  <xsl:variable name="m:addthis-userid" select="'sdalu'"/>
  <xsl:variable name="m:addthis-flattr-url" select="$m:website/@href"/>
  <xsl:variable name="m:addthis-flattr-userid" select="'sdalu'"/>



  <!-- ================================================================ -->
  <!-- === Initialisation                                           === -->
  <!-- ================================================================ -->

  <!-- Detection -->
  <xsl:variable  name="m:addthis-init-content" select="$m:content"/>
  <func:function name="m:addthis-init-required">
    <xsl:param name="nodes" select="$m:addthis-init-content"/>
    <func:result select="$nodes//m:addthis-toolbar   [1] |
			 $nodes//m:addthis-welcomebar[1] |
			 $nodes//m:addthis-bookmark  [1]"/>
  </func:function>

  <!-- Init -->
  <xsl:template name="m:addthis-init">
    <xsl:variable name="flattr-submit-url">
      <xsl:text>http://flattr.com/submit/auto</xsl:text>
      <xsl:text>?</xsl:text>

      <xsl:text>url=</xsl:text>
      <xsl:choose>
	<xsl:when test="$m:addthis-flattr-url">
	  <xsl:value-of
	      select="str:encode-uri($m:addthis-flattr-url, true())"/>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:text>{{url}}</xsl:text>
	</xsl:otherwise>
      </xsl:choose>

      <xsl:if test="$m:addthis-flattr-userid">
	<xsl:text>&#38;</xsl:text>
	<xsl:text>title={{title}}</xsl:text>

	<xsl:text>&#38;</xsl:text>
	<xsl:text>user_id=</xsl:text>
	<xsl:value-of select="$m:addthis-flattr-userid"/>
      </xsl:if>
    </xsl:variable>

    <script type="text/javascript">
        var addthis_config = {
            ui_header_color     : '#000000',
            ui_header_background: '#dddddd',
	    ui_language         : (window.document || document)
	                             .documentElement.lang || '',
	    ui_use_addressbook  : true,
	    services_custom     : [ {
	        name: "Flattr",
                url: "<xsl:value-of select="$flattr-submit-url"/>",
                icon: "//flattr.com/_img/icons/flattr_logo_16.png" 
	      } ]
	}; 
    </script>
    <script type="text/javascript" 
	    src="//s7.addthis.com/js/300/addthis_widget.js#pubid={$m:addthis-userid}"/>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- === Toolbar                                                  === -->
  <!-- ================================================================ -->

  <xsl:template match="m:addthis-toolbar">
    <div id="addthis" class="addthis_toolbox addthis_default_style">
      <a class="addthis_button_google_plusone_share"/>
      <a class="addthis_button_facebook"/>
      <a class="addthis_button_twitter"/>
      <a class="addthis_button_pinterest_share"/>
      <a class="addthis_button_stumbleupon"/>
      <a class="addthis_button_preferred_1"/>
      <a class="addthis_button_preferred_2"/>
      <a class="addthis_button_compact"/>
      <a class="addthis_counter addthis_bubble_style"/>
    </div>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- === Toolbox                                                  === -->
  <!-- ================================================================ -->

  <xsl:template match="m:addthis-toolbox">
    <div class="addthis_toolbox">
      <xsl:copy-of select="@id|@style"/>
      <a class="addthis_button_compact">
	<xsl:choose>
	  <xsl:when test="@img">
	    <img alt="♥" src="{@img}"/>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:apply-templates/>
	  </xsl:otherwise>
	</xsl:choose>
      </a>
    </div>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- === Smart Layer                                              === -->
  <!-- ================================================================ -->

  <xsl:template match="m:addthis-smart-layer">
    <script type="text/javascript">
      addthis.layers({
        'theme' : 'transparent',
        'share' : {
          'position' : 'right',
          'numPreferredServices' : 5
        },  
       'whatsnext' : {},  
       'recommended' : {} 
      });
    </script>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- === Welcomebar                                               === -->
  <!-- ================================================================ -->

  <xsl:template match="m:addthis-welcomebar">
    <script type='text/javascript'>
      addthis.bar.initialize({
        'default' : {
  	  backgroundColor: '#000000',
	  buttonColor: '#098DF4',
	  textColor: '#FFFFFF',
	  buttonTextColor: '#FFFFFF'
        },

	'rules' : [ {
	    name   : 'AnyOther',
	    message: 'If you enjoy this page, do us a favor:',
	    action : {
              type   : 'button',
	      text   : 'Click Here',
	      verb   : 'share',
	      service: 'preferred' }
	  }, {
	    name   : 'Twitter',
	    match  : { referringService: 'twitter' },
	    message: 'If you find this page helpful:',
	    action : {
              type   : 'button',
              text   : 'Tweet it!',
              verb   : 'share',
              service: 'twitter' }
	  }, {
	    name   : 'Facebook',
	    match  : { referringService: 'facebook' },
	    message: 'Tell your friends about us:',
	    action : {
              type   : 'button',
              text   : 'Share on Facebook',
              verb   : 'share',
              service: 'facebook' }
          }, {
	    name   : 'Google',
	    match  : { referrer: 'google.com' },
	    message: 'If you like this page, let Google know:',
	    action : {
              type   : 'button',
	      text   : '+1',
	      verb   : 'share',
	      service: 'google_plusone_share' }
	  } ]
	});
    </script>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- === Bookmark                                                 === -->
  <!-- ================================================================ -->

  <xsl:template match="m:addthis-bookmark">
    <a href="http://www.addthis.com/bookmark.php" class="addthis_button">
      <xsl:apply-templates/>
    </a>
  </xsl:template>

</xsl:stylesheet>

