<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: twitter.xsl 2379 2011-08-01 17:40:12Z sdalu $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl ="http://www.w3.org/1999/XSL/Transform"
  xmlns:func="http://exslt.org/functions"
  xmlns:m   ="http://www.moxow.org/main"
  xmlns:fb  ="http://www.facebook.com/2008/fbml"
  extension-element-prefixes="func"
  exclude-result-prefixes="xsl m fb"
>

<!--
    https://developers.facebook.com/docs/guides/web/
-->


  <!-- ================================================================ -->
  <!-- === Defaults                                                 === -->
  <!-- ================================================================ -->
  
  <xsl:variable name="m:facebook-app-id"/>



  <!-- ================================================================ -->
  <!-- === Initialisation                                           === -->
  <!-- ================================================================ -->

  <!-- Detection -->
  <xsl:variable  name="m:facebook-init-content" select="$m:detection-nodes"/>

  <func:function name="m:facebook-init-required-default">
    <xsl:param name="nodes"/>
    <func:result select="$nodes//fb:*[1]"/>
  </func:function>
  <func:function name="m:facebook-init-required">
    <xsl:param name="nodes" select="$m:facebook-init-content"/>
    <func:result select="m:facebook-init-required-default($nodes)"/>
  </func:function>
  
  <xsl:template name="m:facebook-init-if-required">
    <xsl:if test="m:facebook-init-required()">
      <xsl:call-template name="m:facebook-init"/>
    </xsl:if>
  </xsl:template>

  <!-- Initialisation -->
  <xsl:template name="m:facebook-init">
    <xsl:param name="app-id" select="$m:facebook-app-id"/>

    <div id="fb-root"/>
    <script type="text/javascript">
      window.fbAsyncInit = function() {
        FB.init({
	  <xsl:if test="$app-id">
	    appId: '<xsl:value-of select="$app-id"/>', 
	  </xsl:if>
            status: true, cookie: true, xfbml: true
	});
      };

      (function(d){
	var locale = '<xsl:value-of select="$m:locale"/>';
        var id     = 'facebook-jssdk';
        if (d.getElementById(id)) {return;}
        var js = d.createElement('script'); js.id = id; js.async = true;
        js.src = '//connect.facebook.net/' + locale + '/all.js';
	var ref = d.getElementsByTagName('script')[0];
        ref.parentNode.insertBefore(js, ref);
      }(document));
    </script>
  </xsl:template>

</xsl:stylesheet>
