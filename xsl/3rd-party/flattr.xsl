<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: flattr.xsl 3213 2012-11-16 21:45:39Z sdalu $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl ="http://www.w3.org/1999/XSL/Transform"
  xmlns:func="http://exslt.org/functions"
  xmlns:m   ="http://www.moxow.org/main"
  extension-element-prefixes="func"
  exclude-result-prefixes="xsl m"
>

<!--

Templates:
- m:flattr

-->


  <!-- ================================================================ -->
  <!-- === Initialisation                                           === -->
  <!-- ================================================================ -->

  <!-- Detection -->
  <xsl:variable  name="m:flattr-init-content" select="$m:detection-nodes"/>

  <func:function name="m:flattr-init-required">
    <xsl:param name="nodes" select="$m:flattr-init-content"/>
    <func:result select="$nodes//m:flattr[1]"/>
  </func:function>
  
  <xsl:template name="m:flattr-init-if-required">
    <xsl:if test="m:flattr-init-required()">
      <xsl:call-template name="m:flattr-init"/>
    </xsl:if>
  </xsl:template>

  <!-- Initialisation -->
  <xsl:template name="m:flattr-init">
    <script type="text/javascript">
	  (function() {
	    var s = document.createElement('script');
	    var t = document.getElementsByTagName('script')[0];
	    s.type  = 'text/javascript';
	    s.async = true;
	    s.src   = '//api.flattr.com/js/0.6/load.js?mode=auto';
	    t.parentNode.insertBefore(s, t);
          })();
    </script>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- === Flattr                                                   === -->
  <!-- ================================================================ -->

  <xsl:template match="m:flattr" name="m:flattr">
    <xsl:choose>
      <xsl:when test="starts-with(@thing, 'http')">
	<a class="FlattrButton" style="display:none;"
	   href="{@thing}">
	  <xsl:if test="@type = 'compact'">
	    <xsl:attribute name="data-flattr-button">
	      <xsl:text>compact</xsl:text>
	    </xsl:attribute>
	  </xsl:if>
	</a>
      </xsl:when>
      <xsl:otherwise>
	<a href="http://flattr.com/thing/{@thing}" target="_blank">
	  <img src="//api.flattr.com/button/flattr-badge-large.png"
	       alt="Flattr" title="Flattr" border="0" /></a>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
