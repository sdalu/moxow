<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: desktop.xsl 3292 2013-04-08 21:24:18Z sdalu $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:m  ="http://www.moxow.org/main"
  exclude-result-prefixes="xsl m"
>

  <xsl:template name="m:layout-content-content">
    <xsl:call-template name="m:webpage-main-pre"    />
    <xsl:call-template name="m:webpage-custom-box"  />
    <xsl:call-template name="m:webpage-error"       />
    <xsl:call-template name="m:webpage-title"       />
    <xsl:call-template name="m:webpage-content"     />
    <xsl:call-template name="m:webpage-comments"    />
    <xsl:call-template name="m:webpage-main-post"   />
  </xsl:template>



  <!-- ============================================================ -->
  <!-- Navigation                                                   -->
  <!-- ============================================================ -->

  <xsl:template name="m:layout-header-content">
    <xsl:call-template name="m:webpage-logo"           />
    <xsl:call-template name="m:webpage-menu"           />
    <xsl:call-template name="m:webpage-breadcrumbs"    />
    <xsl:call-template name="m:webpage-lang-switcher"  />
    <xsl:call-template name="m:layout-header-defaults" />
    <div style="clear: both;"/>
  </xsl:template>



  <!-- ============================================================ -->
  <!-- Footer                                                       -->
  <!-- ============================================================ -->

  <xsl:template name="m:layout-footer-content">
      <xsl:param name="data"
		 select=" m:footer[@place='before' or not(@place)]      |
			 (m:footer[@place='replace'] |
			  m:lang($m:defaults/m:footer))[1] |
			  m:footer[@place='after'                ]"/>
      <!-- Data -->
      <xsl:if test="$data">
	<div id="m-footer-data">
	  <xsl:apply-templates select="$data"/>
	</div>
      </xsl:if>

      <!-- Page information -->
      <xsl:call-template name="m:webpage-pageinfo"/>
      <xsl:call-template name="m:webpage-otherinfo"/>
      
      <div style="clear: both;"/>
  </xsl:template>

</xsl:stylesheet>

