<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: desktop-modern.xsl 3292 2013-04-08 21:24:18Z sdalu $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:m   ="http://www.moxow.org/main"
  exclude-result-prefixes="xsl m"
>

  <!-- == Import stylesheet ======================================= -->

  <xsl:import href="desktop.xsl"/>

  
  <xsl:template name="m:layout-content-content">
    <xsl:call-template name="m:webpage-main-pre"      />
    <xsl:call-template name="m:webpage-custom-box"    />
    <xsl:call-template name="m:webpage-error"         />
    <xsl:call-template name="m:webpage-content"       />
    <xsl:call-template name="m:webpage-comments"      />
    <xsl:call-template name="m:webpage-main-post"     />
  </xsl:template>



  <xsl:template name="m:layout-header-content">
    <xsl:param name="website"  />
    <xsl:call-template name="m:webpage-logo" />
    <xsl:call-template name="m:webpage-menu" />
    <xsl:call-template name="m:webpage-title"/>
    <div id="mxw-breadcrumbs-n-lang">
      <xsl:call-template name="m:webpage-breadcrumbs"/>
      <xsl:call-template name="m:webpage-lang-switcher"/>
    </div>
     <xsl:call-template name="m:layout-header-defaults"/>
     <div style="clear: both;"/>
  </xsl:template>

</xsl:stylesheet>
