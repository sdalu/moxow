<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: exhibition.xsl 3231 2012-11-27 23:33:57Z sdalu $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:m  ="http://www.moxow.org/main"
  exclude-result-prefixes="xsl m"
>


  <!-- ================================================================ -->
  <!-- === Exhibition                                               === -->
  <!-- ================================================================ -->

  <!-- Quick buttons -->
  <xsl:template match="m:quick-button">
    <nav id="m-quick-buttons">
      <xsl:for-each select="m:button">
	<a role="button">
          <xsl:copy-of select="@id|@style|@href|@rel|@class"/>
	  <xsl:if test="@type = 'add-this'">
	    <xsl:attribute name="href">
	      <xsl:text>http://www.addthis.com/bookmark.php?v=25</xsl:text>
	    </xsl:attribute>
	    <xsl:attribute name="onmouseover">
	      <xsl:text>return addthis_open(this, '', '[URL]', '[TITLE]')</xsl:text>
	    </xsl:attribute>
	    <xsl:attribute name="onmouseout">
	      <xsl:text>addthis_close()</xsl:text>
	    </xsl:attribute>
	    <xsl:attribute name="onclick">
	      <xsl:text>return addthis_sendto()</xsl:text>
	    </xsl:attribute>
	  </xsl:if>
	  <img><xsl:copy-of select="@src|@alt|@title"/></img>
	</a>
	<xsl:if test="@type = 'add-this'">
	  <script type="text/javascript">
	    var addthis_config = {
	      ui_header_color     : '#000000',
	      ui_header_background: '#dddddd',
	      username            : "<xsl:value-of select='@key'/>",
	    <xsl:if test="$lang">
	      ui_language         : "<xsl:value-of select='$lang'/>", 
	    </xsl:if>
	      ui_use_addressbook  : true
	    };
	  </script>
	  <script type="text/javascript"
		  src="http://s7.addthis.com/js/250/addthis_widget.js"/>
	</xsl:if>
      </xsl:for-each>
    </nav>
  </xsl:template>
 

  <!-- Poster -->
  <xsl:template match="m:poster">
    <div class="m-exhibition-poster {@class}">
      <xsl:copy-of select="@id|@style"/>

      <!-- Image with map -->
      <map name="m-exhibition-poster-map" id="m-exhibition-poster-map">
	<xsl:for-each select="m:area">
  	  <area>
	    <xsl:copy-of select="@href|@shape|@coords"/>
	    <xsl:attribute name="alt">
	      <xsl:value-of select="@name"/></xsl:attribute>
	    <xsl:attribute name="title">
	      <xsl:value-of select="@name"/></xsl:attribute>
	  </area>
	</xsl:for-each>
      </map>

      <xsl:if test="@enter">
	<a class="m-exhibition-enter"> 
	  <xsl:copy-of select="@href"/>
	  <xsl:value-of select="@enter"/>
	</a>
      </xsl:if>
      
      <a href="{@href}">
	<img class="m-exhibition-poster-img"
	     usemap="#m-exhibition-poster-map" src="{@src}">
	  <xsl:attribute name="alt">
	    <xsl:choose>
	      <xsl:when test="$lang = 'fr'">Affiche</xsl:when>
	      <xsl:otherwise>Poster</xsl:otherwise>
	    </xsl:choose>
	    <xsl:text>: </xsl:text>
	    <xsl:value-of select="@name"/>
	  </xsl:attribute>
	</img>
      </a>
      
      <!-- Access: where / when -->
      <xsl:for-each select="../m:access[@id=current()/@access]">
	<a href="{m:where/@href}"
	   class="m-exhibition-poster-where">
	  <xsl:for-each select="m:where/m:addr">
	    <xsl:choose>
	      <xsl:when test="@title">
		<b><xsl:value-of select="@title"/></b>
	      </xsl:when>
	      <xsl:when test="@subtitle">
		<b style="font-size: smaller;"><i>
		    <xsl:text>  </xsl:text>
		    <xsl:value-of select="@subtitle"/></i></b>
	      </xsl:when>
	      <xsl:when test="@line">
		<xsl:value-of select="@line"/>
	      </xsl:when>
	    </xsl:choose>
	    <xsl:if test="position() != last()">
	      <br/>
	    </xsl:if>
	  </xsl:for-each>
	</a>
	
	<table class="m-exhibition-poster-when"
	       unselectable="on" onselectstart="return false">
	  <xsl:if test="m:conditions/@title">
  	    <tr><th colspan="4">
		<xsl:value-of select="m:conditions/@title"/>
	    </th></tr>
	  </xsl:if>
	  <xsl:if test="m:when/@period">
	    <tr><td colspan="4">
		<xsl:value-of select="m:when/@period"/>
	    </td></tr>
	  </xsl:if>
	  <xsl:for-each select="m:when/m:timetable">
	    <tr>
	      <td> </td>
	      <td><xsl:value-of select="@when"/></td>
	      <td>:</td>
	      <td><xsl:value-of select="@schedule"/></td>
	    </tr>
	  </xsl:for-each>
	</table>
      </xsl:for-each>
      <xsl:apply-templates select="node()[not(self::m:area)]"/>
    </div>
  </xsl:template>


  <!-- Access -->
  <xsl:template match="m:access">
    <table class="m-exhibition-access {@class}">
      <xsl:copy-of select="@id|@style"/>
      <tr>
	<td class="m-exhibition-access-where">
	  <xsl:variable name="href" select="m:where/@href"/>
	  <table>
	    <xsl:for-each select="m:where/m:addr">
	      <tr><td>
		  <xsl:choose>
		    <xsl:when test="@title">
		      <a href="{$href}">
			<b><xsl:value-of select="@title"/></b></a>
		    </xsl:when>
		    <xsl:when test="@subtitle">
		      <b><i><xsl:text>  </xsl:text>
			    <xsl:value-of select="@subtitle"/></i></b>
		    </xsl:when>
		    <xsl:when test="@line">
		      <xsl:value-of select="@line"/>
		    </xsl:when>
		  </xsl:choose>
	      </td></tr>
	    </xsl:for-each>
	  </table>
	</td>
	<td class="m-exhibition-access-when">
	  <table>
	    <xsl:if test="m:when/@period">
	      <tr><td colspan="4">
		  <xsl:value-of select="m:when/@period"/>
	      </td></tr>
	    </xsl:if>
	    <xsl:for-each select="m:when/m:timetable">
	      <tr>
		<td> </td>
		<td><xsl:value-of select="@when"/></td>
		<td>:</td>
		<td><xsl:value-of select="@schedule"/></td>
	      </tr>
	    </xsl:for-each>
	  </table>
      </td></tr>
    </table>
  </xsl:template>


  <xsl:template match="m:exhibition">
    <table class="m-exhibition" style="text-align: center;">
      <tr><td class="m-exhibition-holder-poster">
	  <xsl:apply-templates select="m:poster"/>
	</td><td class="m-exhibition-holder-description">
	  <div class="m-exhibition-description">
	    <xsl:for-each select="m:section">
	      <div class="m-exhibition-section {@class}">
		<xsl:copy-of select="@id|@style"/>
		<xsl:for-each select="m:info">
		  <div class="m-exhibition-section-info {@class}">
		    <xsl:copy-of select="@id|@style"/>
		    <xsl:apply-templates select="node()"/>
		  </div>
		</xsl:for-each>
		<xsl:if test="@title">
		  <h1><xsl:value-of select="@title"/></h1>
		</xsl:if>
		<xsl:apply-templates select="node()[not(self::m:info)]"/>
	      </div>
	    </xsl:for-each>
	  </div>
    </td></tr></table>
  </xsl:template>




  <xsl:template match="m:exhibition-message">
    <table align="center" 
	   style="margin: 0 auto; margin-top: 2em; border-spacing: 3em;
		  {@style}">
      <xsl:copy-of select="@class|@id"/>
      <tr>
	<td style="vertical-align: middle; text-align: center;">
	  <a href="/"><img style="width: 250px; border: 2px solid white;" src="{@poster}"/></a>
	</td>
	<td style="width: 300px; vertical-align: top;">
	  <xsl:for-each select="m:section">
	    <div class="m-exhibition-section {@class}">
	      <xsl:copy-of select="@id|@style"/>
	      <xsl:for-each select="m:info">
		<div class="m-exhibition-section-info {@class}">
		  <xsl:copy-of select="@id|@style"/>
		  <xsl:apply-templates select="node()"/>
		</div>
	      </xsl:for-each>
	      <xsl:if test="@title">
		<h1><xsl:value-of select="@title"/></h1>
	      </xsl:if>
	      <xsl:apply-templates select="node()[not(self::m:info)]"/>
	    </div>
	  </xsl:for-each>
	</td>
    </tr></table>
  </xsl:template>


  <xsl:template match="m:exhibition-render">
    <xsl:variable name="render"     select="."/>
    <xsl:variable name="exhibition" select="ancestor::m:exhibition[1]"/>

    <xsl:choose>
      <!-- Access -->
      <xsl:when test="@type = 'access'">
	<xsl:if test="$exhibition/m:access[@on]">
	  <hr/>
	  <div class="m-exhibition-date-current">
	    <xsl:apply-templates select="$exhibition/m:access[@on]"/>
	    <xsl:if test="not($exhibition[@display]) or
			  ($exhibition[@display != 'current'] and 
			   $exhibition/m:access[not(@on)])">
	      <div class="m-exhibition-date-toggler" role="button">
		<xsl:attribute name="title">
		  <xsl:choose>
		    <xsl:when test="$lang = 'fr'">
		      <xsl:text>Toutes les dates</xsl:text>
		    </xsl:when>
		    <xsl:otherwise>
		      <xsl:text>All dates</xsl:text>
		    </xsl:otherwise>
		  </xsl:choose>
		</xsl:attribute>
		<span>+/-</span>
	      </div>
	    </xsl:if>
	  </div>
	</xsl:if>
	<xsl:if test="not($exhibition[@display]) or
		      ($exhibition[@display != 'current'] and 
		       $exhibition/m:access[not(@on)])">
	  <div class="m-exhibition-date-previous m-exhibition-off">
	    <hr/>
	    <xsl:apply-templates select="$exhibition/m:access[not(@on)]"/>
	  </div>
	</xsl:if>
      </xsl:when>

      <!-- Poster download -->
      <xsl:when test="@type = 'poster-download'">
	<xsl:variable name="poster" select="$exhibition/m:poster"/>
	<xsl:for-each select="$exhibition/m:access[@id=$poster/@access]">
	  <div class="m-exhibition-download-poster m-exhibition-infobox">
	    <xsl:copy-of select="$render/@id"/>
	    <xsl:if test="m:poster">
	      <div class="m-exhibition-infobox-trigger">
		<a href="{m:poster[1]/@href}">
		  <xsl:value-of select="$render/@title"/>
		</a>
	      </div>
	    </xsl:if>
	    
	    <table class="m-exhibition-infobox-popup">
	      <tr><td rowspan="2">
		  <img src="{$poster/@thumb}" alt=""
		       style="border: 1px solid black;"/></td>
		<td colspan="2" style="text-align: center;">
		  <xsl:value-of select="$render/@description"/>
		</td>
	      </tr>
	      <tr class="m-exhibition-format-selector">
		<xsl:for-each select="m:poster">
		  <td><a href="{@href}"><img>
		  <xsl:variable name="ext" 
			  select="substring(@href,string-length(@href)-2)"/>
		  <xsl:choose>
		    <xsl:when test="$ext = 'pdf'">
		      <xsl:attribute name="src">
			<xsl:text>/images/common/icons/64x64/PDF.png</xsl:text>
		      </xsl:attribute>
		      <xsl:attribute name="alt">
			<xsl:text>PDF</xsl:text>
		      </xsl:attribute>
		    </xsl:when>
		    <xsl:when test="$ext = 'jpg'">
		      <xsl:attribute name="src">
			<xsl:text>/images/common/icons/64x64/JPG.png</xsl:text>
		      </xsl:attribute>
		      <xsl:attribute name="alt">
			<xsl:text>JPEG</xsl:text>
		      </xsl:attribute>
		    </xsl:when>
		  </xsl:choose>
		  </img></a></td>	
		</xsl:for-each>
	      </tr>
	    </table>
	  </div>
	</xsl:for-each>
      </xsl:when>
    </xsl:choose>
  </xsl:template>


</xsl:stylesheet>

