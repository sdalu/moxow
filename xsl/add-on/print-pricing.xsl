<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: print-pricing.xsl 3186 2012-11-01 19:41:47Z sdalu $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:m="http://www.moxow.org/main"
  xmlns:l10n="http://www.moxow.org/l10n"
  xmlns:pp="http://www.moxow.org/add-on/print-pricing"
  exclude-result-prefixes="xsl m l10n pp"
>

  <xsl:import href="l10n/en/print-pricing.xsl"/>

  <!-- Subscribe form -->
  <xsl:template match="m:print-pricing-selector">
    <form id="print-pricing-selector"
	  style="margin-left: 1em;">
      <table style="border: 1px solid grey;">
	<tr>
	  <th>
	    <xsl:call-template name="l10n:print-pricing--selector--print"/>
	  </th>
	  <td>
	<select name="media">
	  <option value="photo">
	    <xsl:call-template name="l10n:print-pricing--photo"/>
	  </option>
	  <option value="fine-art">
	    <xsl:call-template name="l10n:print-pricing--fine-art"/>
	  </option>
	  <option value="dibond">
	    <xsl:call-template name="l10n:print-pricing--dibond"/>
	  </option>
	  <option value="canvas">
	    <xsl:call-template name="l10n:print-pricing--canvas"/>
	  </option>
	  <option value="acrylic-glass">
	    <xsl:call-template name="l10n:print-pricing--acrylic"/>
	  </option>
	</select>
	<xsl:call-template name="l10n:print-pricing--selector--by"/>
	<select name="format">
	  <optgroup label="Small">
	    <option value="s1">10x15 / 11x15</option>
	    <option value="s2">15x23 / 15x20</option>
	  </optgroup>
	  <optgroup label="Medium">
	    <option value="m1">20x30 / 20x27</option>
	    <option value="m2">30x45 / 30x40</option>
	    <option value="m3">40x60 / 38x50</option>
	  </optgroup>
	  <optgroup label="Large">
	    <option value="l1">50x75 / 50x67</option>
	    <option value="l2">60x90 / 60x80</option>
	  </optgroup>
	</select>
	  </td>
	  <td rowspan="2" style="font-size: x-large; padding: 0 1ex;">
	    <xsl:call-template name="l10n:print-pricing--selector--for"/>
	  </td>
	  <td rowspan="2" style="font-size: xx-large;">
	    <span class="print-pricing-fullcost"/> €
	  </td>
	</tr>

	<tr>
	  <th>
	    <xsl:call-template name="l10n:print-pricing--selector--option"/>
	  </th>
	  <td>
	    <label>
	      <input type="checkbox" name="artist-print"/>
	      <xsl:call-template name="l10n:print-pricing--artist-print"/>
	    </label>
	    <br/>
	    <label>
	      <input type="checkbox" name="certificat"/>
	      <xsl:call-template name="l10n:print-pricing--certificat"/>
	    </label>
	  </td>
	</tr>
      </table>

    </form>
  </xsl:template>



  <xsl:template match="m:print-pricing-explained">
    <div>
      <xsl:copy-of select="@style|@class"/>
      <xsl:call-template name="l10n:print-pricing-explained"/>
    </div>
  </xsl:template>

  <xsl:template match="m:print-pricing-print">
    <table id="print-pricing-print">
      <xsl:copy-of select="@style|@class"/>

      <caption>
	<xsl:call-template name="l10n:print-pricing--print--title"/>
      </caption>

      <colgroup>
	<col style="width: 8ex;"/>
	<col style="width: 5ex;"/>
      </colgroup>
      <colgroup class="price-small">
	<col class="price-small-1"/>
	<col class="price-small-2"/>
      </colgroup>
      <colgroup class="price-medium">
	<col class="price-medium-1"/>
	<col class="price-medium-2"/>
	<col class="price-medium-3"/>
      </colgroup>
      <colgroup class="price-large">
	<col class="price-large-1"/>
	<col class="price-large-2"/>
      </colgroup>
      
      <thead>
	<tr class="print-pricing-format-group">
	  <th colspan="2" rowspan="2" scope="row">
	    <xsl:call-template name="l10n:print-pricing--size"/>
	  </th>
	  <td colspan="2" scope="colgroup" class="price-small"
	      data-pricing-group="small">
	    <xsl:call-template name="l10n:print-pricing--small"/>
	  </td>
	  <td colspan="3" scope="colgroup" class="price-medium"
	      data-pricing-group="medium">
	    <xsl:call-template name="l10n:print-pricing--medium"/>
	  </td>
	  <td colspan="2" scope="colgroup" class="price-large"
	      data-pricing-group="large">
	    <xsl:call-template name="l10n:print-pricing--large"/>
	  </td>
	</tr>
	
	<tr class="print-pricing-format-nickname">
	  <td scope="col" 
	      data-pricing-nickname="s1"
	      data-pricing-group="small">S1</td>
	  <td scope="col" 
	      data-pricing-nickname="s2"
	      data-pricing-group="small">S2</td>
	  <td scope="col"
	      data-pricing-nickname="m1"
	      data-pricing-group="medium">M1</td>
	  <td scope="col"
	      data-pricing-nickname="m2"
	      data-pricing-group="medium">M2</td>
	  <td scope="col"
	      data-pricing-nickname="m3"
	      data-pricing-group="medium">M3</td>
	  <td scope="col" 
	      data-pricing-nickname="l1"
	      data-pricing-group="large">L1</td>
	  <td scope="col" 
	      data-pricing-nickname="l2"
	      data-pricing-group="large">L2</td>
	</tr>
	
	<tr class="print-pricing-format-ratio" 
	    data-pricing-ratio="2:3">
	  <th rowspan="2" scope="rowgroup">
	    <xsl:call-template name="l10n:print-pricing--ratio"/>
	  </th>
	  <th scope="row">2/3</th>
	  <td data-pricing-size="10x15">10x15</td>
	  <td data-pricing-size="15x23">15x23</td>
	  <td data-pricing-size="20x30">20x30</td>
	  <td data-pricing-size="30x45">30x45</td>
	  <td data-pricing-size="40x60">40x60</td>
	  <td data-pricing-size="50x75">50x75</td>
	  <td data-pricing-size="60x90">60x90</td>
	</tr>
	
	<tr class="print-pricing-format-ratio" 
	    data-pricing-ratio="4:3">
	  <th scope="row">4/3</th>
	  <td data-pricing-size="11x15">11x15</td>
	  <td data-pricing-size="15x20">15x20</td>
	  <td data-pricing-size="20x27">20x27</td>
	  <td data-pricing-size="30x40">30x40</td>
	  <td data-pricing-size="38x50">38x50</td>
	  <td data-pricing-size="50x67">50x67</td>
	  <td data-pricing-size="60x68">60x80</td>
	</tr>
      </thead>
      
      <tbody>
	<tr class="print-pricing-cost" 
	    data-pricing-media="photo">
	  <th colspan="2" scope="row"><abbr>
	    <xsl:attribute name="title">
	      <xsl:call-template name="l10n:print-pricing--photo--desc"/>
	    </xsl:attribute>
	    <xsl:call-template name="l10n:print-pricing--photo"/>
	  </abbr></th>
	  <td data-pricing-cost="3"  class="print-pricing-low-budget">3 €</td>
	  <td data-pricing-cost="10" class="print-pricing-low-budget">10 €</td>
	  <td data-pricing-cost="30" class="print-pricing-low-budget">30 €</td>
	  <td data-pricing-cost="60">60 €</td>
	  <td data-pricing-cost="90">90 €</td>
	  <td data-pricing-cost="110">110 €</td>
	  <td data-pricing-cost="120">120 €</td>
	</tr>
	
	<tr class="print-pricing-cost" 
	    data-pricing-media="fine-art">
	  <th colspan="2" scope="row"><abbr>
	    <xsl:attribute name="title">
	      <xsl:call-template name="l10n:print-pricing--fine-art--desc"/>
	    </xsl:attribute>
	    <xsl:call-template name="l10n:print-pricing--fine-art"/>
	  </abbr></th>
	  <td/>
	  <td data-pricing-cost="20">20 €</td>
	  <td data-pricing-cost="45">45 €</td>
	  <td data-pricing-cost="90">90 €</td>
	  <td data-pricing-cost="135">135 €</td>
	  <td data-pricing-cost="165">165 €</td>
	  <td data-pricing-cost="180">180 €</td>
	</tr>
	
	<tr class="print-pricing-cost" 
	    data-pricing-media="dibond">
	  <th colspan="2" scope="row"><abbr>
	    <xsl:attribute name="title">
	      <xsl:call-template name="l10n:print-pricing--dibond--desc"/>
	    </xsl:attribute>
	    <xsl:call-template name="l10n:print-pricing--dibond"/>
	  </abbr></th>
	  <td/>
	  <td/>
	  <td data-pricing-cost="70">70 €</td>
	  <td data-pricing-cost="135">135 €</td>
	  <td data-pricing-cost="200" class="print-pricing-medium-budget">200 €</td>
	  <td data-pricing-cost="250">250 €</td>
	  <td data-pricing-cost="270" class="print-pricing-high-budget">270 €</td>
	</tr>
	
	<tr class="print-pricing-cost" 
	    data-pricing-media="canvas">
	  <th colspan="2" scope="row"><abbr>
	    <xsl:attribute name="title">
	      <xsl:call-template name="l10n:print-pricing--canvas--desc"/>
	    </xsl:attribute>
	    <xsl:call-template name="l10n:print-pricing--canvas"/>
	  </abbr></th>
	  <td/>
	  <td/>
	  <td data-pricing-cost="70">70 €</td>
	  <td data-pricing-cost="135">135 €</td>
	  <td data-pricing-cost="200" class="print-pricing-medium-budget">200 €</td>
	  <td data-pricing-cost="250">250 €</td>
	  <td data-pricing-cost="270" class="print-pricing-high-budget">270 €</td>
	</tr>
	
	<tr class="print-pricing-cost" 
	    data-pricing-media="acrylic-glass">
	  <th colspan="2" scope="row"><abbr>
	    <xsl:attribute name="title">
	      <xsl:call-template name="l10n:print-pricing--acrylic--desc"/>
	    </xsl:attribute>
	    <xsl:call-template name="l10n:print-pricing--acrylic"/>
	  </abbr></th>
	  <td/>
	  <td/>
	  <td/>
	  <td data-pricing-cost="270">270 €</td>
	  <td data-pricing-cost="400">400 €</td>
	  <td data-pricing-cost="500">500 €</td>
	  <td data-pricing-cost="540" class="print-pricing-high-budget">540 €</td>
	</tr>
      </tbody>
    </table>
  </xsl:template>

  <xsl:template match="m:print-pricing-options">
    <table id="print-pricing-option">
      <xsl:copy-of select="@style|@class"/>
      <caption>
	<xsl:call-template name="l10n:print-pricing--options--title"/>
      </caption>
      <colgroup>
	<col/>
	<col/>
	<col/>
      </colgroup>
      <thead>
	<tr>
	  <th>
	    <xsl:call-template name="l10n:print-pricing--option"/>
	  </th><th scope="col">
	    <xsl:call-template name="l10n:print-pricing--description"/>
	  </th><th scope="col">
	    <xsl:call-template name="l10n:print-pricing--price"/>
	  </th>
	</tr>
      </thead>
      <tbody>
	<tr data-pricing-option="artist-print">
	  <th scope="row">
	    <xsl:call-template 
		name="l10n:print-pricing--artist-print"/>
	  </th>
	  <td>
	    <xsl:call-template 
		name="l10n:print-pricing--artist-print--desc"/>
	  </td>
	  <td data-pricing-option-cost="x*2"
	      class="print-pricing-high-budget">× 2</td>
	</tr>
	<tr data-pricing-option="certificat">
	  <th scope="row"><xsl:call-template 
		name="l10n:print-pricing--certificat"/>
	  </th>
	  <td>
	    <xsl:call-template 
		name="l10n:print-pricing--certificat--desc"/>
	  </td>
	  <td data-pricing-option-cost="x+50"
	      class="print-pricing-high-budget">+ 50 €</td>
	</tr>
      </tbody>
    </table>
  </xsl:template>

</xsl:stylesheet>

