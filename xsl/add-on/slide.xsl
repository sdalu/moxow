<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: slide.xsl 3301 2013-04-09 13:44:20Z sdalu $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:m  ="http://www.moxow.org/main"
  xmlns:f  ="http://www.moxow.org/function"

  exclude-result-prefixes="xsl m f"
>

<!--
XSL: m:slide (m:handle / m:caption / m:pager)
JS : jquery.cycle
CSS: slide

    figure
      div.mxw-slide-n-controls
	div.mxw-slide-n-actions
	  div.mxw-slide-prev
	  div.mxw-slide-next
	  div.mxw-slide-container
	div.mxw-slide-pager
      figcaption
	div.mxw-slide-caption
-->

  <!-- ================================================================ -->
  <!-- === Slide                                                    === -->
  <!-- ================================================================ -->
  <xsl:template match="m:slide">
    <xsl:variable name="id" select="f:if-absent(@id, generate-id())"/>
    
    <figure id="{$id}" class="mxw-slide {@class}">
      <xsl:copy-of select="@style"/> 

      <div class="mxw-slide-n-controls">
	<div class="mxw-slide-n-actions">
	  <!-- Handle (Prev/Next) -->
	  <xsl:if test="m:handle">
	    <!-- Prev handle -->
	    <div class="mxw-slide-prev">
	      <xsl:if test="m:handle/@in-title and m:handle/@prev">
		<xsl:attribute name="title">
		  <xsl:value-of select="m:handle/@prev"/>
		</xsl:attribute>
	      </xsl:if>
	      <span>
		<xsl:choose>
		  <xsl:when test="not(m:handle/@in-title) and m:handle/@prev">
		    <xsl:value-of select="m:handle/@prev"/>
		  </xsl:when>
		  <xsl:otherwise>
		    <xsl:text>⇦</xsl:text>
		  </xsl:otherwise>
		</xsl:choose>
	      </span>
	    </div>
	    <!-- Next handle -->
	    <div class="mxw-slide-next">
	      <xsl:if test="m:handle/@in-title and m:handle/@next">
		<xsl:attribute name="title">
		  <xsl:value-of select="m:handle/@next"/>
		</xsl:attribute>
	      </xsl:if>
	      <span>
		<xsl:choose>
		  <xsl:when test="not(m:handle/@in-title) and m:handle/@next">
		    <xsl:value-of select="m:handle/@next"/>
		  </xsl:when>
		  <xsl:otherwise>
		    <xsl:text>⇨</xsl:text>
		  </xsl:otherwise>
		</xsl:choose>
	      </span>
	    </div>
	  </xsl:if>
	  
	  <!-- Slide content -->
	  <div class="mxw-slide-container">
	    <xsl:apply-templates select="*[not(self::m:handle  or 
					       self::m:pager   or
					       self::m:caption   )]"/>
	  </div>
	</div>

	<!-- Slide pager -->
	<xsl:if test="m:pager">
	  <xsl:variable name="dot">
	    <xsl:choose>
	      <xsl:when test="m:pager/@dot">
		<xsl:value-of select="m:pager/@dot"/>
	      </xsl:when>
	      <xsl:otherwise>
		<xsl:text>●</xsl:text>
	      </xsl:otherwise>
	    </xsl:choose>
	  </xsl:variable>

	  <div class="mxw-slide-pager">
	    <ul>
	      <xsl:for-each select="*[not(self::m:handle  or 
				          self::m:pager   or
					  self::m:caption   )]">
		<li><xsl:value-of select="$dot"/></li>
	      </xsl:for-each>
	    </ul>
	  </div>
	</xsl:if>
      </div>

      <!-- Caption -->
      <xsl:if test="m:caption">
	<figcaption>
	  <div class="mxw-slide-caption"/>
	</figcaption>
      </xsl:if>
    </figure>

    <script>
      $('#<xsl:value-of select="$id"/> .mxw-slide-container').cycle({
        <xsl:if test="@effect">
	  fx:    '<xsl:value-of select="@effect"/>', 
	</xsl:if>
	<xsl:if test="m:handle">
	  prev:  '#<xsl:value-of select="$id"/> .mxw-slide-prev',
	  next:  '#<xsl:value-of select="$id"/> .mxw-slide-next',
	</xsl:if>
	<xsl:if test="m:pager">
	  pager: '#<xsl:value-of select="$id"/> .mxw-slide-pager ul', 
	  pagerAnchorBuilder: function(idx, slide) { 
            return '#<xsl:value-of select="$id"/> .mxw-slide-pager li:eq(' + idx + ')'; },
	  activePagerClass: 'selected',
	</xsl:if>
	<xsl:if test="@interval">
	  timeout: <xsl:value-of select="@interval"/>,
	</xsl:if>
	<xsl:if test="@width">
	  width: <xsl:value-of select="@width"/>,
	</xsl:if>
	<xsl:if test="@height">
	  height: <xsl:value-of select="@height"/>,
	</xsl:if>
	<xsl:if test="@pause">
	  pause: true,
	</xsl:if>
	<xsl:if test="m:caption">	
	  before: function(curElt, elt) {
	    $('#<xsl:value-of select="$id"/> .mxw-slide-caption')
	      .fadeOut('fast', function() {
	        var caption = elt.title ||elt.alt;
		<xsl:if test="m:caption/@src">
		  caption = <xsl:value-of select="m:caption/@src"/>;
  	        </xsl:if>
	        $(this).html(caption); })
	      .fadeIn('fast'); },
	</xsl:if>
	fit: 1
      });
    </script>
  </xsl:template>

</xsl:stylesheet>
