<xsl:stylesheet version="1.0"
  xmlns:xsl ="http://www.w3.org/1999/XSL/Transform"
  xmlns:func="http://exslt.org/functions"
  xmlns:m   ="http://www.moxow.org/main"
  xmlns:g   ="http://www.moxow.org/add-on/gallery"
  xmlns:is  ="http://www.moxow.org/add-on/items-selling"

  extension-element-prefixes="func"
  exclude-result-prefixes="xsl func m is"
>

<!--
    is:item-separator
    is:item (g:thumb)
    is:selling
-->

  <!-- ================================================================ -->
  <!-- === Init                                                     === -->
  <!-- ================================================================ -->

  <!-- Detection -->
  <xsl:variable  name="is:init-content" select="$m:content"/>
  <func:function name="is:init-required"/>

  <!-- Initialisation -->
  <xsl:template name="is:init-do-head"/>



  <!-- ================================================================ -->
  <!-- ===                                                          === -->
  <!-- ================================================================ -->



  <xsl:template match="is:item-separator">
    <tr id="{@id}"><th colspan="4">
      <xsl:value-of select="@title"/>
    </th></tr>
  </xsl:template>

  <xsl:template match="is:item">
	<xsl:variable name="id">
	  <xsl:value-of select="generate-id()"/>
	</xsl:variable>
	<tr id="{$id}">
	  <td class="preview-main">
	      <img src="{g:thumb/@src}" alt=""/>
	  </td>
	  <td class="content">
	    <h3 class="title"><xsl:value-of select="@title"/></h3>
	    <div class="description">
	      <xsl:apply-templates select="node()[not(self::g:thumb)]"/>
	    </div>
	  </td>
	  <td class="preview-list">
	    <xsl:for-each select="g:thumb">
	      <img src="{@src}" data-fancybox-group="{$id}" alt=""/>
	    </xsl:for-each>
	  </td>
	  <td class="price">
	    <xsl:value-of select="@price"/> €
	    <xsl:if test="@reserved">
	      <br/><span style="font-size: 60%; color: grey;" ><i>Réservé</i></span>
	    </xsl:if>
	  <script>
  (function() {
    var id = '<xsl:value-of select="$id"/>';
      var title = $('#'+id+ ' .content h3', this).html();
    $('#'+id+ ' .preview-list img').css('cursor', 'pointer').fancybox({
        openSpeed: 'fast',
        closeSpeed: 'fast',
nextSpeed:  'normal',
prevSpeed: 'normal',
nextEffect: 'fade',
prevEffect: 'fade',
        title : title,
        beforeLoad: function() { this.href = this.element.src; } });
    $('#'+id + ' .preview-main img').css('cursor', 'pointer').click(function() {
      $('#'+id + ' .preview-list img:first').click();
    });
  })();
	  </script>
	  </td>
	</tr>
  </xsl:template>

  <xsl:template match="is:selling">
    <table class="selling">
      <xsl:apply-templates/>
    </table>
  </xsl:template>

</xsl:stylesheet>
