<xsl:stylesheet version="1.0"
  xmlns:xsl ="http://www.w3.org/1999/XSL/Transform"
  xmlns:func="http://exslt.org/functions"
  xmlns:m   ="http://www.moxow.org/main"
  xmlns:sc  ="http://www.moxow.org/add-on/sysadmin-code"

  extension-element-prefixes="func"
  exclude-result-prefixes="xsl m sc"
>

  <!-- ================================================================ -->
  <!-- === Init                                                     === -->
  <!-- ================================================================ -->

  <!-- Detection -->
  <xsl:variable  name="sc:init-content" select="$m:content"/>
  <func:function name="sc:init-required"/>

  <!-- Initialisation -->
  <xsl:template name="sc:init-do-head"/>



  <!-- ================================================================ -->
  <!-- === Sysadmin & Code                                          === -->
  <!-- ================================================================ -->

  <!-- Config -->
  <xsl:template match="sc:config">
    <pre class="sysadmin config">
      <xsl:if test="@file">
	<xsl:attribute name="data-sysadmin-filename">
	  <xsl:value-of select="@file"/>
	</xsl:attribute>
      </xsl:if>
      <xsl:apply-templates/>
    </pre>
  </xsl:template>


  <!-- Code -->
  <xsl:template match="sc:code">
    <pre class="sysadmin sysadmin-script sysadmin-{@language}">
      <xsl:apply-templates/>
    </pre>
  </xsl:template>


  <!-- session -->
  <xsl:template match="sc:session">
    <xsl:variable name="way">
      <xsl:choose>
	<xsl:when test="@way"><xsl:value-of select="@way"/></xsl:when>
	<xsl:otherwise>session</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <pre class="sysadmin sysadmin-{$way} sysadmin-{@type}">
      <xsl:if test="@program">
	<xsl:attribute name="data-sysadmin-program">
	  <xsl:value-of select="@program"/>
	</xsl:attribute>
      </xsl:if>
      <xsl:apply-templates/>
    </pre>
  </xsl:template>  

</xsl:stylesheet>
