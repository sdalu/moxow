<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: map.xsl 3186 2012-11-01 19:41:47Z sdalu $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:m  ="http://www.moxow.org/main"
  xmlns:f  ="http://www.moxow.org/function"
  xmlns:map="http://www.moxow.org/add-on/map"

  exclude-result-prefixes="xsl m f map"
>

  <!-- ================================================================ -->
  <!-- === Constants                                                === -->
  <!-- ================================================================ -->
  
  <!-- Default classes used by elements -->
  <xsl:variable name="map:track-class"          select="'map-track'" />


  <!-- ================================================================ -->
  <!-- === Worldmap                                                 === -->
  <!-- ================================================================ -->

  <xsl:template match="map:country" name="map:country">
    <xsl:param name="country" select="@country"/>
    <xsl:param name="style"   select="@style"  />
    <xsl:param name="class"   select="@class"  />
    <xsl:param name="id"      select="@id"     />
    <xsl:param name="caption" select="@caption"/>
    <xsl:variable name="map-id" select="f:if-absent($id, generate-id())"/>
    <figure>
      <div id="{$map-id}" class="thinbox {$class}" style="{$style}"/>
      <xsl:if test="$caption">
	<figcaption>
	  <xsl:value-of select="$caption"/>
	</figcaption>
      </xsl:if>
    </figure>
    <script type="text/javascript">
      (function($) {
	var country = '<xsl:value-of select="$country"/>';
	var mapId   = '<xsl:value-of select="$map-id" />';
	var lang    = '<xsl:value-of select="$lang"   />';
	$.available('#'+mapId, function() {
	  Maps.init({ lang: lang });
	  Maps.country(mapId, geomap_country[country], {
	    noControls: true
	  });
        });
      })(jQuery);
    </script>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- === Track                                                    === -->
  <!-- ================================================================ -->

  <xsl:template match="map:track">
    <xsl:variable name="id" 
		  select="f:if-absent(@id, generate-id())"/>
    <xsl:variable name="class" 
		  select="f:space-list-collapse($map:track-class, @class)"/>

    <div id="{$id}">
      <xsl:copy-of select="@style"/>
      <xsl:if test="normalize-space($class)">
	<xsl:attribute name="class">
	  <xsl:value-of select="$class"/>
	</xsl:attribute>
      </xsl:if>
    </div>
    <script type="text/javascript">
      (function() {       
	var kml     = '<xsl:value-of select="@kml" />';
        var mapId   = '<xsl:value-of select="$id"  />';
	var opts    = {
	  highlight : true,
	  useSocial : true,
	  <xsl:if test="@picview">
	  selected  : Maps.helpers.poi_select_fancybox,
	  unselected: Maps.helpers.poi_unselect_fancybox,
	  </xsl:if>
	  lang      : '<xsl:value-of select="$lang"/>'
	};
	Maps.track(kml, mapId, opts);
      })();
    </script>
  </xsl:template>
</xsl:stylesheet>
