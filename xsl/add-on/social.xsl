<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: social.xsl 3186 2012-11-01 19:41:47Z sdalu $ -->


<xsl:stylesheet version="1.0"
  xmlns:xsl   ="http://www.w3.org/1999/XSL/Transform"
  xmlns:str   ="http://exslt.org/strings"
  xmlns:func  ="http://exslt.org/functions"
  xmlns:crypto="http://exslt.org/crypto"
  xmlns:m     ="http://www.moxow.org/main"
  xmlns:f     ="http://www.moxow.org/function"
  xmlns:s     ="http://www.moxow.org/add-on/social"

  extension-element-prefixes="str func crypto"
  exclude-result-prefixes="xsl m f s"
>

  <!-- ================================================================ -->
  <!-- === Constants                                                === -->
  <!-- ================================================================ -->
  
  <!-- Default clases used by elements -->
  <xsl:variable name="s:badge-class"      select="'user-badge'"/>

  <!-- Gravatar  -->
  <xsl:variable name="s:gravatar-size"    select="'55'"        />
  <xsl:variable name="s:gravatar-default" select="'wavatar'"   />



  <!-- ================================================================ -->
  <!-- === Init                                                     === -->
  <!-- ================================================================ -->

  <!-- Detection -->
  <xsl:variable  name="s:init-content" select="$m:content"/>
  <func:function name="s:init-required"/>

  <!-- Initialisation -->
  <xsl:template name="s:init-do-head"/>

  


  <!-- ================================================================ -->
  <!-- === Gravatar                                                 === -->
  <!-- ================================================================ -->

 <func:function name="s:gravatar-img-url">
    <xsl:param name="id"/>
    <xsl:param name="size"    select="$s:gravatar-size"    />
    <xsl:param name="default" select="$s:gravatar-default" />

    <xsl:variable name="gravatar-id">
      <xsl:choose>
	<xsl:when test="not($id)">
	  <xsl:text>0</xsl:text>
	</xsl:when>
	<xsl:when test="contains($id/@gravatar, '@')">
	  <xsl:value-of select="crypto:md5($id/@gravatar)"/>
	</xsl:when>
	<xsl:when test="$id/@gravatar">
	  <xsl:value-of select="$id/@gravatar"/>
	</xsl:when>
	<xsl:when test="$id/@email">
	  <xsl:value-of select="crypto:md5($id/@email)"/>
	</xsl:when>
	<xsl:when test="contains($id, '@')">
	  <xsl:value-of select="crypto:md5($id)"/>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:value-of select="$id"/>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <func:result select="concat(
	 'http://www.gravatar.com/avatar/', $gravatar-id, '.jpg', 
	 '?', 's=', $size, '&#38;', 'd=', $default)"/>
  </func:function>




  <!-- ================================================================ -->
  <!-- === Badge                                                    === -->
  <!-- ================================================================ -->
  <xsl:template match="s:badge">
    <aside itemscope="itemscope" itemtype="http://schema.org/Person">
      <xsl:copy-of select="@id|@style"/>
      <xsl:variable name="class" 
		    select="f:space-list-collapse($s:badge-class, @class)"/>
      <xsl:if test="normalize-space($class)">
	<xsl:attribute name="class">
	  <xsl:value-of select="$class"/>
	</xsl:attribute>
      </xsl:if>

      <xsl:variable name="face">
	<xsl:choose>
	  <xsl:when test="@face">
	    <xsl:value-of select="@face"/>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:value-of select="s:gravatar-img-url(
				    s:link[@type='email'][1]/@user, 100)"/>
	  </xsl:otherwise>
	</xsl:choose>
      </xsl:variable>


      <table style="width: 100%;">
	<tr>
	  <xsl:if test="node()[not(self::s:link)]">
	    <td class="m-badge-description">
	      <xsl:apply-templates select="node()[not(self::s:link)]"/>
	    </td>
	  </xsl:if>
	  <td style="vertical-align: top;" class="m-badge-face">
	    <xsl:choose>
	      <xsl:when test="@url">
		<a itemprop="url" href="{@url}">
		  <img src="{$face}" itemprop="image"/>
		</a>
	      </xsl:when>
	      <xsl:otherwise>
		<img src="{$face}" itemprop="image"/>
	      </xsl:otherwise>
	    </xsl:choose>
	    <xsl:if test="@name">
	      <span itemprop="name" class="m-badge-name">
		<xsl:value-of select="@name"/>
	      </span>
	    </xsl:if>
	  </td>

	  <xsl:if test="s:link">
	    <td>
	      <xsl:if test="s:link[@type]">
		<div class="m-badge-networks">
		  <xsl:for-each select="s:link[@type]">
		    <xsl:choose>
		      <xsl:when test="@type = 'facebook'">
			<a rel="me" href="https://www.facebook.com/{@user}">
			  <img src="/images/common/web/32x32/facebook.png"/>
			</a>
		      </xsl:when>
		      <xsl:when test="@type = 'google'">
			<a rel="me" href="https://plus.google.com/{@user}">
			  <img src="/images/common/web/32x32/googleplus.png"/>
			</a>
		      </xsl:when>
		      <xsl:when test="@type = 'linkedin'">
			<a rel="me" href="http://www.linkedin.com/in/{@user}">
			  <img src="/images/common/web/32x32/linkedin.png"/>
			</a>
		      </xsl:when>
		      <xsl:when test="@type = 'email'">
			<a rel="me" href="mailto:{@user}" itemprop="email">
			  <img src="/images/common/web/32x32/mail.png"/>
			</a>
		      </xsl:when>
		    </xsl:choose>
		  </xsl:for-each>
		</div>
	      </xsl:if>

	      <xsl:if test="s:link[@href]">
		<ul class="m-badge-links" 
		    style="list-style-position: inside; margin: 0;">
		  <xsl:for-each select="s:link[@href]">
		    <li><a href="{@href}">
		      <xsl:value-of select="@name"/></a></li>
		  </xsl:for-each>
		</ul>
	      </xsl:if>
	    </td>
	  </xsl:if>
	</tr>
      </table>
    </aside>
  </xsl:template>

</xsl:stylesheet>
