<?xml version="1.0" encoding="utf-8"?>

<!DOCTYPE xsl:stylesheet [
<!ENTITY % HTMLlat1 PUBLIC
  "-//W3C//ENTITIES Latin 1 for XHTML//EN" "xhtml-lat1.ent">
%HTMLlat1;

<!ENTITY % HTMLsymbol PUBLIC
  "-//W3C//ENTITIES Symbols for XHTML//EN" "xhtml-symbol.ent">
%HTMLsymbol;

<!ENTITY % HTMLspecial PUBLIC
  "-//W3C//ENTITIES Special for XHTML//EN" "xhtml-special.ent">
%HTMLspecial;
]>

<!-- http://www.tei-c.org/wiki/index.php/Remove-Namespaces.xsl -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:m="http://www.moxow.org/main"
  xmlns:math="http://exslt.org/math"
  xmlns:str="http://exslt.org/strings"
  xmlns:date="http://exslt.org/dates-and-times"

  exclude-result-prefixes="xsl m math str date"
>

  <!-- !! We need to build an index to do grouping in for-each       !! -->
  <!-- !! loop, this won't be necessary in xslt 2.0                  !! -->
  <xsl:key name="music-index"
           match="/m:moxow/m:content//m:music/m:collection"
           use="@groupby"/>

  <!-- Disk template -->
  <!-- ============= -->
  <xsl:template match="m:disk">
    <table class="cd">
      <!-- Header -->
      <tr>
        <th colspan="2" class="track">
          <xsl:if test="@idx &gt; 0"><xsl:value-of select="@idx"/></xsl:if>
        </th>
        <th class="time">Time</th>
        <th class="playlist">Playlist</th>
        <th class="request">Req</th>
        <th class="rating">Rating</th>
      </tr>
      <xsl:text> <!-- insert line break -->
      </xsl:text>

      <!-- Tracks -->
      <xsl:for-each select="m:track">
        <tr>
          <td class="tracknumber"><xsl:value-of select="@idx"/></td>
          <td class="songname"><a href="{@href}.ogg">
            <xsl:value-of select="@title"/></a>
          </td>
          <td class="time"><xsl:value-of select="@length"/></td>
          <td class="playlist">
            <a href="{@href}.m3u?ogg"><img
               src="/images/common/icons/16x16/mimetype-ogg.png" alt="[ogg]"/></a>
            <a href="{@href}.m3u?mp3"><img
               src="/images/common/icons/16x16/mimetype-mp3.png" alt="[mp3]"/></a>
          </td>
          <td class="request">
            <a href="{@href}.req"><img
               src="/images/common/icons/16x16/schedule-song.png" alt="[R]"/></a>
          </td>
          <td class="rating">
            <xsl:choose>
              <xsl:when test="@rating=0">
                &#10025;&#10025;&#10025;&#10025;&#10025;
              </xsl:when>
              <xsl:when test="@rating=1">
                &#10030;&#10025;&#10025;&#10025;&#10025;
              </xsl:when>
              <xsl:when test="@rating=2">
                &#10030;&#10030;&#10025;&#10025;&#10025;
              </xsl:when>
              <xsl:when test="@rating=3">
                &#10030;&#10030;&#10030;&#10025;&#10025;
              </xsl:when>
              <xsl:when test="@rating=4">
                &#10030;&#10030;&#10030;&#10030;&#10025;
              </xsl:when>
              <xsl:when test="@rating=5">
                &#10030;&#10030;&#10030;&#10030;&#10030;
              </xsl:when>
            </xsl:choose>
          </td>
        </tr>
        <xsl:text> <!-- insert line break -->
        </xsl:text>
      </xsl:for-each>
      
    </table>
  </xsl:template>


  <!-- Build music page -->
  <!-- ================ -->
  <xsl:template match="m:music">
    <xsl:choose>

      <!-- === Index === -->
      <xsl:when test="@type='index'">
        <!-- Accelerator -->
        <!-- !! Dirty XSLT hack for iteration !! -->
        <table id="accel_up" class="cd-accelerator"><tr>
          <xsl:variable name="az" select='"*ABCDEFGHIJKLMNOPQRSTUVWXYZ"'/>
          <xsl:for-each select="(//node())[position() &lt;= 27]">
            <xsl:variable name="char" select='substring($az, position(), 1)'/>
            <td>
              <xsl:choose>
                <xsl:when test="key('music-index', $char)">
                  <a href="#accel_{$char}"><xsl:value-of select="$char"/></a>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="$char"/>
                </xsl:otherwise>
              </xsl:choose>
            </td>
          </xsl:for-each>
        </tr></table>
      
        <!-- Albums -->
        <dl class="cd-albumlist">
          <xsl:for-each select="m:collection[
                              generate-id(.) = generate-id(key('music-index', 
                                                               @groupby))]">
            <xsl:comment>= <xsl:value-of select="@groupby"/> =</xsl:comment>
            <xsl:text> <!-- insert line break -->
            </xsl:text>
          
            <dt id="accel_{@groupby}" class="cd-accelerator">
              <xsl:value-of select="@groupby"/>
              <span><a title="up" href="#accel_up"><img
                src="/images/common/icons/16x16/up.png" alt="&#8657;"/></a></span>
            </dt>
            
            <xsl:for-each select="key('music-index', @groupby)">
              <dt class="cd-artist">
                <a href="{@href}"><xsl:value-of select="@name"/></a>
              </dt>
              
              <xsl:for-each select="m:album">
                <dd>
                  <a href="{@href}">
                    <xsl:value-of select="@name"/>
                    <span>&#160;<xsl:value-of select="@tracks"/></span>
                  </a>
                </dd>
              </xsl:for-each>
              
              <xsl:text> <!-- insert line break -->
              </xsl:text>          
            </xsl:for-each>
          </xsl:for-each>
        </dl>
      </xsl:when>

      <!-- === Collection === -->
      <xsl:when test="@type='collection'">
        <xsl:for-each select="m:album">
          <table class="album">
            <!-- Name -->
            <caption><a href="{@href}/">
              <xsl:value-of select="@title"/>
            </a></caption>

            <!-- Cover -->
            <tr><td class="cover">
              <a href="{@href}/">
                <xsl:choose>
                  <xsl:when test="@cover">
                    <img class="cdcover" alt="cover: {@title}"
                                         src="{@href}/small-cover.jpg"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <img class="cdcover" alt="no cover"
                                         src="/images/music/nocover.png"/>
                  </xsl:otherwise>
                </xsl:choose>
              </a>
              <br/>
              <a href="{@href}/all.m3u?ogg"><img
                 src="/images/common/icons/16x16/mimetype-ogg.png" alt="[ogg]"
                 title="Album playlist in OGG format"/></a>
              <a href="{@href}/all.m3u?mp3"><img
                 src="/images/common/icons/16x16/mimetype-mp3.png" alt="[mp3]"
                 title="Album playlist in MP3 format"/></a>
              <xsl:choose>
                <xsl:when test="@cover">
                  <a href="{@href}/cover.jpg"><img
                     src="/images/common/icons/16x16/mimetype-jpg.png" alt="[img]"
                     title="Album cover image"/></a>
                </xsl:when>
                <xsl:otherwise>
                  <img src="/images/common/icons/16x16/nothing.png" alt=""/>
                </xsl:otherwise>
              </xsl:choose>

            <!-- Tracks -->
            </td><td class="content">
              <xsl:for-each select="m:disk">
                <xsl:apply-templates select="."/>
              </xsl:for-each>
            </td></tr>
          </table>  
        </xsl:for-each>
      </xsl:when>

      <!-- === Album === -->
      <xsl:when test="@type='album'">

        <div class="cdview">
          <div class="cdcover">
            <xsl:choose>
              <xsl:when test="m:album/@cover">
                <a href="cover.jpg">
                  <img class="cdcover" alt="cover: {m:album/@title}"
                    src="small-cover.jpg"/>
                </a>
              </xsl:when>
              <xsl:otherwise>
                <img class="cdcover" alt="no cover" 
                  src="/images/music/nocover.png"/>
              </xsl:otherwise>
            </xsl:choose>

            <br/>
            <a href="all.m3u?ogg"><img src="/images/common/icons/16x16/mimetype-ogg.png"
                                       alt="[ogg]"/></a>
            <a href="all.m3u?mp3"><img src="/images/common/icons/16x16/mimetype-mp3.png"
                                       alt="[mp3]"/></a>
          </div>
          <xsl:for-each select="m:album/m:disk">
            <xsl:apply-templates select="."/>
          </xsl:for-each>
        </div>
      </xsl:when>

    </xsl:choose>
  </xsl:template>



</xsl:stylesheet>

