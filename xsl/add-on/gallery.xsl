<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: gallery.xsl 3213 2012-11-16 21:45:39Z sdalu $ -->


<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:str="http://exslt.org/strings"
  xmlns:func="http://exslt.org/functions"
  xmlns:exsl="http://exslt.org/common"
  xmlns:m  ="http://www.moxow.org/main"
  xmlns:f  ="http://www.moxow.org/function"
  xmlns:g  ="http://www.moxow.org/add-on/gallery"
  extension-element-prefixes="str func exsl"
  exclude-result-prefixes="xsl m f g"
>


<!-- Requires:
     VAR : m:content
     FUNC: m:resource-path
-->

<!-- 
  See:
    http://www.schema.org/Person 
    http://www.google.com/webmasters/tools/richsnippets
    http://www.google.com/support/webmasters/bin/answer.py?answer=1229920

g:gallery
g:strip
g:blog-pics


type      = index
behaviour = none | sync | link | zoom | gallery | map
src-thumb
href-is   = src
key-is    = basename
class
content
map       = #id
id
-->

  <!-- ================================================================ -->
  <!-- === Constants                                                === -->
  <!-- ================================================================ -->
  
  <!-- Default classes used by elements -->
  <xsl:variable name="g:gallery-class"          select="'gallery'"        />
  <xsl:variable name="g:gallery-behaviour"      select="'zoom'"           />

  <xsl:variable name="g:strip-class"            select="'gallery inline'" />
  <xsl:variable name="g:strip-behaviour"        select="'gallery'"        />

  <xsl:variable name="g:blog-pics-class"        select="'g-blog-pics'"    />
  <xsl:variable name="g:blog-pics-behaviour"    select="'gallery'"        />

  <xsl:variable name="g:picture-wall-class"     select="'g-picture-wall'" />
  <xsl:variable name="g:picture-wall-behaviour" select="'jump'"           />
  <xsl:variable name="g:picture-wall-width"     select="8"                />

  <xsl:variable name="g:gallery-microdata" select="'Photograph'"  />

  <xsl:variable name="g:thumbnail-class"   select="'thumbnail'"   />

  

  <!-- Conversion table for src-thumb -->
  <xsl:variable name="g:thumb-src-conv-rtf">
    <conv from="tiny"    to="thumb/t"/>
    <conv from="small"   to="thumb/s"/>
    <conv from="average" to="thumb/a"/>
    <conv from="medium"  to="thumb/m"/>
  </xsl:variable>
  <xsl:variable name="g:thumb-src-conv" 
		select="exsl:node-set($g:thumb-src-conv-rtf)"/>



  <!-- ================================================================ -->
  <!-- === Resources                                                === -->
  <!-- ================================================================ -->

  <!-- JavaScript -->
  <xsl:variable  name="g:js-path"  select="m:resource-path('js/'   )"/>
  <func:function name="g:js-path">
    <xsl:param name="file"/>
    <xsl:param name="path" select="$g:js-path"/>
    <func:result select="concat($path, $file)"/>
  </func:function>

  <!-- Stylesheet -->
  <xsl:variable  name="g:css-path" select="m:resource-path('style/')"/>
  <func:function name="g:css-path">
    <xsl:param name="file"/>
    <xsl:param name="path" select="$g:css-path"/>
    <func:result select="concat($path, $file)"/>
  </func:function>



  <!-- ================================================================ -->
  <!-- === Init                                                     === -->
  <!-- ================================================================ -->

  <!-- Detection -->
  <xsl:variable  name="g:init-content" select="$m:content"/>
  <func:function name="g:init-required">
    <xsl:param name="nodes" select="$g:init-content"/>
    <func:result select="$nodes//g:*[1]"/>
  </func:function>
  <func:function name="g:init-required-js">
    <xsl:param name="nodes" select="$g:init-content"/>
    <func:result select="$nodes//g:*[1]"/>
  </func:function>

  <!-- Initialisation -->
  <xsl:template name="g:init-for-head">
    <xsl:param name="js" select="true"/>
    <link rel="stylesheet" type="text/css"
	  href="{m:resource-path('fancybox/jquery.fancybox.css')}"/>
    <link rel="stylesheet" type="text/css"
	  href="{m:resource-path('fancybox/helpers/jquery.fancybox-thumbs.css')}"/>
    <link rel="stylesheet" type="text/css"
	  href="{m:resource-path('fancybox/helpers/jquery.fancybox-buttons.css')}"/>


    <link rel="stylesheet" type="text/css"
	  href="{g:css-path('gallery.css')}"/>
    <xsl:choose>
      <xsl:when test="$js = 'use-map'">
	<script type="text/javascript" 
		src="http://www.google.com/jsapi?key=ABQIAAAAvJ1SzGydIBYyytosieLrEBQGBISaU2XnJ0Q3LpBSQTF-eHtcWxTRH_jSTU6nVVv7Ghguu0GRn4XPMg"/>
	<script type="text/javascript" 
		src="{g:js-path('geomap-data-travel.js')}"/>
	<script type="text/javascript" 
		src="{g:js-path('gallery-with-map.js')}"/>
      </xsl:when>
      <xsl:when test="$js">
	<script type="text/javascript" 
		src="{m:resource-path('fancybox/jquery.fancybox.js')}"/>
	<script type="text/javascript" 
		src="{m:resource-path('fancybox/helpers/jquery.fancybox-thumbs.js')}"/>
	<script type="text/javascript" 
		src="{m:resource-path('fancybox/helpers/jquery.fancybox-buttons.js')}"/>

	<script type="text/javascript" 
		src="{g:js-path('gallery.js')}"/>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="g:init-do-head">
    <xsl:if test="g:init-required()">
      <xsl:call-template name="g:init-for-head">
	<xsl:with-param name="js" select="g:init-required-js()"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
  


  <!-- ================================================================ -->
  <!-- === Function helper                                          === -->
  <!-- ================================================================ -->

  <!-- href for thumb -->
  <func:function name="g:thumb-href">
    <xsl:param name="href"   />
    <xsl:param name="href-is"/>
    <xsl:param name="src"    />
    <func:result>
      <xsl:choose>
        <xsl:when test="$href">			<!-- href is provided  -->
	  <xsl:value-of select="$href"/>
	</xsl:when>
	<xsl:when test="$href-is = 'dir'">	<!-- href is directory -->
	  <xsl:value-of select="concat(f:dirname($src), '/')"/>
	</xsl:when>
	<xsl:when test="$href-is = 'src'">	<!-- href is src       -->
	  <xsl:value-of select="$src"/>
	</xsl:when>
      </xsl:choose>
    </func:result>
  </func:function>

  <!-- key for thumb -->
  <func:function name="g:thumb-key">
    <xsl:param name="key"   />
    <xsl:param name="key-is"/>
    <xsl:param name="src"   />
    <func:result>
      <xsl:choose>
        <xsl:when test="$key">			<!-- key is provided  -->
	  <xsl:value-of select="$key"/>
	</xsl:when>
	<xsl:when test="$key-is = 'basename'">	<!-- key is basename -->
	  <xsl:variable name="k" select="f:basename($src)"/>
	  <xsl:choose>
	    <xsl:when test="f:ends-with('.jpg')">
	      <xsl:value-of select="substring($k, 1, string-length($k)-4)"/>
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:value-of select="$k"/>
	    </xsl:otherwise>
	  </xsl:choose>
	</xsl:when>
      </xsl:choose>
    </func:result>
  </func:function>

  <!-- converter for src-thumb -->
  <func:function name="g:thumb-src-conv">
    <xsl:param name="src"/>
    <func:result>
      <xsl:choose>
	<xsl:when test="f:starts-with($src, ':')">
	  <xsl:variable name="key" select="substring($src, 2)"/>
	  <xsl:value-of select="$g:thumb-src-conv/conv[@from = $key]/@to"/>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:value-of select="$src"/>
	</xsl:otherwise>
      </xsl:choose>
    </func:result>
  </func:function>

  <!-- src for thumb -->
  <func:function name="g:thumb-src">
    <xsl:param name="src"      />
    <xsl:param name="src-thumb"/>
    <func:result>
      <xsl:variable name="thumb" select="g:thumb-src-conv($src-thumb)"/>
      <xsl:choose>
	<xsl:when test="f:starts-with($thumb, '/')"> <!-- prefix       -->
	  <xsl:message terminate="yes">
	    src-thumb with abolute path not supported yet
	  </xsl:message>
	</xsl:when>
	<xsl:when test="$thumb">                <!-- insert thumb dir  -->
	  <xsl:value-of select="f:path-insert-last($src, $thumb)"/>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:value-of select="$src"/>		<!-- use plain src     -->
	</xsl:otherwise>
      </xsl:choose>
    </func:result>
  </func:function>

  <!-- img for thumb -->
  <func:function name="g:thumb-img">
    <xsl:param name="src"      />
    <xsl:param name="attrs"    />
    <xsl:param name="microdata"/>

    <func:result>
      <img src="{$src}">
	<xsl:if test="$microdata">
	  <xsl:attribute name="itemprop">
	    <xsl:text>image</xsl:text>
	  </xsl:attribute>
	</xsl:if>
	<xsl:copy-of select="$attrs"/>
	<xsl:if test="$attrs/../@title">
	  <xsl:attribute name="alt">
	    <xsl:value-of select="$attrs/../@title"/>
	  </xsl:attribute>
	</xsl:if>
      </img>
    </func:result>
  </func:function>

  <!-- caption use data from thumb -->
  <func:function name="g:thumb-caption-use">
    <xsl:param name="caption-use"/>
    <xsl:param name="thumb"      />
    <func:result>
      <xsl:choose>
	<xsl:when test="$caption-use = 'title' and $thumb/@title">
	  <xsl:value-of select="$thumb/@title"/>
	</xsl:when>
	<xsl:when test="$caption-use = 'alt'   and $thumb/@alt">
	  <xsl:value-of select="$thumb/@alt"/>
	</xsl:when>
      </xsl:choose>
    </func:result>
  </func:function>

  <!-- wrap data in element/class -->
  <func:function name="g:wrap-element-class">
    <xsl:param name="data"   />
    <xsl:param name="element"/>
    <xsl:param name="class"  />
    <func:result>
      <xsl:choose>
	<xsl:when test="$element">
	  <xsl:element name="{$element}">
	    <xsl:copy-of select="$data"/>
	    <xsl:if test="$class">
	      <xsl:attribute name="class">
		<xsl:value-of select="$class"/>
	      </xsl:attribute>
	    </xsl:if>
	  </xsl:element>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:value-of select="$data"/>
	</xsl:otherwise>
      </xsl:choose>
    </func:result>
  </func:function>

  <!-- Microdata item type -->
  <func:function name="g:microdata-itemtype">
    <xsl:param name="microdata"/>
    
    <func:result>
      <xsl:text>http://schema.org/</xsl:text>
      <xsl:choose>
	<xsl:when test="$microdata = '-'"/>
	<xsl:otherwise>
	  <xsl:value-of select="$microdata"/>
	</xsl:otherwise>
      </xsl:choose>
    </func:result>
  </func:function>





  <xsl:template match="g:thumb-resizer">
    <div id="thumbnail-sizer" 
	 data-thumb-width  ="{f:if-absent(@width,  200)}"
	 data-thumb-height ="{f:if-absent(@height, 200)}"
	 data-thumb-min    ="{f:if-absent(@min,     50)}"
	 data-thumb-max    ="{f:if-absent(@max,    100)}"
	 data-thumb-default="{f:if-absent(@default, 80)}">
      <xsl:copy-of select="@id|@style|@class"/>
    </div>
  </xsl:template>


  <!-- ================================================================ -->
  <!-- === Thumbnail                                                === -->
  <!-- ================================================================ -->

  
  <xsl:template match="g:thumb">
    <xsl:param name="microdata"           select="'-'"/>
    <xsl:param name="gallery"             select="parent::g:gallery"/>
    <xsl:param name="src-thumb"           select="$gallery/@src-thumb"/>
    <xsl:param name="src-base"            select="$gallery/@src-base"/>
    <xsl:param name="href-is"             select="$gallery/@href-is"/>
    <xsl:param name="key-is"              select="$gallery/@key-is"/>
    <xsl:param name="caption-use"         select="$gallery/@caption-use"/>
    <xsl:param name="caption-use-element" select="$gallery/@caption-use-element"/>
    <xsl:param name="caption-use-class"   select="$gallery/@caption-use-class"/>

    <xsl:variable name="src-fullpath">
      <xsl:if test="not(f:starts-with(@src, '/')) and 
		    normalize-space($src-base)">
	<xsl:value-of select="$src-base"/>
	<xsl:if test="not(f:ends-with($src-base, '/'))">
	  <xsl:text>/</xsl:text>
	</xsl:if>
      </xsl:if>
      <xsl:value-of select="@src"/>
    </xsl:variable>
    <xsl:variable name="src" 
		  select="g:thumb-src ($src-fullpath,  $src-thumb)"/>
    <xsl:variable name="href"
		  select="g:thumb-href(@href, $href-is, $src-fullpath)"/>
    <xsl:variable name="key"
		  select="g:thumb-key (@key,  $key-is,  $src-fullpath)"/>
    <xsl:variable name="img"  
		  select="g:thumb-img ($src, @alt|@title, $microdata)" />

    <div class="{$g:thumbnail-class} {@class}">
      <xsl:copy-of select="@style|@id"/> 
      <xsl:if test="$microdata">
	 <xsl:attribute name="itemscope">
	   <xsl:text>itemscope</xsl:text>
	 </xsl:attribute>
	 <xsl:attribute name="itemtype">
	   <xsl:value-of select="g:microdata-itemtype($microdata)"/>
	 </xsl:attribute> 
      </xsl:if>

      <!-- Grouping -->
      <xsl:if test="@grp">
	<xsl:attribute name="data-thumb-grp">
	  <xsl:value-of select="@grp"/>
	</xsl:attribute>
      </xsl:if>	

      <!-- Key used for adding -->
      <xsl:if test="normalize-space($key)">
	<xsl:attribute name="data-thumb-key">
	  <xsl:value-of select="$key"/>
	</xsl:attribute>
      </xsl:if>

      <!-- Picture ratio -->
      <xsl:if test="@ratio">
	<xsl:attribute name="data-thumb-ratio">
	  <xsl:value-of select="@ratio"/>
	</xsl:attribute>
      </xsl:if>

      <!-- Author -->
      <xsl:if test="@author">
	<div class="author">
	  <xsl:if test="$microdata">
	    <xsl:attribute name="itemprop">
	      <xsl:text>author</xsl:text>
	    </xsl:attribute>
	  </xsl:if>
	  <xsl:choose>
	    <xsl:when test="@author-href">
	      <a href="{@author-href}">
		<xsl:value-of select="@author"/>
	      </a>
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:value-of select="@author"/>
	    </xsl:otherwise>
	  </xsl:choose>
	</div>
      </xsl:if>

      <!-- Frame -->
        <xsl:choose>
	  <!-- with href    -->
          <xsl:when test="normalize-space($href)"> 
	    <a class="frame" href="{$href}"><div class="size">
	      <xsl:copy-of select="@rel"/>
	      <xsl:if test="$microdata">
		<xsl:attribute name="itemprop"> 
		  <xsl:text>url</xsl:text>
		</xsl:attribute>
	      </xsl:if>
	      <xsl:copy-of select="$img"/>
	    </div></a>
          </xsl:when>
	  <!-- without href -->
	  <xsl:otherwise>
	    <div class="frame"><div class="size">
	      <xsl:copy-of select="$img"/>
	    </div></div>
	  </xsl:otherwise>
	</xsl:choose>

      <!-- Caption -->
      <xsl:variable name="caption-nodes"
		    select="node()"/>
      <xsl:variable name="caption-data"
		    select="g:thumb-caption-use($caption-use, .)"/>
      <xsl:if test="normalize-space($caption-data) or 
		    normalize-space($caption-nodes)">
	<div class="caption">
	  <xsl:if test="$microdata">
	    <xsl:attribute name="itemprop">
	      <xsl:text>description</xsl:text>
	    </xsl:attribute>
	  </xsl:if>

	  <xsl:choose>
	    <xsl:when test="$caption-use-element">
	      <xsl:copy-of select="g:wrap-element-class($caption-data,
				   $caption-use-element, $caption-use-class)"/>
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:copy-of select="$caption-data"/>
	    </xsl:otherwise>
	  </xsl:choose>

	  <xsl:apply-templates select="$caption-nodes"/>
	</div>
      </xsl:if>

      <div style="clear: both;"/>
    </div>
  </xsl:template>






  <xsl:template match="g:thumb" mode="gallery2">
    <xsl:param name="microdata"           select="'-'"/>
    <xsl:param name="gallery"             select="parent::g:gallery2"/>
    <xsl:param name="src-thumb"           select="$gallery/@src-thumb"/>
    <xsl:param name="src-base"            select="$gallery/@src-base"/>
    <xsl:param name="href-is"             select="$gallery/@href-is"/>
    <xsl:param name="key-is"              select="$gallery/@key-is"/>
    <xsl:param name="caption-use"         select="$gallery/@caption-use"/>
    <xsl:param name="caption-use-element" select="$gallery/@caption-use-element"/>
    <xsl:param name="caption-use-class"   select="$gallery/@caption-use-class"/>

    <xsl:variable name="src-fullpath">
      <xsl:if test="not(f:starts-with(@src, '/')) and 
		    normalize-space($src-base)">
	<xsl:value-of select="$src-base"/>
	<xsl:if test="not(f:ends-with($src-base, '/'))">
	  <xsl:text>/</xsl:text>
	</xsl:if>
      </xsl:if>
      <xsl:value-of select="@src"/>
    </xsl:variable>
    <xsl:variable name="src" 
		  select="g:thumb-src ($src-fullpath,  $src-thumb)"/>
    <xsl:variable name="href"
		  select="g:thumb-href(@href, $href-is, $src-fullpath)"/>
    <xsl:variable name="key"
		  select="g:thumb-key (@key,  $key-is,  $src-fullpath)"/>
    <xsl:variable name="img"  
		  select="g:thumb-img ($src, @alt|@title, $microdata)" />

    <div class="{$g:thumbnail-class} {@class}">
      <xsl:copy-of select="@style|@id"/> 
      <xsl:if test="$microdata">
	 <xsl:attribute name="itemscope">
	   <xsl:text>itemscope</xsl:text>
	 </xsl:attribute>
	 <xsl:attribute name="itemtype">
	   <xsl:value-of select="g:microdata-itemtype($microdata)"/>
	 </xsl:attribute> 
      </xsl:if>

      <!-- Grouping -->
      <xsl:if test="@grp">
	<xsl:attribute name="data-thumb-grp">
	  <xsl:value-of select="@grp"/>
	</xsl:attribute>
      </xsl:if>	

      <!-- Key used for adding -->
      <xsl:if test="normalize-space($key)">
	<xsl:attribute name="data-thumb-key">
	  <xsl:value-of select="$key"/>
	</xsl:attribute>
      </xsl:if>

      <!-- Picture ratio -->
      <xsl:if test="@ratio">
	<xsl:attribute name="data-thumb-ratio">
	  <xsl:value-of select="@ratio"/>
	</xsl:attribute>
      </xsl:if>

      <!-- Author -->
      <xsl:if test="@author">
	<div class="author">
	  <xsl:if test="$microdata">
	    <xsl:attribute name="itemprop">
	      <xsl:text>author</xsl:text>
	    </xsl:attribute>
	  </xsl:if>
	  <xsl:choose>
	    <xsl:when test="@author-href">
	      <a href="{@author-href}">
		<xsl:value-of select="@author"/>
	      </a>
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:value-of select="@author"/>
	    </xsl:otherwise>
	  </xsl:choose>
	</div>
      </xsl:if>



      <!-- Frame -->
      <xsl:choose>
	<!-- with href    -->
        <xsl:when test="normalize-space($href)"> 
          <a href="{$href}" class="frame">
	    <xsl:copy-of select="@rel"/>
	    <xsl:if test="$microdata">
	      <xsl:attribute name="itemprop"> 
		<xsl:text>url</xsl:text>
	      </xsl:attribute>
	    </xsl:if>
	    <xsl:copy-of select="$img"/>
	  </a>
        </xsl:when>
	<!-- without href -->
	<xsl:otherwise>
	  <div class="frame">
	    <xsl:copy-of select="$img"/>
	  </div>
	</xsl:otherwise>
      </xsl:choose>

      <!-- Caption -->
      <xsl:variable name="caption-nodes"
		    select="node()"/>
      <xsl:variable name="caption-data"
		    select="g:thumb-caption-use($caption-use, .)"/>
      <xsl:if test="normalize-space($caption-data) or 
		    normalize-space($caption-nodes)">
	<div class="caption">
	  <xsl:if test="$microdata">
	    <xsl:attribute name="itemprop">
	      <xsl:text>description</xsl:text>
	    </xsl:attribute>
	  </xsl:if>

	  <xsl:choose>
	    <xsl:when test="$caption-use-element">
	      <xsl:copy-of select="g:wrap-element-class($caption-data,
				   $caption-use-element, $caption-use-class)"/>
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:copy-of select="$caption-data"/>
	    </xsl:otherwise>
	  </xsl:choose>

	  <xsl:apply-templates select="$caption-nodes"/>
	</div>
      </xsl:if>
    </div>
  </xsl:template>








  <xsl:template match="g:thumb" mode="inline">
    <xsl:param name="microdata"           select="'-'"/>
    <xsl:param name="gallery"             select="parent::g:gallery"/>
    <xsl:param name="src-thumb"           select="$gallery/@src-thumb"/>
    <xsl:param name="src-base"            select="$gallery/@src-base"/>
    <xsl:param name="href-is"             select="$gallery/@href-is"/>
    <xsl:param name="caption-use"         select="$gallery/@caption-use"/>
    <xsl:param name="caption-use-element" select="$gallery/@caption-use-element"/>
    <xsl:param name="caption-use-class"   select="$gallery/@caption-use-class"/>


    <xsl:variable name="src-fullpath">
      <xsl:if test="not(f:starts-with(@src, '/')) and 
		    normalize-space($src-base)">
	<xsl:value-of select="$src-base"/>
	<xsl:if test="not(f:ends-with($src-base, '/'))">
	  <xsl:text>/</xsl:text>
	</xsl:if>
      </xsl:if>
      <xsl:value-of select="@src"/>
    </xsl:variable>
    <xsl:variable name="src" 
		  select="g:thumb-src ($src-fullpath,  $src-thumb)"/>
    <xsl:variable name="href"
		  select="g:thumb-href(@href, $href-is, $src-fullpath)"/>
    <xsl:variable name="img"  
		  select="g:thumb-img ($src, @alt|@title, $microdata)" />

    <span class="{$g:thumbnail-class} {@class}">
      <xsl:copy-of select="@style|@id"/> 
      <xsl:if test="$microdata">
	 <xsl:attribute name="itemscope">
	   <xsl:text>itemscope</xsl:text>
	 </xsl:attribute>
	 <xsl:attribute name="itemtype">
	   <xsl:value-of select="g:microdata-itemtype($microdata)"/>
	 </xsl:attribute> 
      </xsl:if>
      
      <!-- Image -->
      <xsl:choose>
	<!-- with href    -->
        <xsl:when test="normalize-space($href)"> 
          <a class="frame" href="{$href}">
	    <xsl:copy-of select="@rel"/>
	    <xsl:if test="$microdata">
	      <xsl:attribute name="itemprop"> 
		<xsl:text>url</xsl:text>
	      </xsl:attribute>
	    </xsl:if>
	    <xsl:copy-of select="$img"/>
	  </a>
        </xsl:when>
	<!-- without href -->
	<xsl:otherwise>
	  <span class="frame">
	    <xsl:copy-of select="$img"/>
	  </span>
	</xsl:otherwise>
      </xsl:choose>

      <!-- Caption -->
      <xsl:variable name="caption-nodes"
		    select="node()"/>
      <xsl:variable name="caption-data"
		    select="g:thumb-caption-use($caption-use, .)"/>
      <xsl:if test="normalize-space($caption-data) or 
		    normalize-space($caption-nodes)">
	<span class="caption">
	  <xsl:if test="$microdata">
	    <xsl:attribute name="itemprop">
	      <xsl:text>description</xsl:text>
	    </xsl:attribute>
	  </xsl:if>

	  <xsl:choose>
	    <xsl:when test="$caption-use-element">
	      <xsl:copy-of select="g:wrap-element-class($caption-data,
				   $caption-use-element, $caption-use-class)"/>
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:copy-of select="$caption-data"/>
	    </xsl:otherwise>
	  </xsl:choose>

	  <xsl:apply-templates select="$caption-nodes"/>
	</span>
      </xsl:if>

    </span>
  </xsl:template>





  <!-- ================================================================ -->
  <!-- === Gallery                                                  === -->
  <!-- ================================================================ -->


  <xsl:template match="g:blog-pics">
    <xsl:variable name="id" 
		  select="f:if-absent(@id, generate-id())"/>
    <xsl:variable name="behaviour" 
		  select="f:if-absent(@behaviour, $g:blog-pics-behaviour)"/>
    <xsl:variable name="class" 
		  select="f:space-list-collapse($g:blog-pics-class, @class)"/>

    <span id="{$id}" data-gallery-behaviour="{$behaviour}"
	             data-gallery-group="global">
      <!-- Attibutes -->
      <xsl:copy-of select="@style|@title|@lang"/>
      <xsl:if test="normalize-space($class)">
	<xsl:attribute name="class">
	  <xsl:value-of select="$class"/>
	</xsl:attribute>
      </xsl:if>
      <xsl:if test="normalize-space(@grp)">
	<xsl:attribute name="data-gallery-group">
	  <xsl:value-of select="@grp"/>
	</xsl:attribute>
      </xsl:if>

      <xsl:apply-templates select="g:thumb" mode="inline">
	<xsl:with-param name="src-thumb"   select="':medium'"   />
	<xsl:with-param name="href-is"     select="'src'"       />
	<xsl:with-param name="caption-use" select="'title'"     />
	<xsl:with-param name="microdata"   select="'Photograph'"/>
      </xsl:apply-templates>
    </span>

    <xsl:call-template name="g:behaviour-init">
      <xsl:with-param name="behaviour" select="$g:blog-pics-behaviour"/>
      <xsl:with-param name="id"        select="$id"                   />
    </xsl:call-template>
  </xsl:template>



  <xsl:template match="g:picture-wall" name="g:picture-wall">
    <!-- Parameters -->
    <xsl:param name="id" 
	       select="f:if-absent(@id, generate-id())"/>
    <xsl:param name="behaviour" 
	       select="f:if-absent(@behaviour, $g:picture-wall-behaviour)"/>
    <xsl:param name="class" 
	       select="f:space-list-collapse($g:picture-wall-class, @class)"/>
    <xsl:param name="gallery"
	       select="."/>
    <xsl:param name="src-thumb"  
               select="$gallery/@src-thumb"/>
    <xsl:param name="grp"
	       select="@grp"/>
    <xsl:param name="width"
	       select="f:if-absent(@width, $g:picture-wall-width)"/>

    <!-- Main layout -->
    <table id="{$id}" data-gallery-behaviour="{$behaviour}">
      <!-- Attibutes -->
      <xsl:copy-of select="@style|@title|@lang"/>
      <xsl:if test="normalize-space($class)">
	<xsl:attribute name="class">
	  <xsl:value-of select="$class"/>
	</xsl:attribute>
      </xsl:if>
      <xsl:if test="normalize-space($grp)">
	<xsl:attribute name="data-gallery-group">
	  <xsl:value-of select="$grp"/>
	</xsl:attribute>
      </xsl:if>

      <tr>
	<xsl:for-each select="g:thumb[position() &lt;= $width]">
	  <td>
	    <img src="{g:thumb-src (@src,  $src-thumb)}">
	      <xsl:copy-of select="@title"/>
	    </img>
	  </td>
	</xsl:for-each>
      </tr>
    </table>

    <!-- Initialisation -->
    <xsl:call-template name="g:behaviour-init">
      <xsl:with-param name="behaviour" select="$behaviour"/>
      <xsl:with-param name="id"        select="$id"       />
    </xsl:call-template>
  </xsl:template>


  <!-- Customized inlined gallery -->
  <xsl:template match="g:strip">
    <xsl:variable name="id" 
		  select="f:if-absent(@id, generate-id())"/>
    <xsl:variable name="behaviour" 
		  select="f:if-absent(@behaviour, $g:strip-behaviour)"/>
    <xsl:variable name="class" 
		  select="f:space-list-collapse($g:strip-class, @class)"/>

    <span id="{$id}" data-gallery-behaviour="{$behaviour}">
      <!-- Attibutes -->
      <xsl:copy-of select="@style|@title|@lang"/> 
      <xsl:if test="normalize-space($class)">
	<xsl:attribute name="class">
	  <xsl:value-of select="$class"/>
	</xsl:attribute>
      </xsl:if>
      <xsl:if test="normalize-space(@grp)">
	<xsl:attribute name="data-gallery-group">
	  <xsl:value-of select="@grp"/>
	</xsl:attribute>
      </xsl:if>

      <xsl:apply-templates select="g:thumb" mode="inline">
	<xsl:with-param name="src-thumb"   select="'thumb/a'"   />
	<xsl:with-param name="href-is"     select="'src'"   />
	<xsl:with-param name="caption-use" select="'title'"     />
	<xsl:with-param name="microdata"   select="'Photograph'"/>
      </xsl:apply-templates>
    </span>
    <xsl:call-template name="g:behaviour-init">
      <xsl:with-param name="behaviour" select="$behaviour"/>
      <xsl:with-param name="id"        select="$id"       />
    </xsl:call-template>
  </xsl:template>


  <xsl:template match="g:gallery">
    <xsl:variable name="id" 
		  select="f:if-absent(@id, generate-id())"/>
    <xsl:variable name="behaviour" 
		  select="f:if-absent(@behaviour, $g:gallery-behaviour)"/>
    <xsl:variable name="class" 
		  select="f:space-list-collapse($g:gallery-class, @class)"/>
    <xsl:variable name="microdata">
      <xsl:choose>
	<xsl:when test="@type = 'index' and not(@content)">
	  <xsl:text>-</xsl:text>
	</xsl:when>
	<xsl:when test="@content">
	  <xsl:value-of select="@content"/>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:value-of select="$g:gallery-microdata"/>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <div id="{$id}" data-gallery-behaviour="{$behaviour}">
      <!-- Attibutes -->
      <xsl:copy-of select="@style|@title|@lang"/> 
      <xsl:if test="normalize-space($class)">
	<xsl:attribute name="class">
	  <xsl:value-of select="$class"/>
	</xsl:attribute>
      </xsl:if>
      <xsl:if test="normalize-space(@grp)">
	<xsl:attribute name="data-gallery-group">
	  <xsl:value-of select="@grp"/>
	</xsl:attribute>
      </xsl:if>
      <xsl:if test="normalize-space(@options)">
	<xsl:attribute name="data-gallery-options">
	  <xsl:value-of select="@options"/>
	</xsl:attribute>
      </xsl:if>

      <xsl:for-each select="*">
	<xsl:choose>
	  <xsl:when test="self::g:thumb">
	    <xsl:apply-templates select=".">
	      <xsl:with-param name="microdata" select="$microdata"/>
	    </xsl:apply-templates>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:apply-templates select="."/>
	  </xsl:otherwise>
	</xsl:choose>
      </xsl:for-each>

      <div style="clear: both;"/>
    </div>
    <xsl:call-template name="g:behaviour-init">
      <xsl:with-param name="behaviour" select="$behaviour"/>
      <xsl:with-param name="id"        select="$id"       />
    </xsl:call-template>
  </xsl:template>






  <xsl:template match="g:gallery2">
    <xsl:variable name="id" 
		  select="f:if-absent(@id, generate-id())"/>
    <xsl:variable name="behaviour" 
		  select="f:if-absent(@behaviour, $g:gallery-behaviour)"/>
    <xsl:variable name="class" 
		  select="f:space-list-collapse('gallery2', @class)"/>
    <xsl:variable name="microdata">
      <xsl:choose>
	<xsl:when test="@type = 'index' and not(@content)">
	  <xsl:text>-</xsl:text>
	</xsl:when>
	<xsl:when test="@content">
	  <xsl:value-of select="@content"/>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:value-of select="$g:gallery-microdata"/>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <div id="{$id}" data-gallery-behaviour="{$behaviour}">
      <!-- Attibutes -->
      <xsl:copy-of select="@style|@title|@lang"/> 
      <xsl:if test="normalize-space($class)">
	<xsl:attribute name="class">
	  <xsl:value-of select="$class"/>
	</xsl:attribute>
      </xsl:if>
      <xsl:if test="normalize-space(@grp)">
	<xsl:attribute name="data-gallery-group">
	  <xsl:value-of select="@grp"/>
	</xsl:attribute>
      </xsl:if>
      <xsl:if test="normalize-space(@options)">
	<xsl:attribute name="data-gallery-options">
	  <xsl:value-of select="@options"/>
	</xsl:attribute>
      </xsl:if>

      <xsl:for-each select="*">
	<xsl:choose>
	  <xsl:when test="self::g:thumb">
	    <xsl:apply-templates select="." mode="gallery2">
	      <xsl:with-param name="microdata" select="$microdata"/>
	    </xsl:apply-templates>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:apply-templates select="."/>
	  </xsl:otherwise>
	</xsl:choose>
      </xsl:for-each>
      <div style="clear: both;"/>
    </div>
    <xsl:call-template name="g:behaviour-init">
      <xsl:with-param name="behaviour" select="$behaviour"/>
      <xsl:with-param name="id"        select="$id"       />
    </xsl:call-template>
  </xsl:template>







  <!-- === -->


  <xsl:template name="g:behaviour-init">
    <xsl:param name="behaviour"/>
    <xsl:param name="id"       />
<!--
    <script type="text/javascript">
      (function(undefined) {
        if (typeof Gallery == 'undefined') return;
        var func = Gallery.behaviour['<xsl:value-of select="$behaviour"/>'];
	if (func) func('<xsl:value-of select="$id"/>');
      })();
    </script>
-->
  </xsl:template>


  <xsl:template name="g:gg-init">
    <xsl:variable name="class" 
		  select="f:space-list-collapse($g:gallery-class, @class)"/>

    <xsl:copy-of select="@style|@title|@lang"/> 
    <xsl:if test="normalize-space($class)">
      <xsl:attribute name="class">
	<xsl:value-of select="$class"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:if test="normalize-space(@grp)">
      <xsl:attribute name="data-gallery-group">
	<xsl:value-of select="@grp"/>
      </xsl:attribute>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
