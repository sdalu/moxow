<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: shopping-cart.xsl 3186 2012-11-01 19:41:47Z sdalu $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:m  ="http://www.moxow.org/main"
  xmlns:f  ="http://www.moxow.org/function"
  exclude-result-prefixes="xsl m f"
>


  <!-- ================================================================ -->
  <!-- === Localization                                             === -->
  <!-- ================================================================ -->

  <xsl:template name="m:sc-l10n">
    <xsl:param name="msg"/>

    <xsl:choose>
      <!-- Cart helper -->
      <xsl:when test="$msg = 'cart-howto'">
      <xsl:choose>
	<xsl:when test="$lang = 'fr'">
          <div style="border-bottom: 1px solid white;
	   	      font-size: x-large;
		      text-align: center;">Passer commande</div>
	  <div class="cart-howto-content">
	    <ol>
	      <li>Choisissez les options de <a href="#item-type" 
	        class="cart-choose-print-link">tirage</a>
		qui seront appliquées par défaut</li>
	      <li>Dans la <a href="#gallery"
	        class="cart-choose-picture-link">galerie</a>, lorsque la souris
		est sur une des vignettes vous pouvez:
		<ul>
		  <li>ajouter des tirages avec les options par défaut en
		    cliquant sur 
		      <img src="/images/common/icon/16x16/gtk-add.png"
			   alt="+"/></li>
		    <li>ajouter des tirages <em>en choisissant</em> les 
		    options, pour cela utilisez le menu qui apparaît
		    lorsque la souris est sur
 		      <img src="/images/common/icon/16x16/gtk-add.png" 
			   alt="+"/></li>
		    <li>supprimer les tirages basés sur une même photo en
		    cliquant sur 
		      <img src="/images/common/icon/16x16/checked.png" 
			   alt="√"/>/
		      <img src="/images/common/icon/16x16/cancel.png" 
			   alt="x"/></li>
		    <li>avoir des compléments d'information en allant sur
		      <img src="/images/common/icon/16x16/gtk-info.png" 
			   alt="i"/></li>
		    <li>zoomer en cliquant sur l'image</li>
		</ul>
	      </li>
	      <li>Depuis le panier, modifiez les quantités
		<img src="/images/common/icon/16x16/go-up.png" alt="◀"/>/
		<img src="/images/common/icon/16x16/go-down.png" alt="▶"/>
		ou supprimez
		<img src="/images/common/icon/16x16/gtk-delete.png" alt="x"/>
		le tirage</li>
	      <li><img style="float: right;"
		       src="/images/common/cart/Shopcart-Apply_32.png"
		       alt="panier"/>Validez le panier en cliquant sur
		le caddie</li>
	      <li>Saisissez les informations demandées par le service de 
		paiement en ligne</li>
	      <li>Réceptionnez vos tirages</li>
	    </ol>
	  </div>
	</xsl:when>
	<xsl:otherwise>
          <div style="border-bottom: 1px solid white;
	   	      font-size: x-large;
		      text-align: center;">Submitting order</div>
	  <div class="cart-howto-content">
	    <ol>
	      <li>Choose the <a href="#item-type"
	        class="cart-choose-print-link">print</a> options to
  	        be applied by default</li>
	      <li>In the <a href="#gallery" 
	        class="cart-choose-picture-link">gallery</a>, when the mouse
	        is over a thumbnail, you can:
		<ul>
		  <li>add prints with the default options by clicking on
		    <img src="/images/common/icon/16x16/gtk-add.png"
			 alt="+"/></li>
		    <li>add prints <em>by choosing</em> the options,
		    for that, use the menu that appears when the 
		    mouse is on
		      <img src="/images/common/icon/16x16/gtk-add.png" 
			   alt="+"/></li>
		    <li>remove prints based on the same photo by clicking on
		      <img src="/images/common/icon/16x16/checked.png" 
			   alt="√"/>/
		      <img src="/images/common/icon/16x16/cancel.png" 
			   alt="x"/></li>
		    <li>have additional information by going on
		      <img src="/images/common/icon/16x16/gtk-info.png" 
			   alt="i"/></li>
		    <li>zoom in by clicking on the image</li>
		</ul>
	      </li>
	      <li>From the basket, change quantities
		<img src="/images/common/icon/16x16/go-up.png" alt="◀"/>/
		<img src="/images/common/icon/16x16/go-down.png" alt="▶"/>
		or remove
		<img src="/images/common/icon/16x16/gtk-delete.png" alt="x"/>
		print</li>
	      <li><img style="float: right;"
		       src="/images/common/cart/Shopcart-Apply_32.png"
		       alt="panier"/>Validate the shopping basket by
	        clicking on the cart</li>
	      <li>Enter the information requested by the online 
	        payment service</li>
	      <li>Receive your prints</li>
	    </ol>
	  </div>	  
	</xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    </xsl:choose>
  </xsl:template>




  <xsl:template match="m:sc-checkout">
    <table class="cart-checkout-standalone"><tr>
	<td><div class="cart-checkout"></div></td>
	<td><div class="cart-checkout-validate" role="button" 
		 title="{@validate}">
	  <span><xsl:value-of select="@validate"/></span>
	</div></td>
	<td><div class="cart-checkout-empty"    role="button"
		 title="{@empty}">
	  <span><xsl:value-of select="@empty"/></span>
	</div></td>
    </tr></table>
  </xsl:template>


  <xsl:template match="m:sc-paypal" name="m:sc-paypal">
    <a href="javascript:window.open('https://www.paypal.com/fr/cgi-bin/webscr?cmd=xpt/Marketing/popup/OLCWhatIsPayPal-outside','olcwhatispaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=600, height=450');">
      <xsl:copy-of select="@style"/>
      <img  src="/images/common/cart/paypal2.png" alt="PayPal"/>
    </a>
  </xsl:template>


  <xsl:template match="m:sc-cart">
    <div class="cart-basket">
      <xsl:copy-of select="@id"/>
      <div class="cart-basket-items">
	<div title="{@empty}" style="display: none;"
	     class="cart-checkout-empty" role="button">
	  <span><xsl:value-of select="@empty"/></span>
	</div>
        <div class="cart-howto">
	  <xsl:call-template name="m:sc-l10n">
	    <xsl:with-param name="msg">cart-howto</xsl:with-param>
	  </xsl:call-template>
	</div>
      </div>
      <div class="cart-basket-checkout">
	<table style="width: 100%;">
	  <tr><td>
	    <div class="cart-checkout"/></td>
              <td style="width: 30px;"> 
	    <div title="{@validate}"
 		 class="cart-checkout-validate" role="button">
	      <span><xsl:value-of select="@validate"/></span>
	    </div>
          </td></tr>
	</table>
      </div> 
    </div>
  </xsl:template>


  <xsl:template match="m:sc-default">
    <div class="cart-default {@class}">
      <xsl:copy-of select="@id|@style"/>

      <form action="">
	<xsl:for-each select="m:sc-choice">
	  <div class="cart-default-choice">	  
	    <input type="radio"  id="{generate-id(.)}"
		   name="format" value="{@value}">
	      <xsl:if test="@checked">
		<xsl:attribute name="checked">checked</xsl:attribute>
	      </xsl:if>
	    </input>
	    <label for="{generate-id(.)}">
	      <xsl:if test="@prefix">
		<xsl:value-of select="@prefix"/>
	      </xsl:if>
	      <tt>
		<xsl:value-of select="f:if-absent(@text, @value)"/>
		<xsl:text> = </xsl:text>
		<xsl:value-of select="@price"/>
		<xsl:text>€</xsl:text>
	      </tt>
	    </label>
	  </div>
	</xsl:for-each>
	<div style="clear: left;"></div>
      </form>
    </div>
  </xsl:template>

</xsl:stylesheet>
