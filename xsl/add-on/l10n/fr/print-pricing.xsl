<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: print-pricing.xsl 2945 2012-06-08 23:05:08Z sdalu $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:l10n="http://www.moxow.org/l10n"
  exclude-result-prefixes="xsl l10n"
>

  <!-- Media -->
  <xsl:template name="l10n:print-pricing--photo">
    Photo
  </xsl:template>
  <xsl:template name="l10n:print-pricing--photo--desc">
    <xsl:text>Fuji Crystal Archive ou Kodak Endura II</xsl:text>
  </xsl:template>
  <xsl:template name="l10n:print-pricing--fine-art">
    Fine art
  </xsl:template>
  <xsl:template name="l10n:print-pricing--fine-art--desc">
    <xsl:text>Baryté Fine Art Hahnemühle et encres HP vivera</xsl:text>
  </xsl:template>
  <xsl:template name="l10n:print-pricing--dibond">
    Dibond
  </xsl:template>
  <xsl:template name="l10n:print-pricing--dibond--desc">
    <xsl:text>Baryté Fine Art Hahnemühle et encres HP vivera contrecollé sur dibond</xsl:text>
  </xsl:template>
  <xsl:template name="l10n:print-pricing--canvas">
    Canvas
  </xsl:template>
  <xsl:template name="l10n:print-pricing--canvas--desc">
    <xsl:text>Canvas Fine Art Ilford et cadre en bois 20x45</xsl:text>
  </xsl:template>
  <xsl:template name="l10n:print-pricing--acrylic">
    Verre acrylique
  </xsl:template>
  <xsl:template name="l10n:print-pricing--acrylic--desc">
    <xsl:text>Verre acrylique et encres HP contrecollé sur dibond</xsl:text>
  </xsl:template>

  <!-- Options -->
  <xsl:template name="l10n:print-pricing--artist-print">
    Tirage d'artiste
  </xsl:template>
  <xsl:template name="l10n:print-pricing--artist-print--desc">
    Le tirage est personnellement inspecté, signé et numéroté 
    garantissant ainsi qu'une qualité de type musée est expédiée
  </xsl:template>
  <xsl:template name="l10n:print-pricing--certificat">
    Certificat
  </xsl:template>
  <xsl:template name="l10n:print-pricing--certificat--desc">
    Certificat d'authenticité <a rel="external" 
       href="http://www.arttrustonline.com/en/Products-Services/Introduction"
       >ArtTrust</a>
  </xsl:template>

  <!-- headers words -->
  <xsl:template name="l10n:print-pricing--size">
    Taille
  </xsl:template>
  <xsl:template name="l10n:print-pricing--small">
    Petit
  </xsl:template>
  <xsl:template name="l10n:print-pricing--medium">
    Moyen
  </xsl:template>
  <xsl:template name="l10n:print-pricing--large">
    Large
  </xsl:template>
  <xsl:template name="l10n:print-pricing--ratio">
    Ratio
  </xsl:template>
  <xsl:template name="l10n:print-pricing--option">
    Option
  </xsl:template>
  <xsl:template name="l10n:print-pricing--description">
    Description
  </xsl:template>
  <xsl:template name="l10n:print-pricing--price">
    Prix
  </xsl:template>

  <!-- selector -->
  <xsl:template name="l10n:print-pricing--selector--print">
    Tirage
  </xsl:template>
  <xsl:template name="l10n:print-pricing--selector--option">
    Option
  </xsl:template>
  <xsl:template name="l10n:print-pricing--selector--by">
    par
  </xsl:template>
  <xsl:template name="l10n:print-pricing--selector--for">
    pour
  </xsl:template>

  <!-- print & options -->
  <xsl:template name="l10n:print-pricing--print--title">
    Taille et support des tirages
  </xsl:template>
  <xsl:template name="l10n:print-pricing--options--title">
    Options de collection
  </xsl:template>

  <!-- pricing explained -->
  <xsl:template name="l10n:print-pricing-explained">
    <p>En fonction de votre budget nous avons souligné
    quelques options pour vous aider dans votre choix:</p>
    <ul>
      <li><span class="print-pricing-low-budget">petit</span>:
      Tirage photo en petite et moyenne dimension</li>
      <li><span class="print-pricing-medium-budget">moyen</span>:
      Fine art sur dibond ou canvas en dimension moyenne</li>
      <li><span class="print-pricing-high-budget">grand</span>:
      Tirage d'artiste fine art sur dibond, canvas, ou verre acrylique
      en grande dimension</li>
    </ul>
  </xsl:template>

</xsl:stylesheet>

