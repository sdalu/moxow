<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: print-pricing.xsl 2915 2012-06-05 20:27:36Z sdalu $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:l10n="http://www.moxow.org/l10n"
  exclude-result-prefixes="xsl l10n"
>

  <!-- Media -->
  <xsl:template name="l10n:print-pricing--photo">
    Photo
  </xsl:template>
  <xsl:template name="l10n:print-pricing--photo--desc">
    <xsl:text>Fuji Crystal Archive or Kodak Endura II</xsl:text>
  </xsl:template>
  <xsl:template name="l10n:print-pricing--fine-art">
    Fine art
  </xsl:template>
  <xsl:template name="l10n:print-pricing--fine-art--desc">
    <xsl:text>Fine Art Baryta Hahnemühle and HP vivera inks</xsl:text>
  </xsl:template>
  <xsl:template name="l10n:print-pricing--dibond">
    Dibond
  </xsl:template>
  <xsl:template name="l10n:print-pricing--dibond--desc">
    <xsl:text>Fine Art Baryta Hahnemühle and HP vivera inks on dibond laminated support</xsl:text>
  </xsl:template>
  <xsl:template name="l10n:print-pricing--canvas">
    Canvas
  </xsl:template>
  <xsl:template name="l10n:print-pricing--canvas--desc">
    <xsl:text>Fine Art Ilford Canvas and 20x45 wood frame</xsl:text>
  </xsl:template>
  <xsl:template name="l10n:print-pricing--acrylic">
    Acrylic glass
  </xsl:template>
  <xsl:template name="l10n:print-pricing--acrylic--desc">
    <xsl:text>Acrylic glass with HP inks on dibond laminated support</xsl:text>
  </xsl:template>

  <!-- Options -->
  <xsl:template name="l10n:print-pricing--artist-print">
    Artist proof print
  </xsl:template>
  <xsl:template name="l10n:print-pricing--artist-print--desc">
    Print is personally inspected, signed and numbered to ensure museum
    quality work gets shipped
  </xsl:template>
  <xsl:template name="l10n:print-pricing--certificat">
    Certificat
  </xsl:template>
  <xsl:template name="l10n:print-pricing--certificat--desc">
    <a rel="external" 
       href="http://www.arttrustonline.com/en/Products-Services/Introduction"
       >ArtTrust</a> authenticity certificat
  </xsl:template>

  <!-- headers words -->
  <xsl:template name="l10n:print-pricing--size">
    Size
  </xsl:template>
  <xsl:template name="l10n:print-pricing--small">
    Small
  </xsl:template>
  <xsl:template name="l10n:print-pricing--medium">
    Medium
  </xsl:template>
  <xsl:template name="l10n:print-pricing--large">
    Large
  </xsl:template>
  <xsl:template name="l10n:print-pricing--ratio">
    Ratio
  </xsl:template>
  <xsl:template name="l10n:print-pricing--option">
    Option
  </xsl:template>
  <xsl:template name="l10n:print-pricing--description">
    Description
  </xsl:template>
  <xsl:template name="l10n:print-pricing--price">
    Price
  </xsl:template>

  <!-- selector -->
  <xsl:template name="l10n:print-pricing--selector--print">
    Print
  </xsl:template>
  <xsl:template name="l10n:print-pricing--selector--option">
    Option
  </xsl:template>
  <xsl:template name="l10n:print-pricing--selector--by">
    by
  </xsl:template>
  <xsl:template name="l10n:print-pricing--selector--for">
    for
  </xsl:template>

  <!-- print & options -->
  <xsl:template name="l10n:print-pricing--print--title">
    Print size and medium
  </xsl:template>
  <xsl:template name="l10n:print-pricing--options--title">
    Collection options
  </xsl:template>

  <!-- pricing explained -->
  <xsl:template name="l10n:print-pricing-explained">
    <p>According to your budget we have highlightled some options
    to help you choose:</p>
    <ul>
      <li><span class="print-pricing-low-budget">tight</span>:
      Photo print in small or medium size</li>
      <li><span class="print-pricing-medium-budget">medium</span>:
      Fine art on dibond or canvas in medium size</li>
      <li><span class="print-pricing-high-budget">large</span>:
      Fine art on dibond, canvas, or acrylic glass in large size
      with artist proof print</li>
    </ul>
  </xsl:template>

</xsl:stylesheet>

