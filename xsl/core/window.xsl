<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: helper.xsl 1433 2010-09-30 09:25:40Z sdalu $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl ="http://www.w3.org/1999/XSL/Transform"
  xmlns:func="http://exslt.org/functions"
  xmlns:m   ="http://www.moxow.org/main"
  xmlns:w   ="http://www.moxow.org/window"

  extension-element-prefixes="func"
  exclude-result-prefixes="xsl func m w"
>

<!-- Requires:
     VAR : m:content
     FUNC: m:resource-path
-->


  <!-- ================================================================ -->
  <!-- === Resources                                                === -->
  <!-- ================================================================ -->

  <!-- Stylesheet -->
  <xsl:variable  name="w:css-path" select="m:resource-path('style/')"/>
  <func:function name="w:css-path">
    <xsl:param name="file"/>
    <xsl:param name="path" select="$w:css-path"/>
    <func:result select="concat($path, $file)"/>
  </func:function>



  <!-- ================================================================ -->
  <!-- === Init                                                     === -->
  <!-- ================================================================ -->

  <!-- Detection -->
  <xsl:variable  name="w:init-content" select="$m:content"/>
  <func:function name="w:init-required">
    <xsl:param name="nodes" select="$w:init-content"/>
    <func:result select="$nodes//w:*[1]"/>
  </func:function>

  <!-- Initialisation -->
  <xsl:template name="w:init-for-head">
    <link rel="stylesheet" type="text/css"
	  href="{w:css-path('moxow-window.css')}"/>
  </xsl:template>

  <xsl:template name="w:init-do-head">
    <xsl:if test="w:init-required()">
      <xsl:call-template name="w:init-for-head"/>
    </xsl:if>
  </xsl:template>



  <!--                                                              -->
  <!-- ============================================================ -->
  <xsl:template match="w:radio">
    <label class="w-button {@class}">
      <input type="radio" value="{@value}">
	<xsl:copy-of select="parent::w:set/@onchange|@onchange"/>
	<xsl:copy-of select="parent::w:set/@name    |@name"/>
	<xsl:copy-of select="@checked"/>
      </input>
      <xsl:if test="@icon">
	<xsl:if test="@icon-place != 'right'">
	  <img src="{@icon}">
	    <xsl:if test="@icon-place = 'only' and @title" >
	      <xsl:attribute name="title">
		<xsl:value-of select="@title"/>
	      </xsl:attribute>
	    </xsl:if>
	  </img>
	</xsl:if>
	<xsl:if test="@icon-place != 'only'">
	  <xsl:value-of select="@title"/>
	</xsl:if>
	<xsl:if test="@icon-place = 'right'">
	  <img src="{@icon}"/>
	</xsl:if>
      </xsl:if>
    </label>
  </xsl:template>


  <!--                                                              -->
  <!-- ============================================================ -->
  <xsl:template match="w:set">
    <xsl:choose>
      <xsl:when test="@id|@class|@style">
	<div>
	  <xsl:copy-of select="@id|@class|@style"/>
	  <xsl:apply-templates/>
	</div>
      </xsl:when>
      <xsl:otherwise>
	<xsl:apply-templates/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
