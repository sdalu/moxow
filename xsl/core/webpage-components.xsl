<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: webpage-components.xsl 3293 2013-04-08 22:02:07Z sdalu $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl ="http://www.w3.org/1999/XSL/Transform"
  xmlns:f   ="http://www.moxow.org/function"
  xmlns:m   ="http://www.moxow.org/main"
  exclude-result-prefixes="xsl f m"
>


  <!-- ================================================================ -->
  <!-- == Logo                                                       == -->
  <!-- ================================================================ -->

  <xsl:template name="m:webpage-logo">
    <xsl:if test="$m:website/@logo">
      <a id="mxw-logo" class="mxw-logo" href="/">
	<img src="{$m:website/@logo}" alt="{$m:website/@name}"/>
      </a>
    </xsl:if>
  </xsl:template>


  <!-- ================================================================ -->
  <!-- == Waypoints                                                  == -->
  <!-- ================================================================ -->

  <xsl:template name="m:webpage-waypoints">
    <xsl:for-each select="($m:defaults|$m:pagedesc)/m:waypoint">
      <xsl:apply-templates select="m:lang(node())"/>
      <script type="text/javascript" id="{generate-id()}">
      (function() {
          var jId   = $('#<xsl:value-of select="generate-id()"/>').prev();
	  var offset= '<xsl:value-of select="@offset"/>';
	  <![CDATA[
          if (jId && $.waypoints) {
 	      jId.on('click', function() {
	          $(document.body).scrollTo(0, 'fast');
	      });
	      $('#mxw-content').waypoint(function(direction) {
	          jId[direction === "up" ? 'hide' : 'show']();
	      }, { offset: offset });
	  }
	  ]]> 
      })();
      </script>
    </xsl:for-each>
  </xsl:template>


  <!-- ================================================================ -->
  <!-- == Menu                                                       == -->
  <!-- ================================================================ -->

  <xsl:template name="m:webpage-menu">
    <xsl:variable name="menu" select="$m:pagedesc/m:menu"/>
    <xsl:if test="$menu">
      <nav id="mxw-nav-menu" class="mxw-nav-menu">
	<xsl:apply-templates select="$m:menu">
	  <xsl:with-param name="selected-id" select="$menu/@selected"/>
	</xsl:apply-templates>
      </nav>
    </xsl:if>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- == Breadcrumbs                                                == -->
  <!-- ================================================================ -->

  <xsl:template name="m:webpage-breadcrumbs">
    <xsl:variable name="path" select="$m:pagedesc/m:path"/>
    <xsl:if test="$path">
      <nav id="mxw-nav-breadcrumbs" class="mxw-nav-breadcrumbs">
	<xsl:apply-templates select="$path"/>
      </nav>
    </xsl:if>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- == Lang switcher                                              == -->
  <!-- ================================================================ -->

  <xsl:template name="m:webpage-lang-switcher">
    <xsl:variable name="lang" select="$m:pagedesc/m:lang"/>

    <!-- Lang navigation -->
    <xsl:if test="$m:lang">
      <nav id="mxw-nav-lang-switcher" class="mxw-nav-lang-switcher">
	<ul class="mxw-lang-switcher">
	  <xsl:apply-templates select="lang"/>
	</ul>
      </nav>
    </xsl:if>

    <!-- Switching to another language using cookie if:   -->
    <!--  - language switching by cookie is enabled       -->
    <!--  - page is multilingual                          -->
    <!--  - we are not in a localized subdomain           -->
    <xsl:if test="$m:i18n/@lang-cookie and
		  $m:pagedesc/m:lang and
		  substring-before(substring-after($server-name, '.'), '.') != $expect-lang">
      <script type="text/javascript">
	MoXoW.lang_switch_by_cookie('mxw-nav-lang-switcher');
      </script>
    </xsl:if>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- == Custom box                                                 == -->
  <!-- ================================================================ -->

  <xsl:template name="m:webpage-custom-box">
      <xsl:apply-templates select="$m:moxow/m:custom-box"/>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- == Error                                                      == -->
  <!-- ================================================================ -->

  <xsl:template name="m:webpage-error">
    <xsl:variable name="error" select="$m:moxow/m:error"/>

    <xsl:if test="$error">
      <xsl:comment>google_ad_section_start(weight=ignore)</xsl:comment>
      <div class="mxw-error robots-nocontent {$error/@class}">
	<xsl:copy-of select="$error/@style"/>
	<xsl:apply-templates select="$error/node()"/>
      </div>
      <xsl:comment>google_ad_section_end</xsl:comment>
    </xsl:if>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- == Title                                                      == -->
  <!-- ================================================================ -->

  <xsl:template name="m:title">
    <xsl:variable name="title" select="$m:pagedesc/m:title"/> 

     <xsl:choose>
       <xsl:when test="$title/@from = 'menu'">
	 <xsl:variable name="selected"
		       select="$m:pagedesc/m:menu/@selected"/>
	 <xsl:variable name="entry"
		       select="m:lang($m:menu/m:entry[@id=$selected]/m:title)"/>
	 <xsl:choose>
	   <xsl:when test="$entry/@text">
	     <xsl:value-of select="$entry/@text"/>
	   </xsl:when>
	   <xsl:otherwise>
	     <xsl:value-of select="$entry"/>
	   </xsl:otherwise>
	 </xsl:choose>
       </xsl:when>
       <xsl:otherwise>
	 <xsl:value-of select="$title"/>
       </xsl:otherwise>
     </xsl:choose>
  </xsl:template>
  
  <!-- Title to be used in browser -->
  <xsl:template name="m:description-browser-title">
    <xsl:variable name="title" select="$m:pagedesc/m:title"/> 

    <xsl:if test="$title">
      <title>
	<xsl:if test="normalize-space($title/@prefix)">
	  <xsl:value-of select="$title/@prefix"/>
	  <xsl:text>: </xsl:text>
	</xsl:if>
	<xsl:call-template name="m:title"/>
	<xsl:choose>
	  <xsl:when test="$title/@suffix">
	    <xsl:value-of select="$title/@suffix"/>
	  </xsl:when>
	  <xsl:when test="$m:pagedesc-dflt/m:title/@suffix">
	    <xsl:text> | </xsl:text>
	    <xsl:value-of select="m:lang($m:website)/@name"/>
	  </xsl:when>
	</xsl:choose>
      </title>
    </xsl:if>
  </xsl:template>

  <!-- -->
  <xsl:template name="m:webpage-title">
    <xsl:variable name="title" select="$m:pagedesc/m:title"/> 
    <xsl:if test="$title and
		  (not($title/@main) or $title/@main != 'false')">
      <xsl:comment>google_ad_section_start</xsl:comment>
      <h1 class="maintitle">
	<xsl:copy-of select="$title/@style"/>
	<xsl:call-template name="m:title"/>
      </h1>
      <xsl:comment>google_ad_section_end</xsl:comment>
    </xsl:if>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- == Content                                                    == -->
  <!-- ================================================================ -->

  <xsl:template name="m:webpage-content">
    <xsl:call-template name="m:layout-content-defaults"/>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- == Page information                                           == -->
  <!-- ================================================================ -->

  <xsl:template name="m:webpage-pageinfo">
    <xsl:variable name="timestamp"  select="$m:pagedesc/m:timestamp"/>

    <ul id="mxw-footer-pageinfo" class="mxw-footer-pageinfo">
      <!-- last modification -->
      <xsl:if test="$timestamp/@when">
	<xsl:variable name="datetime">
	  <xsl:choose>
	    <xsl:when test="$timestamp/@when = 'lastmod'">
	      <xsl:value-of select="$lastmod"/>
	    </xsl:when>
	    <xsl:when test="$timestamp/@when = 'now'">
	      <xsl:value-of select="$now"/>
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:value-of select="$timestamp/@when"/>
	    </xsl:otherwise>
	  </xsl:choose>
	</xsl:variable>

	<li class="lastmod">
	  <xsl:choose>
	    <xsl:when test="$timestamp/@generated = 'true'">
	      <xsl:text>Last generated: </xsl:text>
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:text>Last modified: </xsl:text>
	    </xsl:otherwise>
	  </xsl:choose>
	  
	  <xsl:value-of select="f:datetime-to-string($datetime)"/>
	</li>
      </xsl:if>
	
      <!-- validator -->
      <xsl:if test="$m:feature/@validator = 'true'">
	<li class="validator">Validator: 
	  <a href="http://validator.w3.org/check/referer">html</a> / 
	  <a href="http://jigsaw.w3.org/css-validator/check/referer">css</a>
	</li>
      </xsl:if>
      
      <!-- website -->
      <xsl:if test="normalize-space($m:website/@href)">
	<li class="website">
	  <a href="{$m:website/@href}">
	    <xsl:value-of select="$m:website/@href"/></a>
	</li>
      </xsl:if>

      <!-- webmaster -->
      <xsl:if test="normalize-space($m:webmaster/@name)">
	<li class="address">
	  <xsl:choose>
	    <xsl:when test="normalize-space($m:webmaster/@href)">
	      <a href="{$m:webmaster/@href}">
		<xsl:value-of select="$m:webmaster/@name"/>
	      </a>
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:value-of select="$m:webmaster/@name"/>
	    </xsl:otherwise>
	  </xsl:choose>
	</li>
      </xsl:if>
    </ul>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- == Page other-information                                     == -->
  <!-- ================================================================ -->

  <xsl:template name="m:webpage-otherinfo">
  </xsl:template>



  <!-- ================================================================ -->
  <!-- == Comments                                                   == -->
  <!-- ================================================================ -->

  <xsl:template name="m:webpage-comments-identifier"/>
  <xsl:template name="m:webpage-comments-renderer"/>

  <xsl:template name="m:webpage-comments">
    <xsl:param name="site-id" select="$m:defaults/m:comments/@site-id"/>

    <xsl:if test="normalize-space($site-id)">
      <xsl:variable name="identifier">
	<xsl:choose>
	  <xsl:when test="normalize-space($m:pagedesc/m:comments/@id) = '+'">
	  </xsl:when>
	  <xsl:when test="normalize-space($m:pagedesc/m:comments/@id)">
	    <xsl:value-of select="$m:pagedesc/m:comments/@id"/>
	  </xsl:when>
	  <xsl:when test="$m:defaults/m:comments/@default = 'off'">
	    <xsl:text>-</xsl:text>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:call-template name="m:webpage-comments-identifier"/>
	  </xsl:otherwise>
	</xsl:choose>
      </xsl:variable>
      
      <xsl:if test="substring($identifier,1,1) != '-'">
	<div id="mxw-page-comments">
	  <xsl:call-template name="m:webpage-comments-renderer">
	    <xsl:with-param name="site-id"    select="$site-id"    />
	    <xsl:with-param name="lang"       select="$m:lang"     />
	    <xsl:with-param name="identifier" select="$identifier" />
	  </xsl:call-template>
	</div>
      </xsl:if>
    </xsl:if>
  </xsl:template>


  <!-- ================================================================ -->
  <!-- == Elements                                                   == -->
  <!-- ================================================================ -->
  <!-- ============================================================ -->
  <!-- Main                                                         -->
  <!-- ============================================================ -->
  
  <xsl:template name="m:webpage-main-pre" />
  <xsl:template name="m:webpage-main-post"/>

</xsl:stylesheet>
