<xsl:stylesheet version="1.1"
  xmlns:xsl ="http://www.w3.org/1999/XSL/Transform"
  xmlns:str ="http://exslt.org/strings"
  xmlns:date="http://exslt.org/dates-and-times"
  xmlns:func="http://exslt.org/functions"
  xmlns:exsl="http://exslt.org/common"
  xmlns:f   ="http://www.moxow.org/function"
  extension-element-prefixes="str date func exsl"
  exclude-result-prefixes="xsl f"
>



  <!-- ================================================================ -->
  <!-- === I18N                                                     === -->
  <!-- ================================================================ -->

  <func:function name="f:_">
    <xsl:param name="txt"/>
      
    <func:result>
      YO
    </func:result>
  </func:function>



  <!-- ================================================================ -->
  <!-- === List                                                     === -->
  <!-- ================================================================ -->

  <func:function name="f:space-list-collapse">
    <xsl:param name="l1"/>
    <xsl:param name="l2"/>
    <xsl:variable name="list" select="concat($l1, ' ', $l2)"/>

    <func:result>
      <xsl:variable name="dash"  select="str:split($list, ' ')[. = '-']"/>
      <xsl:choose>
	<xsl:when test="$dash">
	  <xsl:value-of select="f:string-join(
				  $dash/following-sibling::node(), ' ')"/>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:value-of select="$list"/>
	</xsl:otherwise>
      </xsl:choose>
    </func:result>
  </func:function>



  <!-- ================================================================ -->
  <!-- === String                                                   === -->
  <!-- ================================================================ -->

  <func:function name="f:string-join">
    <xsl:param name="string-set"/>
    <xsl:param name="separator"/>

    <func:result>
      <xsl:for-each select="$string-set">
	<xsl:if test="position() &gt; 1">
	  <xsl:value-of select="$separator"/>
	</xsl:if>
	<xsl:value-of select="."/>
      </xsl:for-each>
    </func:result>
  </func:function>

  <func:function name="f:ends-with">
    <xsl:param name="target"/>
    <xsl:param name="suffix"/>
    <func:result select="$suffix = substring($target, 
		   string-length($target) - string-length($suffix) + 1)"/>
  </func:function>

  <func:function name="f:starts-with">
    <xsl:param name="target"/>
    <xsl:param name="prefix"/>
    <func:result select="$prefix = substring($target, 1, 
			                     string-length($prefix))"/>
  </func:function>



  <!-- ================================================================ -->
  <!-- === Date & Time                                              === -->
  <!-- ================================================================ -->

  <!-- Convert a date timestamp to a human readable string -->
  <func:function name="f:datetime-to-string">
    <xsl:param name="datetime" />

    <xsl:variable name="dt">
      <xsl:choose>
        <xsl:when test="normalize-space($datetime)">
          <xsl:value-of select="substring($datetime,1,4)"/>
          <xsl:text>-</xsl:text>
          <xsl:value-of select="substring($datetime,5,2)"/>
          <xsl:text>-</xsl:text>
          <xsl:value-of select="substring($datetime,7,2)"/>
          <xsl:text>T</xsl:text>
          <xsl:value-of select="substring($datetime,9,2)"/>
          <xsl:text>:</xsl:text>
          <xsl:value-of select="substring($datetime,11,2)"/>
          <xsl:text>:</xsl:text>
          <xsl:value-of select="substring($datetime,13,2)"/>
          <xsl:value-of select="substring($datetime,15,1)"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="date:date-time()"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <func:result select="concat(date:day-abbreviation($dt),   ' ',
			        date:month-abbreviation($dt), ' ',
				date:day-in-month($dt),       '. ',
				date:year($dt))"/>
  </func:function>



  <!-- ================================================================ -->
  <!-- === Path                                                     === -->
  <!-- ================================================================ -->

  <func:function name="f:dirname">
    <xsl:param name="path"/>
    <func:result 
	select="f:string-join(str:split($path,'/')[position()!=last()], '/')"/>
  </func:function>

  <func:function name="f:basename">
    <xsl:param name="path"/>
    <func:result 
	select="str:split($path,'/')[last()]"/>
  </func:function>

  <func:function name="f:path-insert-last">
    <xsl:param name="src"/>
    <xsl:param name="dir"/>
      
    <func:result>
      <xsl:if test="f:starts-with($src, '/')">
	<xsl:text>/</xsl:text>
      </xsl:if>
      <xsl:variable name="elt" select="str:split($src, '/')"/>
      <xsl:for-each select="$elt[position() &lt; last()]">
	<xsl:value-of select="."/><xsl:text>/</xsl:text>
      </xsl:for-each>
      <xsl:if test="string-length($dir)">
	<xsl:value-of select="$dir"/>
	<xsl:text>/</xsl:text>
      </xsl:if>
      <xsl:value-of select="$elt[last()]"/>
    </func:result>
  </func:function>



  <!-- ================================================================ -->
  <!-- === Javascript                                               === -->
  <!-- ================================================================ -->

  <xsl:variable name="f-javascript-string-escaper-data">
    <replace src="\"      dst="\\"/>
    <replace src="&quot;" dst="\&quot;"/>
    <replace src="&#xA;"  dst="\n"/>
    <replace src="&#xD;"  dst="\r"/>
    <replace src="&#x9;"  dst="\t"/>
  </xsl:variable>
  <xsl:variable name="f-javascript-string-escaper"
		select="exsl:node-set($f-javascript-string-escaper-data)"/>

  <func:function name="f:javascript-string-encode">
    <xsl:param name="str"/>
    <func:result>
      <xsl:text>"</xsl:text>
      <xsl:value-of select="str:replace($str, 
			      $f-javascript-string-escaper/replace/@src,
			      $f-javascript-string-escaper/replace/@dst)"/>
      <xsl:text>"</xsl:text>
    </func:result>
  </func:function>

  <func:function name="f:javascript-encode">
    <xsl:param name="data"/>
    <xsl:variable name="type" select="exsl:object-type($data)"/>
    <func:result>
      <xsl:choose>
	<xsl:when test="$type = 'number'">
	  <xsl:value-of select="$data"/>
	</xsl:when>
	<xsl:when test="$type = 'boolean'">
	  <xsl:value-of select="f:if($data,'true','false')"/>
	</xsl:when>
	<xsl:when test="not($data)">
	  <xsl:text>null</xsl:text>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:value-of select="f:javascript-string-encode($data)"/>
	</xsl:otherwise>
      </xsl:choose>
    </func:result>
  </func:function>



  <!-- ================================================================ -->
  <!-- === JSON                                                     === -->
  <!-- ================================================================ -->

  <!-- constants -->
  <xsl:variable name="f-ns-XHTML" select="'http://www.w3.org/1999/xhtml'" />

  <!-- root-node -->
  <xsl:template match="/" mode="jsonml">
    <xsl:apply-templates select="*" mode="jsonml"/>
  </xsl:template>

  <!-- comments -->
  <xsl:template match="comment()" mode="jsonml">
    <!--
	<xsl:text>/*</xsl:text>
	<xsl:value-of select="." disable-output-escaping="yes" />
	<xsl:text>*/</xsl:text>
    -->
  </xsl:template>

  <!-- elements -->
  <xsl:template match="*" mode="jsonml">
    <xsl:text>[</xsl:text>
    
    <!-- tag-name string -->
    <xsl:text>"</xsl:text>
    <xsl:value-of select="f:if(namespace-uri() = $f-ns-XHTML,
			       local-name(), name())"/>
    <xsl:text>"</xsl:text>
    
    <!-- attribute object -->
    <xsl:if test="count(@*)>0">
      <xsl:text>,{</xsl:text>
      <xsl:for-each select="@*">
	<xsl:if test="position()>1">
	  <xsl:text>,</xsl:text>
	</xsl:if>
	<xsl:apply-templates select="." mode="jsonml"/>
      </xsl:for-each>
      <xsl:text>}</xsl:text>
    </xsl:if>
    
    <!-- child elements and text-nodes -->
    <xsl:for-each select="*|text()[normalize-space()]">
      <xsl:text>,</xsl:text>
      <xsl:apply-templates select="." mode="jsonml"/>
    </xsl:for-each>
    
    <xsl:text>]</xsl:text>
  </xsl:template>

  <!-- text-nodes -->
  <xsl:template match="text()" mode="jsonml">
    <xsl:value-of select="f:javascript-string-encode(.)" 
		  disable-output-escaping="yes" />
  </xsl:template>

  <!-- attributes -->
  <xsl:template match="@*" mode="jsonml">
    <xsl:text>"</xsl:text>
    <xsl:value-of select="f:if(namespace-uri() = $f-ns-XHTML,
			       local-name(), name())"/>
    <xsl:text>"</xsl:text>
    <xsl:text>:</xsl:text>
    <xsl:value-of select="f:javascript-string-encode(.)" 
		  disable-output-escaping="yes" />
  </xsl:template>

  <func:function name="f:jsonml-encode">
    <xsl:param name="node"/>
    <func:result>
      <xsl:choose>
	<xsl:when test="exsl:object-type($node) = 'node-set'">
	  <xsl:apply-templates select="$node" mode="jsonml"/>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:apply-templates select="exsl:node-set($node)" mode="jsonml"/>
	</xsl:otherwise>
      </xsl:choose>
    </func:result>
  </func:function>



  <!-- ================================================================ -->
  <!-- === Conditionals                                             === -->
  <!-- ================================================================ -->

  <!-- if-absent -->
  <func:function name="f:if-absent" as="xs:anyAtomicType*">
    <xsl:param name="node"  as="node()?"         />
    <xsl:param name="value" as="xs:anyAtomicType"/>
    <xsl:choose>
      <xsl:when test="$node">
	<func:result select="$node"/>
      </xsl:when>
      <xsl:otherwise>
	<func:result select="$value"/>
      </xsl:otherwise>
    </xsl:choose>
  </func:function>
  
  <!-- if-empty -->
  <func:function name="f:if-empty" as="xs:anyAtomicType*">
    <xsl:param name="node"  as="node()?"         />
    <xsl:param name="value" as="xs:anyAtomicType"/>
    <xsl:choose>
      <xsl:when test="$node">
	<func:result select="$node and $node/child::node()"/>
      </xsl:when>
      <xsl:otherwise>
	<func:result select="$value"/>
      </xsl:otherwise>
    </xsl:choose>
  </func:function>
  
  <!-- if-nodes -->
  <func:function name="f:if-nodes">
    <xsl:param name="bool"/>
    <xsl:param name="then"/>
    <xsl:param name="else" select="/.."/>
    <xsl:choose>
      <xsl:when test="$bool">
	<func:result select="$then"/>
      </xsl:when>
      <xsl:otherwise>
	<func:result select="$else"/>
      </xsl:otherwise>
    </xsl:choose>
  </func:function>
  
  <!-- unless-nodes -->
  <func:function name="f:unless-nodes">
    <xsl:param name="bool"/>
    <xsl:param name="then"/>
    <xsl:param name="else" select="/.."/>
    <xsl:choose>
      <xsl:when test="not($bool)">
	<func:result select="$then"/>
      </xsl:when>
      <xsl:otherwise>
	<func:result select="$else"/>
      </xsl:otherwise>
    </xsl:choose>
  </func:function>

</xsl:stylesheet>
