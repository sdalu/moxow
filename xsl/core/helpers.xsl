<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: helpers.xsl 3208 2012-11-09 16:13:44Z sdalu $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl ="http://www.w3.org/1999/XSL/Transform"
  xmlns:func="http://exslt.org/functions"
  xmlns:m   ="http://www.moxow.org/main"
  xmlns:d   ="http://www.moxow.org/data"
  extension-element-prefixes="func"
  exclude-result-prefixes="xsl m d"
>


  <!-- ================================================================ -->
  <!-- === Lang processing                                          === -->
  <!-- ================================================================ -->

  <!-- Select best matching node according to lang -->
  <func:function name="m:lang">
    <xsl:param name="nodes"                   />
    <xsl:param name="lang"   select="$m:lang" />

    <xsl:choose>
      <xsl:when test="$nodes[lang($lang)]">
	<func:result select="$nodes[lang($lang)]"/>
      </xsl:when>
      <xsl:when test="$nodes[lang('en')]">
	<func:result select="$nodes[lang('en')]"/>
      </xsl:when>
      <xsl:otherwise>
	<func:result select="$nodes[1]"/>
      </xsl:otherwise>
    </xsl:choose>
  </func:function>


  <!-- Convert a lang (iso2) to a human readable string -->
  <func:function name="m:lang-to-string">
    <xsl:param name="lang"/>

    <func:result>
      <xsl:choose>
	<xsl:when test="$lang='en'">English</xsl:when>
	<xsl:when test="$lang='fr'">Français</xsl:when>
	<xsl:otherwise><xsl:message terminate="yes">
	  Unknown lang abbr (<xsl:value-of select="$lang"/>)
	</xsl:message></xsl:otherwise>
      </xsl:choose>
    </func:result>
  </func:function>



  <!-- ================================================================ -->
  <!-- === Snippets                                                 === -->
  <!-- ================================================================ -->

  <!-- Snippets defaults to empty -->
  <xsl:variable name="m:snippets" select="/.."/>

  <!-- Select snippet according to it's id -->
  <func:function name="m:snippet">
    <xsl:param name="id"                            />
    <xsl:param name="lang"     select="$m:lang"     />
    <xsl:param name="snippets" select="$m:snippets" />

    <xsl:variable name="snippet" 
		  select="$snippets/d:snippets/d:snippet[@id=$id]"/>
    <func:result select="m:lang($snippet)"/>
  </func:function>

  <!-- Render snippet -->
  <xsl:template match="m:snippet">
    <xsl:apply-templates select="m:snippet(@for)"/>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- === URI                                                      === -->
  <!-- ================================================================ -->

  <!-- Select snippet according to it's id -->
  <func:function name="m:uri-absolute-builder">
    <xsl:param name="link"/>

    <func:result>
      <xsl:if test="not(contains($link, '://'))">
	<xsl:value-of select="$uri-scheme-host-port"/>
      </xsl:if>
      <xsl:if test="substring($link, 1, 1) != '/'">
	<xsl:value-of select="$uri-path"/>
      </xsl:if>
      <xsl:value-of select="$link"/>
    </func:result>
  </func:function>
  
  <!-- uri-self-i18n-lang                                           -->
  <!-- ============================================================ -->
  <!-- m-lang : m:lang tag -->
  <!-- current: current language code iso-639 -->
  <xsl:template name="uri-self-i18n-lang">
    <xsl:param name="m-lang" />
    <xsl:param name="current"/>

    <!-- host & path -->
    <xsl:choose>
      <!-- href is specified -->
      <xsl:when test="$m-lang/@href">
	<xsl:value-of select="$m-lang/@href"/>
      </xsl:when>

      <!-- use the host specified-->
      <xsl:when test="$m-lang/@host">
	<xsl:text>//</xsl:text>
	<xsl:value-of select="$m-lang/@host"/>
	<xsl:value-of select="$request-uri"/>
      </xsl:when>

      <!-- use different host according to default values -->
      <xsl:when test="$m:defaults/m:i18n/@use-host">
	<xsl:text>//</xsl:text>
	<xsl:value-of select="$m:defaults/m:i18n/@*[local-name() = 
			                           $m-lang/@isocode]"/>
	<xsl:value-of select="$request-uri"/>
      </xsl:when>

      <!-- use subdomain -->
      <xsl:otherwise>
	<xsl:text>//</xsl:text>
	<xsl:variable name="host" 
		      select="substring-before($server-name, '.')"/>
	<xsl:variable name="domain"
		      select="substring-after($server-name, '.')"/>
	<xsl:variable name="sub-domain"
		      select="substring-before($domain, '.')"/>
	<xsl:variable name="parent-domain"
		      select="substring-after($domain, '.')"/>
	<xsl:value-of select="$host"/>
	<xsl:text>.</xsl:text>
	<xsl:value-of select="$m-lang/@isocode"/>
	<xsl:text>.</xsl:text>
	<xsl:if test="$sub-domain != $current">
	  <xsl:value-of select="$sub-domain"/>
	  <xsl:text>.</xsl:text>
	</xsl:if>
	<xsl:value-of select="$parent-domain"/>	    
	<xsl:value-of select="$request-uri"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>






  <!-- Localisation                                                 -->
  <!-- ============================================================ -->
  <xsl:template name="l10n-string">
    <xsl:param name="id"/>
    <xsl:variable name="l10n" 
		  select="document(concat('l10n/', $m:lang, '.xml'))"/>
    <xsl:copy-of select="$l10n//msg[@id=$id]/node()"/>
  </xsl:template>


</xsl:stylesheet>
