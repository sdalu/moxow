<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: pagedesc.xsl 3289 2013-04-08 14:44:27Z sdalu $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:m  ="http://www.moxow.org/main"
  xmlns:d  ="http://www.moxow.org/data"
  exclude-result-prefixes="xsl m d"
>




  <!-- ================================================================ -->
  <!-- === Custom box                                               === -->
  <!-- ================================================================ -->
  <xsl:template match="m:custom-box">
    <div class="m-custom-box {@class}">
      <xsl:copy-of select="@id|@style"/>
      <xsl:apply-templates select="node()"/>
    </div> 
  </xsl:template>



  <!-- ================================================================ -->
  <!-- === Sitemap                                                  === -->
  <!-- ================================================================ -->

  <xsl:template match="m:sitemap">
    <nav class="mxw-sitemap">
      <xsl:choose>
	<xsl:when test="node()">
	  <xsl:apply-templates/>
	</xsl:when>
	<xsl:when test="$m:sitemap/d:snippet">
	    <xsl:apply-templates select="m:lang($m:sitemap/d:snippet)"/>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:apply-templates select="$m:sitemap"/>
	</xsl:otherwise>
      </xsl:choose>
    </nav>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- === Lang                                                     === -->
  <!-- ================================================================ -->

  <xsl:template match="m:lang">
    <xsl:variable name="title" select="m:lang-to-string(@isocode)"/>

    <li>
      <!-- Set active lang from expected language (expect-lang,
	   if defined) and next from the language of the
	   served file ($lang) to avoid flag switching
	   between pages due to the availability of
	   the localized files 		                -->
      <xsl:variable name="display-lang">
	<xsl:choose>
	  <xsl:when test="$expect-lang">
	    <xsl:value-of select="$expect-lang"/>
	  </xsl:when>
	  <xsl:when test="$lang">
	    <xsl:value-of select="$lang"/>
	  </xsl:when>
	</xsl:choose>
      </xsl:variable>
      
      <xsl:if test="$display-lang = @isocode">
        <xsl:attribute name="class">active</xsl:attribute>
      </xsl:if>
      
      <xsl:variable name="href">
	<xsl:call-template name="uri-self-i18n-lang">
	  <xsl:with-param name="m-lang"  select="."/>
	  <xsl:with-param name="current" select="$lang"/>
	</xsl:call-template>
      </xsl:variable>
      <a href="{$href}" hreflang="{@isocode}">
	<img src="/images/common/flags/18x12/{@isocode}.png"
	     alt="{@isocode}" title="{$title}"/>
      </a>
    </li>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- === Breadcrumbs                                              === -->
  <!-- ================================================================ -->

  <!-- VALIDATOR: http://www.google.com/webmasters/tools/richsnippets -->

  <xsl:template match="m:path">
    <xsl:variable name="title"    select="$m:pagedesc/m:title"/> 
    
    <ul class="mxw-breadcrumbs">
      <!-- Smart complete with root and menu -->
      <xsl:if test="@smart">
	<xsl:variable name="selected" select="$m:pagedesc/m:menu/@selected"/>
	<xsl:variable name="root"     select="$m:menu/@root"/>
	<xsl:choose>
	  <xsl:when test="$root">
	    <li itemscope="itemscope"
		itemtype="http://data-vocabulary.org/Breadcrumb">
	      <a href="{$m:menu/m:entry[@id=$root]/@href}" itemprop="url">
		<span itemprop="title">
		  <xsl:variable name="entry" 
			select="m:lang($m:menu/m:entry[@id=$root]/m:title)"/>
		  <xsl:choose>
		    <xsl:when test="$entry/@text">
		      <xsl:value-of select="$entry/@text"/>
		    </xsl:when>
		    <xsl:otherwise>
		      <xsl:value-of select="$entry"/>
		    </xsl:otherwise>
		  </xsl:choose>
		</span>
	      </a>
	    </li>
	  </xsl:when>
	  <xsl:otherwise>
	    <li itemscope="itemscope"
		itemtype="http://data-vocabulary.org/Breadcrumb">
	      <a href="/" itemprop="url">
		<span itemprop="title">		
		  <xsl:call-template name="l10n-string">
		    <xsl:with-param name="id" select="'home'"/>
		  </xsl:call-template>
		</span>
	      </a>
	    </li>
	  </xsl:otherwise>
	</xsl:choose>
	<xsl:if test="@smart != 'root' and $selected != $root">
	  <li itemscope="itemscope"
	      itemtype="http://data-vocabulary.org/Breadcrumb">
	    <a itemprop="url">
	      <xsl:attribute name="href">
		<xsl:value-of select="$m:menu/m:entry[@id=$selected]/@href"/>
	      </xsl:attribute>
	      <span itemprop="title">		
		  <xsl:variable name="entry" 
		      select="m:lang($m:menu/m:entry[@id=$selected]/m:title)"/>
		  <xsl:choose>
		    <xsl:when test="$entry/@text">
		      <xsl:value-of select="$entry/@text"/>
		    </xsl:when>
		    <xsl:otherwise>
		      <xsl:value-of select="$entry"/>
		    </xsl:otherwise>
		  </xsl:choose>
	      </span>
	    </a>
	  </li>
	</xsl:if>
      </xsl:if>
      <!-- Smart add current first -->
      <xsl:if test="@smart = 'first'">
	<li itemscope="itemscope"
	    itemtype="http://data-vocabulary.org/Breadcrumb">
	  <a href="" itemprop="url">
	    <span itemprop="title">		
	      <xsl:value-of select="$title"/>
	    </span>
	  </a>
	</li>
      </xsl:if>
      <!-- User supplied entries -->
      <xsl:for-each select="m:entry">
	<li itemscope="itemscope"
	    itemtype="http://data-vocabulary.org/Breadcrumb">
	  <xsl:choose>
	    <xsl:when test="@href">
	      <a href="{@href}" itemprop="url">
		<span itemprop="title">
		  <xsl:value-of select="."/>
		</span>
	      </a>
	    </xsl:when>
	    <xsl:otherwise>
		<span itemprop="title">
		  <xsl:value-of select="."/>
		</span>
	    </xsl:otherwise>
	  </xsl:choose>
	</li>
      </xsl:for-each>
      <!-- Smart add current last -->
      <xsl:if test="@smart = 'last'">
	<li itemscope="itemscope"
	    itemtype="http://data-vocabulary.org/Breadcrumb">
	  <a href="" itemprop="url">
	    <span itemprop="title">
	      <xsl:value-of select="$title"/>
	    </span>
	  </a>
	</li>
      </xsl:if>
    </ul>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- === Menu / Submenu                                           === -->
  <!-- ================================================================ -->

  <xsl:template match="m:menu">
    <xsl:param name="selected-id" select="@selected"/>

    <!-- Build menu -->
    <ul id="mxw-menu">
      <xsl:for-each select="m:entry[not(@hidden)]">
	<li id="mxw-menu-tab-{@id}">
	  <xsl:variable name="title" select="m:lang(m:title)"/>
	  <xsl:attribute name="class">
	    <xsl:text>mxw-menu-entry</xsl:text>
            <xsl:if test="$selected-id = @id">selected active</xsl:if>
	  </xsl:attribute>
	  <xsl:choose>
	    <xsl:when test="@href">
	      <a href="{@href}">
		<xsl:apply-templates select="$title"/>
	      </a>
	    </xsl:when>
	    <xsl:otherwise>
	      <span><xsl:apply-templates select="$title"/></span>
	    </xsl:otherwise>
	  </xsl:choose>

	  <xsl:if test="m:submenu">
	    <xsl:variable name="width">
	      <xsl:choose>
		<xsl:when test="m:submenu/@width">
		  <xsl:value-of select="m:submenu/@width"/>;
		</xsl:when>
		<xsl:otherwise>200px</xsl:otherwise>
	      </xsl:choose>
	    </xsl:variable>
	    <xsl:variable name="submenu" 
			  select="m:lang(m:submenu)"/>
	    <div style="position: relative">
	      <!-- Above wrapping necessary as relative in unsupported -->
	      <!--   in table-cell style                               --> 
	      <div class="mxw-pane mxw-hide" 
		   id="mxw-menu-sub-{@id}"
		   style="width: {$width};">
		<xsl:apply-templates select="$submenu/node()"/>
	      </div>
	    </div>
	  </xsl:if>
	</li>
	</xsl:for-each>
    </ul>

    <!-- Build sub-menu panel (aka: menu-sub-*) -->
    <script type="text/javascript">
      (function(){ // Variable scoping
        var menutab = MoXoW.menutab();
      <xsl:for-each select="m:entry">
	menutab.attach('<xsl:value-of select="@id"/>'); 
      </xsl:for-each>
      })();
    </script>

  </xsl:template>

</xsl:stylesheet>
