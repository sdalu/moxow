<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
  xmlns:xsl ="http://www.w3.org/1999/XSL/Transform"
  xmlns:m   ="http://www.moxow.org/main"
  exclude-result-prefixes="xsl m"
>

  <!-- Forward definitions -->
  <xsl:template name="m:comments-copyright"         />
  <xsl:template name="m:comments-website"           />
  <xsl:template name="m:comments-served-in"         />

  <xsl:template name="m:fix-html5-compat"           />
  <xsl:template name="m:fix-javascript-compat"      />
  <xsl:template name="m:fix-others"                 />

  <xsl:template name="m:semantic-description"       />
  <xsl:template name="m:semantic-authorship"        />
  <xsl:template name="m:semantic-opengraph"         />
  <xsl:template name="m:semantic-schema-org"        />
  <xsl:template name="m:semantic-others"            />

  <xsl:template name="m:navigation-robots"          />
  <xsl:template name="m:navigation-alternate-lang"  />
  <xsl:template name="m:navigation-feeds"           />
  <xsl:template name="m:navigation-others"          />

  <xsl:template name="m:framework-pre"              />
  <xsl:template name="m:framework-commons"          />
  <xsl:template name="m:framework-others"           />
  <xsl:template name="m:framework-post"             />

  <xsl:template name="m:stylesheets-pre"            />
  <xsl:template name="m:stylesheets-others"         />
  <xsl:template name="m:stylesheets-defaults"       />
  <xsl:template name="m:stylesheets-webpage"        />
  <xsl:template name="m:stylesheets-post"           />

  <xsl:template name="m:scripts-pre"                />
  <xsl:template name="m:scripts-others"	            />
  <xsl:template name="m:scripts-defaults"           />
  <xsl:template name="m:scripts-webpage"            />
  <xsl:template name="m:scripts-post"               />

  <xsl:template name="m:device-viewport"            />

  <xsl:template name="m:warning-crappy-browser"     /> 
  <xsl:template name="m:warning-javascript-required"/>
  <xsl:template name="m:warning-cookie-inuse"       />
  <xsl:template name="m:warning-others"             />

  <xsl:template name="m:layout"                     />


  <!-- ================================================================ -->
  <!-- === HTML                                                     === -->
  <!-- ================================================================ -->

  <!-- Comments -->
  <xsl:template name="m:html-intro">
    <xsl:call-template name="m:comments-copyright"       />
    <xsl:call-template name="m:comments-website"         />
  </xsl:template>
  
  
  <!-- Geek candy -->
  <xsl:template name="m:html-geek-candy">
  </xsl:template>
  

  <!-- served -->
  <xsl:template name="m:html-stats">
    <xsl:call-template name="m:comments-served-in"       />
  </xsl:template>



  <!-- ================================================================ -->
  <!-- === Head                                                     === -->
  <!-- ================================================================ -->

  <!-- Fixes -->
  <xsl:template name="m:head-fixes">
    <xsl:call-template name="m:fix-html5-compat"         />
    <xsl:call-template name="m:fix-javascript-compat"    />
    <xsl:call-template name="m:fix-others"               />
  </xsl:template>


  <!-- Semantics -->
  <xsl:template name="m:head-semantics">
    <xsl:call-template name="m:semantic-description"     />
    <xsl:call-template name="m:semantic-authorship"      />
    <xsl:call-template name="m:semantic-opengraph"       />
    <xsl:call-template name="m:semantic-schema-org"      />
    <xsl:call-template name="m:semantic-others"          />
  </xsl:template>


  <!-- Navigations -->
  <xsl:template name="m:head-navigations">
    <xsl:call-template name="m:navigation-robots"        />
    <xsl:call-template name="m:navigation-alternate-lang"/>
    <xsl:call-template name="m:navigation-feeds"         />
    <xsl:call-template name="m:navigation-others"        />
  </xsl:template>


  <!-- Frameworks -->
  <xsl:template name="m:head-frameworks">
    <xsl:call-template name="m:framework-pre"            />
    <xsl:call-template name="m:framework-commons"        />
    <xsl:call-template name="m:framework-others"         />
    <xsl:call-template name="m:framework-post"           />
  </xsl:template>


  <!-- Scripts -->
  <xsl:template name="m:head-scripts">
    <xsl:call-template name="m:scripts-pre"              />
    <xsl:call-template name="m:scripts-others"           />
    <xsl:call-template name="m:scripts-defaults"         />
    <xsl:call-template name="m:scripts-webpage"          />
    <xsl:call-template name="m:scripts-post"             />
  </xsl:template>


  <!-- Stylesheets -->
  <xsl:template name="m:head-stylesheets">
    <xsl:call-template name="m:stylesheets-pre"          />
    <xsl:call-template name="m:stylesheets-others"       />
    <xsl:call-template name="m:stylesheets-defaults"     />
    <xsl:call-template name="m:stylesheets-webpage"      />
    <xsl:call-template name="m:stylesheets-post"         />
  </xsl:template>


  <!-- Devices -->
  <xsl:template name="m:head-devices">
    <xsl:call-template name="m:device-viewport"         />
  </xsl:template>


  <!-- Links -->
  <xsl:template name="m:head-links">
  </xsl:template>


  <!-- Services -->
  <xsl:template name="m:head-services">
  </xsl:template>



  <!-- ================================================================ -->
  <!-- === Body                                                     === -->
  <!-- ================================================================ -->

  <!-- Warnings -->
  <xsl:template name="m:body-warnings">
    <xsl:call-template name="m:warning-crappy-browser"     />
    <xsl:call-template name="m:warning-javascript-required"/>
    <xsl:call-template name="m:warning-cookie-inuse"       />
    <xsl:call-template name="m:warning-others"             />
  </xsl:template>


  <!-- Content -->
  <xsl:template name="m:body-content">
    <xsl:call-template name="m:layout"/>
  </xsl:template>


  <!-- Analytics -->
  <xsl:template name="m:body-analytics">
  </xsl:template>

</xsl:stylesheet>
