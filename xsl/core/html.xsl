<xsl:stylesheet version="1.0"
  xmlns:xsl ="http://www.w3.org/1999/XSL/Transform"
  xmlns:m   ="http://www.moxow.org/main"
  exclude-result-prefixes="xsl m"
>
  
  <!-- Forward definitions -->
  <xsl:template name="m:html-pre"           />
  <xsl:template name="m:html-head-pre"      />
  <xsl:template name="m:html-head-defaults" />
  <xsl:template name="m:html-head-post"     />
  <xsl:template name="m:html-body-pre"      />
  <xsl:template name="m:html-body-defaults" />
  <xsl:template name="m:html-body-post"     />
  <xsl:template name="m:html-body-analytics"/>
  <xsl:template name="m:html-post"          />
  

  
  <!-- ================================================================ -->
  <!-- === HTML                                                     === -->
  <!-- ================================================================ -->

  <xsl:template name="m:html">
    <xsl:param name="root" select="$m:moxow"/>

    <!-- HTML -->
    <html>
      <xsl:copy-of select="$root/@lang|$root/@class|$root/@manifest"/>
      <xsl:call-template name="m:html-pre"/>
      
      <head>
	<xsl:call-template name="m:html-head"/>
      </head>

      <body>
	<xsl:call-template name="m:html-body"/>
      </body>
      
      <xsl:call-template name="m:html-post"/>
    </html>
  </xsl:template>


  <!-- Head -->
  <xsl:template name="m:html-head">
    <xsl:call-template name="m:html-head-pre"      />
    <xsl:call-template name="m:html-head-defaults" />
    <xsl:call-template name="m:html-head-post"     />
  </xsl:template>


  <!-- Body -->
  <xsl:template name="m:html-body">
    <xsl:call-template name="m:html-body-pre"      />
    <xsl:call-template name="m:html-body-defaults" />
    <xsl:call-template name="m:html-body-post"     />
    <xsl:call-template name="m:html-body-analytics"/>
  </xsl:template>

</xsl:stylesheet>
