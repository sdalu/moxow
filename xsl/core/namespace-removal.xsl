<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: namespace-removal.xsl 3186 2012-11-01 19:41:47Z sdalu $ -->

<xsl:stylesheet version="1.0"
  xmlns:h  ="http://www.w3.org/1999/xhtml" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:fb ="http://www.facebook.com/2008/fbml"
  exclude-result-prefixes="h xsl fb"
>

  <!-- 
       See documentation:
        - http://www.tei-c.org/wiki/index.php/Remove-Namespaces.xsl 
  -->


  <!-- Disable output escaping and remove namespace --> 
  <!-- ============================================ --> 
  <xsl:template match="script|style"> 
    <xsl:element name="{name()}"> 
      <xsl:copy-of select="@*"/> 
      <xsl:value-of select="." disable-output-escaping="yes"/> 
    </xsl:element> 
  </xsl:template> 

  <!-- Perform a copy and remove namespace -->
  <!-- =================================== -->
  <xsl:template match="fb:*|h:*">
    <xsl:element name="{name()}">
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>

</xsl:stylesheet>
