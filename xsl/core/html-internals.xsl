<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:m  ="http://www.moxow.org/main"
  xmlns:o  ="urn:moxow:3rd-party"
  xmlns:w  ="http://www.moxow.org/window"
  exclude-result-prefixes="xsl m o w"
>
  
  <!-- Forward definitions -->
  <xsl:template name="m:twitter-init-if-required" />
  <xsl:template name="m:flattr-init-if-required"  />
  <xsl:template name="m:facebook-init-if-required"/>
  <xsl:template name="m:webpage-waypoints"        />

  
  <xsl:template name="m:head-services">
    <!-- Social widgets (1/2) -->
    <!--   (facebook is done inside body)        -->
    <xsl:call-template name="m:twitter-init-if-required"/>
    <xsl:call-template name="m:flattr-init-if-required" />
  </xsl:template>


  <!-- Body -->
  <xsl:template name="m:html-body">
    <div id="body">
      <!-- Class for body -->
      <xsl:attribute name="class">          
	<xsl:value-of select="$m:body-class"/>
      </xsl:attribute>
      <xsl:call-template name="m:html-body-pre"      />
      <xsl:call-template name="m:html-body-defaults" />
      <xsl:call-template name="m:html-body-post"     />
    </div>
    <xsl:call-template name="m:html-body-analytics"/>
  </xsl:template>

  <xsl:template name="m:body-warnings">
    <div id="mxw-global-warning" class="mxw-global-warning">
      <xsl:call-template name="m:warning-crappy-browser"/>
      <xsl:call-template name="m:warning-javascript-required"/>
      <xsl:call-template name="m:warning-cookie-inuse"/>
      <xsl:call-template name="m:warning-others"        />
    </div>
  </xsl:template>

  <xsl:template name="m:body-content">
    <!-- Social widgets (2/2) -->
    <xsl:call-template name="m:facebook-init-if-required"/>

    <!-- Page layout -->
    <div style="position: relative;">
      <!-- Insert default data -->
      <xsl:apply-templates select="$m:defaults/m:data"/>
      <!-- layout for body -->
      <xsl:call-template name="m:layout"/>
    </div>
    
    <!-- Waypoints -->
    <xsl:call-template name="m:webpage-waypoints"/>
  </xsl:template>

</xsl:stylesheet>
