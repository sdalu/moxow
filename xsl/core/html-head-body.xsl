<xsl:stylesheet version="1.0"
  xmlns:xsl ="http://www.w3.org/1999/XSL/Transform"
  xmlns:m   ="http://www.moxow.org/main"
  exclude-result-prefixes="xsl m"
>
  
  <!-- Forward definitions -->
  <xsl:template name="m:html-intro"       />
  <xsl:template name="m:html-geek-candy"  />
  <xsl:template name="m:html-stats"       />

  <xsl:template name="m:head-fixes"       />
  <xsl:template name="m:head-semantics"   />
  <xsl:template name="m:head-navigations" />
  <xsl:template name="m:head-frameworks"  />
  <xsl:template name="m:head-stylesheets" />
  <xsl:template name="m:head-scripts"     />
  <xsl:template name="m:head-devices"     />
  <xsl:template name="m:head-links"       />
  <xsl:template name="m:head-services"    />
  <xsl:template name="m:head-others"      />

  <xsl:template name="m:body-warnings"    />
  <xsl:template name="m:body-content"     />
  <xsl:template name="m:body-analytics"   />


  
  <!-- ================================================================ -->
  <!-- === HTML / HEAD / BODY                                       === -->
  <!-- ================================================================ -->

  <!-- html -->
  <xsl:template name="m:html-pre">
    <xsl:call-template name="m:html-intro"      />
    <xsl:call-template name="m:html-geek-candy" />
  </xsl:template>
  
  <xsl:template name="m:html-post">
    <xsl:call-template name="m:html-stats"      />
  </xsl:template>


  <!-- head -->
  <xsl:template name="m:html-head-pre">
  </xsl:template>

  <xsl:template name="m:html-head-defaults">
    <xsl:call-template name="m:head-fixes"       />
    <xsl:call-template name="m:head-devices"     />
    <xsl:call-template name="m:head-semantics"   />
    <xsl:call-template name="m:head-navigations" />
    <xsl:call-template name="m:head-frameworks"  />
    <xsl:call-template name="m:head-stylesheets" />
    <xsl:call-template name="m:head-scripts"     />
    <xsl:call-template name="m:head-links"       />
    <xsl:call-template name="m:head-services"    />
    <xsl:call-template name="m:head-others"      />
  </xsl:template>

  <xsl:template name="m:html-head-post">
  </xsl:template>


  <!-- body -->
  <xsl:template name="m:html-body-pre">
  </xsl:template>

  <xsl:template name="m:html-body-defaults">
    <xsl:call-template name="m:body-warnings"   />
    <xsl:call-template name="m:body-content"    />
  </xsl:template>

  <xsl:template name="m:html-body-post">
  </xsl:template>

  <xsl:template name="m:html-body-analytics">
    <xsl:call-template name="m:body-analytics"   />
  </xsl:template>
  
</xsl:stylesheet>
