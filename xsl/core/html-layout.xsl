<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: layout.xsl 3293 2013-04-08 22:02:07Z sdalu $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl ="http://www.w3.org/1999/XSL/Transform"
  xmlns:m   ="http://www.moxow.org/main"
  exclude-result-prefixes="xsl m"
>

  <!-- ================================================================ -->
  <!-- === Body layout                                              === -->
  <!-- ================================================================ -->

  <xsl:template name="m:layout">
    <xsl:call-template name="m:layout-pre"         />
    <xsl:call-template name="m:layout-header"      />
    <xsl:call-template name="m:layout-navigation"  />
    <xsl:call-template name="m:layout-notification"/>
    <xsl:call-template name="m:layout-content"     />
    <xsl:call-template name="m:layout-footer"      />
    <xsl:call-template name="m:layout-post"        />
  </xsl:template>



  <!-- ================================================================ -->
  <!-- === Layout                                                   === -->
  <!-- ================================================================ -->

  <!-- Pre -->
  <xsl:template name="m:layout-pre" />


  <!-- Header -->
  <xsl:variable name="m:layout-header-element" select="'header'"/>
  <xsl:variable name="m:layout-header-class"   select="'mxw-header'"/>
  
  <xsl:template name="m:layout-header">
    <xsl:call-template name="m:layout-wrapped-header"/>
  </xsl:template>

  <xsl:template name="m:layout-wrapped-header">
    <xsl:element name="{$m:layout-header-element}">
      <xsl:attribute name="id">mxw-header</xsl:attribute>
      <xsl:attribute name="class">
	<xsl:value-of select="$m:layout-header-class"/>
	<xsl:text> robots-nocontent</xsl:text>
      </xsl:attribute>
      <xsl:comment>google_ad_section_start(weight=ignore)</xsl:comment>
      <xsl:call-template name="m:layout-raw-header"/>
      <xsl:comment>google_ad_section_end</xsl:comment>
    </xsl:element>
  </xsl:template>

  <xsl:template name="m:layout-raw-header">
    <xsl:call-template name="m:layout-header-pre"     />
    <xsl:call-template name="m:layout-header-content" />
    <xsl:call-template name="m:layout-header-post"    />
  </xsl:template>

  <xsl:template name="m:layout-header-pre"/>
  <xsl:template name="m:layout-header-content">
    <xsl:call-template name="m:layout-header-defaults"/>
  </xsl:template>
  <xsl:template name="m:layout-header-post"/>

  <xsl:template name="m:layout-header-defaults">
    <xsl:variable name="header" select="$m:moxow/m:header"/>
    <xsl:apply-templates select="$header[@place='pre']"/>
    <xsl:if test="not($header[@place = 'replace'])">
      <xsl:apply-templates select="m:lang($m:defaults/m:header)"/>
    </xsl:if>
    <xsl:apply-templates select="$header[not(@place)]"/>
    <xsl:apply-templates select="$header[@place='post']"/>
  </xsl:template>


  <!-- Navigation -->
  <xsl:variable name="m:layout-navigation-element" select="'nav'"/>
  <xsl:variable name="m:layout-navigation-class"   select="'mxw-navigation'"/>

  <xsl:template name="m:layout-navigation">
    <xsl:call-template name="m:layout-wrapped-navigation"/>
  </xsl:template>

  <xsl:template name="m:layout-wrapped-navigation">
    <xsl:element name="{$m:layout-navigation-element}">
      <xsl:attribute name="id">mxw-navigation</xsl:attribute>
      <xsl:attribute name="class">
	<xsl:value-of select="$m:layout-navigation-class"/>
	<xsl:text> robots-nocontent</xsl:text>
      </xsl:attribute>
      <xsl:comment>google_ad_section_start(weight=ignore)</xsl:comment>
      <xsl:call-template name="m:layout-raw-navigation"/>
      <xsl:comment>google_ad_section_end</xsl:comment>
    </xsl:element>
  </xsl:template>

  <xsl:template name="m:layout-raw-navigation">
    <xsl:call-template name="m:layout-navigation-pre"     />
    <xsl:call-template name="m:layout-navigation-content" />
    <xsl:call-template name="m:layout-navigation-post"    />
  </xsl:template>

  <xsl:template name="m:layout-navigation-pre"/>
  <xsl:template name="m:layout-navigation-content">
    <xsl:call-template name="m:layout-navigation-defaults"/>
  </xsl:template>
  <xsl:template name="m:layout-navigation-post"/>

  <xsl:template name="m:layout-navigation-defaults">
    <xsl:variable name="navigation" select="$m:moxow/m:navigation"/>
    <xsl:apply-templates select="$navigation[@place='pre']"/>
    <xsl:if test="not($navigation[@place = 'replace'])">
      <xsl:apply-templates select="m:lang($m:defaults/m:navigation)"/>
    </xsl:if>
    <xsl:apply-templates select="$navigation[not(@place)]"/>
    <xsl:apply-templates select="$navigation[@place='post']"/>
  </xsl:template>


  <!-- Notification -->
  <xsl:variable name="m:layout-notification-element" select="'div'"/>
  <xsl:variable name="m:layout-notification-class"   select="'mxw-notification'"/>

  <xsl:template name="m:layout-notification">
    <xsl:call-template name="m:layout-wrapped-notification"/>
  </xsl:template>

  <xsl:template name="m:layout-wrapped-notification">
    <xsl:element name="{$m:layout-notification-element}">
      <xsl:attribute name="id">mxw-notification</xsl:attribute>
      <xsl:attribute name="class">
	<xsl:value-of select="$m:layout-notification-class"/>
	<xsl:text> robots-nocontent</xsl:text>
      </xsl:attribute>
      <xsl:comment>google_ad_section_start(weight=ignore)</xsl:comment>
      <xsl:call-template name="m:layout-raw-notification"/>
      <xsl:comment>google_ad_section_end</xsl:comment>
    </xsl:element>
  </xsl:template>

  <xsl:template name="m:layout-raw-notification">
    <xsl:call-template name="m:layout-notification-pre"     />
    <xsl:call-template name="m:layout-notification-content" />
    <xsl:call-template name="m:layout-notification-post"    />
  </xsl:template>

  <xsl:template name="m:layout-notification-pre"/>
  <xsl:template name="m:layout-notification-content">
    <xsl:call-template name="m:layout-notification-defaults"/>
  </xsl:template>
  <xsl:template name="m:layout-notification-post"/>

  <xsl:template name="m:layout-notification-defaults">
    <xsl:variable name="notification" select="$m:moxow/m:notification"/>
    <xsl:apply-templates select="$notification[@place='pre']"/>
    <xsl:if test="not($notification[@place = 'replace'])">
      <xsl:apply-templates select="m:lang($m:defaults/m:notification)"/>
    </xsl:if>
    <xsl:apply-templates select="$notification[not(@place)]"/>
    <xsl:apply-templates select="$notification[@place='post']"/>
  </xsl:template>


  <!-- Content -->
  <xsl:variable name="m:layout-content-element" select="'div'"/>
  <xsl:variable name="m:layout-content-class"   select="'mxw-content'"/>

  <xsl:template name="m:layout-content">
    <xsl:call-template name="m:layout-wrapped-content"/>
  </xsl:template>

  <xsl:template name="m:layout-wrapped-content">
    <xsl:element name="{$m:layout-content-element}">
      <xsl:attribute name="id">mxw-content</xsl:attribute>
      <xsl:attribute name="class">
	<xsl:value-of select="$m:layout-content-class"/>
      </xsl:attribute>
      <xsl:attribute name="role">main</xsl:attribute>
      <xsl:call-template name="m:layout-raw-content"/>
    </xsl:element>
  </xsl:template>

  <xsl:template name="m:layout-raw-content">
    <xsl:call-template name="m:layout-content-pre"     />
    <xsl:call-template name="m:layout-content-content" />
    <xsl:call-template name="m:layout-content-post"    />
  </xsl:template>

  <xsl:template name="m:layout-content-pre"/>
  <xsl:template name="m:layout-content-content">
    <xsl:call-template name="m:layout-content-defaults"/>
  </xsl:template>
  <xsl:template name="m:layout-content-post"/>

  <xsl:template name="m:layout-content-defaults">
    <xsl:variable name="content" select="m:lang($m:defaults/m:content)"/>
    <xsl:apply-templates select="$content[@place='pre']"/>
    <xsl:apply-templates select="$m:content"/>
    <xsl:apply-templates select="$content[@place='post']"/>
  </xsl:template>


  <!-- Footer -->
  <xsl:variable name="m:layout-footer-element" select="'footer'"/>
  <xsl:variable name="m:layout-footer-class"   select="'mxw-footer'"/>
  
  <xsl:template name="m:layout-footer">
    <xsl:call-template name="m:layout-wrapped-footer"/>
  </xsl:template>

  <xsl:template name="m:layout-wrapped-footer">
    <xsl:element name="{$m:layout-footer-element}">
      <xsl:attribute name="id">mxw-footer</xsl:attribute>
      <xsl:attribute name="class">
	<xsl:value-of select="$m:layout-footer-class"/>
	<xsl:text> robots-nocontent</xsl:text>
      </xsl:attribute>
      <xsl:comment>google_ad_section_start(weight=ignore)</xsl:comment>
      <xsl:call-template name="m:layout-raw-footer"/>
      <xsl:comment>google_ad_section_end</xsl:comment>
    </xsl:element>
  </xsl:template>

  <xsl:template name="m:layout-raw-footer">
    <xsl:call-template name="m:layout-footer-pre"     />
    <xsl:call-template name="m:layout-footer-content" />
    <xsl:call-template name="m:layout-footer-post"    />
  </xsl:template>

  <xsl:template name="m:layout-footer-pre"/>
  <xsl:template name="m:layout-footer-content">
    <xsl:call-template name="m:layout-footer-defaults"/>
  </xsl:template>
  <xsl:template name="m:layout-footer-post"/>

  <xsl:template name="m:layout-footer-defaults">
    <xsl:variable name="footer" select="$m:moxow/m:footer"/>
    <xsl:apply-templates select="$footer[@place='pre']"/>
    <xsl:if test="not($footer[@place = 'replace'])">
      <xsl:apply-templates select="m:lang($m:defaults/m:footer)"/>
    </xsl:if>
    <xsl:apply-templates select="$footer[not(@place)]"/>
    <xsl:apply-templates select="$footer[@place='post']"/>
  </xsl:template>


  <!-- Post -->
  <xsl:template name="m:layout-post" />

</xsl:stylesheet>
