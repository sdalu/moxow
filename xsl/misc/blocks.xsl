<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: blocks.xsl 3289 2013-04-08 14:44:27Z sdalu $ -->

<xsl:stylesheet version="1.0"
  xmlns:xsl ="http://www.w3.org/1999/XSL/Transform"
  xmlns:m   ="http://www.moxow.org/main"
  xmlns:f   ="http://www.moxow.org/function"
  xmlns:o  ="urn:moxow:3rd-party"
  exclude-result-prefixes="xsl m f o"
>

  <!-- ================================================================ -->
  <!-- === Link reference                                           === -->
  <!-- ================================================================ -->
  <xsl:template match="m:link-ref">
    <div class="m-link-references">
      <div>
	<xsl:choose>
	  <xsl:when test="@title"><xsl:value-of select="@title"/></xsl:when>
	  <xsl:otherwise>References</xsl:otherwise>
	</xsl:choose>
	<xsl:text>:</xsl:text>
      </div>
      <ul>
	<xsl:for-each select="m:ref">
	  <li><a href="{@href}"><xsl:apply-templates select="node()"/></a></li>
	</xsl:for-each>
      </ul>
    </div>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- === Advertisement                                            === -->
  <!-- ================================================================ -->
  <xsl:template match="m:advertisement">
    <div>
      <xsl:copy-of select="@id|@class|@style"/>
      <xsl:choose>
	<!-- Adsense -->
	<xsl:when test="@type = 'adsense'">
	  <xsl:call-template name="o:google-adsense">
	    <xsl:with-param name="key"    select="@key"   />
	    <xsl:with-param name="slot"   select="@slot"  />
	    <xsl:with-param name="width"  select="@width" />
	    <xsl:with-param name="height" select="@height"/>
	  </xsl:call-template>
	</xsl:when>
	<!-- Infolinks -->
	<xsl:when test="@type = 'infolinks'">
	  <xsl:call-template name="infolinks-ad">
	    <xsl:with-param name="key"    select="@key"   />
	  </xsl:call-template>
	</xsl:when>
	<!-- Infolinks -->
	<xsl:when test="@type = 'oxamedia'">
	  <xsl:call-template name="oxamedia-ad">
	    <xsl:with-param name="key"    select="@key"   />
	  </xsl:call-template>
	</xsl:when>
      </xsl:choose>
    </div>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- === Notify                                                   === -->
  <!-- ================================================================ -->
  <xsl:template match="m:notify">
    <xsl:variable name="id">
      <xsl:choose>
	<xsl:when test="@id">
	  <xsl:value-of select="@id"/></xsl:when>
        <xsl:when test="m:close|@autoclose">
	  <xsl:value-of select="generate-id(.)"/></xsl:when>
      </xsl:choose>
    </xsl:variable>

    <div>
      <xsl:attribute name="class">
	<xsl:text>m-notify</xsl:text>
	<xsl:text> </xsl:text>
	<xsl:value-of select="@class"/>
	<xsl:if test="@start > 0">
	  <xsl:text> m-hidden</xsl:text>
	</xsl:if>
	<xsl:if test="@type">
	  <xsl:text> </xsl:text>
	  <xsl:value-of select="concat('notify-', @type)"/>
	</xsl:if>

      </xsl:attribute>
      <xsl:copy-of select="@style"/>
      <xsl:if test="normalize-space($id)">
	<xsl:attribute name="id"><xsl:value-of select="$id"/></xsl:attribute>
      </xsl:if>
      <xsl:if test="m:close">
	<div id="{$id}-close" role="button">
	  <xsl:copy-of select="m:close/@style"/>
	  <xsl:attribute name="class">
	    m-btn-close-box
	    <xsl:text> </xsl:text>
	    <xsl:if test="m:close/@show = 'hover'">m-hidden</xsl:if>
	  </xsl:attribute>
	  <xsl:choose>
	    <xsl:when test="m:close/node()">
	      <xsl:apply-templates select="m:close/node()"/>
	    </xsl:when>
	    <xsl:otherwise>Hide
	      <xsl:if test="@autoclose">
		(<span class="m-autoclose-value">
		  <xsl:value-of select="@autoclose"/></span>)
	      </xsl:if>
	    </xsl:otherwise>
	  </xsl:choose>
	</div>
      </xsl:if>
      <xsl:if test="@title">
	<h2><xsl:value-of select="@title"/></h2>
      </xsl:if>
      <xsl:apply-templates select="node()[not(self::m:close)]"/>
    </div>          

    <xsl:if test="m:close|@autoclose">
      <script type="text/javascript">
	MoXoW.closable_box(
	'<xsl:value-of select="$id"/>',
	<xsl:choose>
	  <xsl:when test="m:close">
	    '<xsl:value-of select="$id"/>-close'</xsl:when>
	  <xsl:otherwise>
	    null</xsl:otherwise>
	</xsl:choose>
	, {	
	<xsl:if test="@start">
	  start    : <xsl:value-of select="@start"/>,
	</xsl:if>
	<xsl:if test="@autoclose">
	  autoclose: <xsl:value-of select="@autoclose"/>,
	</xsl:if>
	<xsl:if test="m:close/@show">
	  show     : '<xsl:value-of select="m:close/@show"/>',
	</xsl:if>
	<xsl:if test="m:close/@key">
	  key      : '<xsl:value-of select="m:close/@key"/>',
	  <xsl:if test="m:close/@reset">
	  reset    : <xsl:value-of select="m:close/@reset"/>,
	  </xsl:if>
	</xsl:if>
	});
      </script>
    </xsl:if>
  </xsl:template>


  <!-- ================================================================ -->
  <!-- === Infobox                                                  === -->
  <!-- ================================================================ -->
  <xsl:template match="m:infobox">
    <div class="infobox-container">
      <div class="triangle-l"/><div class="triangle-r"/>
      <div class="infobox">
	<h3><xsl:value-of select="@title"/></h3>
	<xsl:apply-templates/>
      </div>
    </div>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- === Table Of Content                                         === -->
  <!-- ================================================================ -->
  <xsl:template name="toc-from-simple-list">
    <xsl:param name="toc"     select="$m:pagedesc/m:toc"/>
    <xsl:param name="content" select="$m:content"/>

    <ul>
      <xsl:for-each select="$toc/m:item">
	<xsl:variable name="key"   select="@key"/>
	<xsl:variable name="title" select="$content//node()[@id=$key]/node()"/>
	<xsl:if test="$title">
	  <li><a href="#{@key}">
	    <xsl:choose>
	      <xsl:when test="node()">
		<xsl:value-of select="node()"/>
	      </xsl:when>
	      <xsl:otherwise>
		<xsl:value-of select="$title"/>
	      </xsl:otherwise>
	    </xsl:choose>
	  </a></li>
	</xsl:if>
      </xsl:for-each>
    </ul>
  </xsl:template>

  <xsl:template name="toc-generate-from-script">
    <xsl:param name="id-toc"   />
    <xsl:param name="id-handle"/>
    <xsl:param name="entries"  />
    <script type="text/javascript">
      $(function() { // Need to wait for the full DOM to retrieve titles
        MoXoW.toc('<xsl:value-of select="$id-toc"   />',
	          '<xsl:value-of select="$id-handle"/>', {
		  <xsl:if test="normalize-space($entries)">
		    hdrs: '<xsl:value-of select="normalize-space($entries)"/>'
		  </xsl:if>
		  });
      });
    </script>
  </xsl:template>


  <xsl:template match="m:toc">
    <xsl:variable name="toc" select="$m:pagedesc/m:toc"/>

    <nav class="mxw-toc {@class}">
      <xsl:copy-of select="@id|@style"/>
      
      <!-- ensure id is defined -->
      <xsl:variable name="id" select="f:if-absent(@id, generate-id())"/>

      <!-- Title -->
      <div id="{$id}-handle">
	<xsl:choose>
	  <xsl:when test="@title">
	    <xsl:value-of select="@title"/>
	  </xsl:when>
	  <xsl:when test="$toc/@title">
	    <xsl:value-of select="$toc/@title"/>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:call-template name="l10n-string">
	      <xsl:with-param name="id" select="'table-of-content'"/>
	    </xsl:call-template>
	  </xsl:otherwise>
	</xsl:choose>
      </div>
      
      <!-- Build TOC 1/2 -->
      <xsl:choose>
	<!-- from predefined toc -->
	<xsl:when test="$toc">
	  <xsl:call-template name="toc-from-simple-list">
	    <xsl:with-param name="toc"     select="$toc"    />
	    <xsl:with-param name="content" select="$m:content"/>
	  </xsl:call-template>
	</xsl:when>
	<!-- from javascript -->
	<xsl:otherwise>
	  <div id="{$id}-toc"/>
	</xsl:otherwise>
      </xsl:choose>
      
      <!-- Insert node content -->
      <xsl:apply-templates/>
      
      <!-- Build TOC 2/2 -->
      <xsl:choose>
	<!-- from pagedesc -->
	<xsl:when test="$toc"/>
	<!-- from javascript -->
	<xsl:otherwise>
	  <xsl:call-template name="toc-generate-from-script">
	    <xsl:with-param name="id-toc">
	      <xsl:value-of select="$id"/><xsl:text>-toc</xsl:text>
	    </xsl:with-param>
	    <xsl:with-param name="id-handle">
	      <xsl:value-of select="$id"/><xsl:text>-handle</xsl:text>
	    </xsl:with-param>
	    <xsl:with-param name="entries">
	      <xsl:value-of select="@entries"/>
	    </xsl:with-param>
	  </xsl:call-template>
	</xsl:otherwise>
      </xsl:choose>
    </nav>
  </xsl:template>



  <!-- ================================================================ -->
  <!-- === Figure                                                   === -->
  <!-- ================================================================ -->
  <xsl:template match="m:figure">
    <figure class="mxw-figure {@class}">
      <xsl:copy-of select="@id|@style|@title|@lang"/> 

      <xsl:choose>
	<xsl:when test="@href">
	  <a href="{@href}">
	    <xsl:apply-templates select="*[not(self::m:caption)]"/>
	  </a>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:apply-templates select="*[not(self::m:caption)]"/>
	</xsl:otherwise>
      </xsl:choose>
      
      <xsl:if test="m:caption">
	<figcaption>
	  <xsl:apply-templates select="m:caption"/>
	  <xsl:if test="@href">
	    <a href="{@href}"><img src="/images/common/misc/open.png"
	    alt="[]"/></a>
	  </xsl:if>
	</figcaption>
      </xsl:if>

    </figure>
  </xsl:template>




</xsl:stylesheet>
