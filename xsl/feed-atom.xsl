<?xml version="1.0" encoding="utf-8"?>
<!-- $Id: feed-atom.xsl 3202 2012-11-07 02:39:41Z sdalu $ -->

<xsl:stylesheet version="1.0"
  xmlns     ="http://www.w3.org/2005/Atom"
  xmlns:xsl ="http://www.w3.org/1999/XSL/Transform"
  xmlns:f   ="http://www.moxow.org/feed"
  exclude-result-prefixes="xsl f"
>

  <xsl:output method="xml" indent="yes" encoding="utf-8"
	      media-type="application/atom+xml"
	      omit-xml-declaration="no"
	      standalone="yes"/>

  <!-- identity -->
  <xsl:template match="*">    
    <xsl:element name="{name()}" namespace="{namespace-uri()}">
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>


  <!-- title / subtitle -->
  <xsl:template match="f:title">
    <title><xsl:value-of select="node()"/></title>
  </xsl:template>

  <xsl:template match="f:subtitle">
    <subtitle><xsl:value-of select="node()"/></subtitle>
  </xsl:template>


  <!-- link / category -->
  <xsl:template match="f:link">
    <link>
      <xsl:copy-of select="@href|@rel|@type|@hreflang|@title|@length"/>
    </link>
  </xsl:template>

  <xsl:template match="f:category">
    <category><xsl:copy-of select="@term|@scheme|@label"/></category>
  </xsl:template>
  

  <!-- author / rights -->
  <xsl:template match="f:author">
    <author>
      <name><xsl:value-of select="@name"/></name>
      <xsl:if test="@email">
	<email><xsl:value-of select="@email"/></email>
      </xsl:if>
    </author>
  </xsl:template>

  <xsl:template match="f:rights">
    <rights><xsl:value-of select="node()"/></rights>
  </xsl:template>


  <!-- content -->
  <xsl:template match="f:content">
    <content>
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates select="node()"/>
    </content>
  </xsl:template>
  

  <!-- feed -->
  <xsl:template match="f:feed">
    <feed>
      <!-- keep xml attribute (base/lang) -->
      <xsl:copy-of select="@xml:base|@xml:lang"/>

      <!-- id -->
      <id><xsl:value-of select="@id"/></id>

      <!-- updated -->
      <updated>
	<xsl:choose>
	  <xsl:when test="@updated">
	    <xsl:value-of select="@updated"/>
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:for-each select="f:entry">
	      <xsl:sort select="@updated" data-type="text" order="descending"/>
	      <xsl:if test="position() = 1">
		<xsl:value-of select="@updated"/>
	      </xsl:if>
	    </xsl:for-each>
	  </xsl:otherwise>
	</xsl:choose>
      </updated>

      <!-- icon / logo -->
      <xsl:if test="@icon">
	<icon><xsl:value-of select="@icon"/></icon>
      </xsl:if>
      <xsl:if test="@logo">
	<logo><xsl:value-of select="@logo"/></logo>
      </xsl:if>

      <!-- other elements -->
      <xsl:apply-templates />
    </feed>
  </xsl:template>


  <!-- entry -->
  <xsl:template match="f:entry">
    <entry>
      <!-- id -->
      <id><xsl:value-of select="@id"/></id>

      <!-- published / updated -->
      <xsl:if test="@published">
	<published><xsl:value-of select="@published"/></published>
      </xsl:if>
      <xsl:if test="@updated">
	<updated><xsl:value-of select="@updated"/></updated>
      </xsl:if>

      <!-- other elements -->
      <xsl:apply-templates/>
    </entry>
  </xsl:template>

</xsl:stylesheet>

