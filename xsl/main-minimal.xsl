<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
  xmlns:xsl ="http://www.w3.org/1999/XSL/Transform"
  xmlns:m   ="http://www.moxow.org/main"
  exclude-result-prefixes="xsl m"
>

  <!-- == Import stylesheet =========================================== -->

  <xsl:import href="/web/MoXoW/xsl/main-core.xsl"       />
  <xsl:import href="/web/MoXoW/xsl/impl/analytics.xsl"  />
  <xsl:import href="/web/MoXoW/xsl/3rd-party/piwik.xsl" />
  <xsl:import href="/web/MoXoW/xsl/config-defaults.xsl" />


</xsl:stylesheet>

