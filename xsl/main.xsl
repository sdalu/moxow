<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
  xmlns:xsl ="http://www.w3.org/1999/XSL/Transform"
  xmlns:func="http://exslt.org/functions"
  xmlns:m   ="http://www.moxow.org/main"
  xmlns:d   ="http://www.moxow.org/data"
  xmlns:f   ="http://www.moxow.org/function"
  xmlns:exsl="http://exslt.org/common"
  extension-element-prefixes="func exsl"
  exclude-result-prefixes="xsl m d f"
>

<!--
http://www.w3.org/1999/xhtml/vocab/
-->

<!--
m:lang / m:locale

-->


  <!-- == Import stylesheet =========================================== -->

  <xsl:import href="main-core.xsl"                />

  <xsl:import href="impl/analytics.xsl"           />
  <xsl:import href="impl/fixes.xsl"               />

  <xsl:import href="core/html-internals.xsl"      />
  <xsl:import href="core/pagedesc.xsl"            />
  <xsl:import href="core/webpage-components.xsl"  />

  <xsl:import href="misc/blocks.xsl"              />

  <xsl:import href="layout/desktop.xsl"           />

  <xsl:import href="3rd-party/google.xsl"         />
  <xsl:import href="3rd-party/twitter.xsl"        />
  <xsl:import href="3rd-party/flattr.xsl"         />
  <xsl:import href="3rd-party/facebook.xsl"       />
  <xsl:import href="3rd-party/addthis.xsl"        />
  <xsl:import href="3rd-party/piwik.xsl"          />
  <xsl:import href="3rd-party/disqus.xsl"         />

  <xsl:import href="config-defaults.xsl"          />


  <!-- ================================================================ -->
  <!-- == Constants helper                                           == -->
  <!-- ================================================================ -->

  <!-- Frameworks -->
  <xsl:variable name="m:framework-fake">
    <m:framework moxow="yes"/>
  </xsl:variable>
  <xsl:variable name="m:framework"
		select="($m:pagedesc | $m:defaults | exsl:node-set($m:framework-fake))/m:framework"/>

  <!-- Page description (and default) -->
  <xsl:variable name="m:pagedesc-dflt" select="$m:defaults"/>

  <!-- Menu & Sitemap -->
  <xsl:variable name="m:menu"
		select="document($m:pagedesc/m:menu/@href |
			$m:defaults/m:menu/@href)/m:menu"        />

  <xsl:variable name="m:sitemap"
		select="document($m:pagedesc/m:sitemap/@href |
			$m:defaults/m:sitemap/@href)/d:sitemap"  />

  <!-- Content -->
  <xsl:variable name="m:notify"     select="/m:moxow/m:notify"        /> 

  <!-- Detection nodes -->
  <xsl:variable name="m:detection-nodes-default"
		select="$m:content | $m:moxow/m:custom-box | 
			$m:defaults/m:data | /m:moxow/m:data"   />
  <xsl:variable name="m:detection-nodes"
		select="$m:detection-nodes-default"                  />



  <!-- == Variables to be overwritten ================================= -->

  <xsl:variable name="m:body-class"/>

  <xsl:variable  name="m:resource-path" select="'/common/'"/>
  <func:function name="m:resource-path">
    <xsl:param name="file"/>
    <xsl:param name="path" select="$m:resource-path"/>
    <func:result select="concat($path, $file)"/>
  </func:function>


</xsl:stylesheet>

